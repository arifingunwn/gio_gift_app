import 'package:gio_gift_app/business_logic/database/database_provider.dart';
import 'package:gio_gift_app/business_logic/models/customer.dart';

abstract class CustomerRepository {
  DatabaseProvider databaseProvider;
  Future<bool> insert(Customer customer);
  Future<bool> update(Customer customer);
  Future<bool> delete(String customerId);
  Future<Customer> get(customerId);
  Future<List<Customer>> getCustomers();
  Future<Customer> getCustomerByPhoneNumber(String phoneNumber);
}
