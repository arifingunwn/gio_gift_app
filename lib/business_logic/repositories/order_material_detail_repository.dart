import 'package:gio_gift_app/business_logic/database/database_provider.dart';
import 'package:gio_gift_app/business_logic/models/order_material_detail.dart';

abstract class OrderMaterialDetailRepository {
  DatabaseProvider databaseProvider;
  Future<OrderMaterialDetail> get(String orderMaterialDetailId);
  Future<List<OrderMaterialDetail>> getOrderMaterialDetails(
      String orderMaterialId);
  Future<bool> insert(OrderMaterialDetail orderMaterialDetail);
  Future<bool> update(OrderMaterialDetail orderMaterialDetail);
  Future<bool> delete(String orderMaterialDetailId);
  Future<bool> deleteByOrderMaterialId(String orderMaterialId);
}
