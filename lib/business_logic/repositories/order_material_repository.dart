import 'package:gio_gift_app/business_logic/database/database_provider.dart';
import 'package:gio_gift_app/business_logic/models/order_material.dart';

abstract class OrderMaterialRepository {
  DatabaseProvider databaseProvider;
  Future<OrderMaterial> get(String orderId);
  Future<void> insert(OrderMaterial orderMaterial);
  Future<void> delete(String orderMaterialId);
}
