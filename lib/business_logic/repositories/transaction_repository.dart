import 'package:gio_gift_app/business_logic/database/database_provider.dart';
import '../models/transaction.dart';

abstract class TransactionRepository {
  DatabaseProvider databaseProvider;
  Future<List<Transaction>> getTransactions(
      DateTime startDate, DateTime endDate, int offset, int limit);
  Future<Transaction> getLastTransaction();
  Future<bool> insert(Transaction trans);
  Future<bool> update(Transaction trans);
}
