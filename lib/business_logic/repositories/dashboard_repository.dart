import 'package:gio_gift_app/business_logic/models/dashboard_data.dart';

abstract class DashboardRepository {
  Future<DashboardData> getDashboardData();
}
