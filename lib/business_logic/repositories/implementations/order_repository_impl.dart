import 'package:gio_gift_app/business_logic/dao/order_dao.dart';
import 'package:gio_gift_app/business_logic/database/database_provider.dart';
import 'package:gio_gift_app/business_logic/models/order.dart';
import 'package:sqflite/sqflite.dart';
import '../order_repository.dart';

class OrderRepositoryImpl implements OrderRepository {
  final dao = OrderDao();

  @override
  DatabaseProvider databaseProvider = DatabaseProvider.dbProvider;

  @override
  Future<bool> insert(Order order) async {
    final Database db = await databaseProvider.db();
    var result = await db.insert(dao.tableName, dao.toMap(order));
    return result > 0;
  }

  @override
  Future<Order> get(String orderId) async {
    final Database db = await databaseProvider.db();
    var queryResult =
        await db.rawQuery("SELECT * FROM [Order] WHERE orderId = ?", [orderId]);
    if (queryResult.isNotEmpty) {
      return dao.fromMap(queryResult.first);
    }

    return null;
  }

  @override
  Future<List<Order>> getOrderInThisMonth() async {
    // final Database db = await databaseProvider.db();
    // return await db.query(dao.tableName);
    return null;
  }

  @override
  Future<List<Order>> getOrders() async {
    final Database db = await databaseProvider.db();
    var queryResult = await db.rawQuery(
        "SELECT * FROM [${dao.tableName}] ORDER BY datetime(createdAt) DESC");
    return dao.fromList(queryResult);
  }

  @override
  Future<List<Order>> getOrdersByStatus(String orderStatus) async {
    final Database db = await databaseProvider.db();
    var queryResult = await db.rawQuery(
        "SELECT * FROM [order] WHERE orderStatus = ? ORDER BY datetime(createdAt) DESC",
        [orderStatus]);
    return dao.fromList(queryResult);
  }

  @override
  Future<bool> update(Order order) async {
    final Database db = await databaseProvider.db();
    int result = await db.update(dao.tableName, dao.toMap(order),
        where: "orderId = ?", whereArgs: [order.orderId]);

    return result > 0;
  }
}
