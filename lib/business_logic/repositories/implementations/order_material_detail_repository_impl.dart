import 'package:gio_gift_app/business_logic/dao/order_material_detail_dao.dart';
import 'package:gio_gift_app/business_logic/models/order_material_detail.dart';
import 'package:gio_gift_app/business_logic/database/database_provider.dart';
import 'package:sqflite/sqflite.dart';

import '../order_material_detail_repository.dart';

class OrderMaterialDetailRepositoryImpl
    implements OrderMaterialDetailRepository {
  final OrderMaterialDetailDao dao = OrderMaterialDetailDao();

  @override
  DatabaseProvider databaseProvider = DatabaseProvider.dbProvider;

  @override
  Future<bool> delete(String orderMaterialDetailId) async {
    final Database db = await databaseProvider.db();
    var result = await db.delete("${dao.tableName}",
        where: "orderMaterialDetailId = ?", whereArgs: [orderMaterialDetailId]);
    return result > 0;
  }

  @override
  Future<OrderMaterialDetail> get(String orderMaterialDetailId) async {
    final Database db = await databaseProvider.db();
    var queryResult = await db.query(dao.tableName,
        where: "orderMaterialDetailId = ?", whereArgs: [orderMaterialDetailId]);
    if (queryResult.isNotEmpty) {
      return dao.fromMap(queryResult.first);
    }

    return null;
  }

  @override
  Future<List<OrderMaterialDetail>> getOrderMaterialDetails(
      String orderMaterialId) async {
    final Database db = await databaseProvider.db();
    final String query = """
      SELECT * FROM [OrderMaterialDetail] OMD
      INNER JOIN [RawMaterial] RM ON RM.rawMaterialId = OMD.rawMaterialId
      WHERE OMD.[orderMaterialId] = ?
    """;
    var queryResult = await db.rawQuery(query, [orderMaterialId]);
    if (queryResult.isNotEmpty) {
      return dao.fromList(queryResult);
    }

    return null;
  }

  @override
  Future<bool> insert(OrderMaterialDetail orderMaterialDetail) async {
    final Database db = await databaseProvider.db();
    var result = await db.insert(dao.tableName, dao.toMap(orderMaterialDetail));
    return result > 0;
  }

  @override
  Future<bool> update(OrderMaterialDetail orderMaterialDetail) async {
    final Database db = await databaseProvider.db();
    var result = await db.update(dao.tableName, dao.toMap(orderMaterialDetail),
        where: "orderMaterialDetailId = ?",
        whereArgs: [orderMaterialDetail.orderMaterialDetailId]);
    return result > 0;
  }

  @override
  Future<bool> deleteByOrderMaterialId(String orderMaterialId) async {
    final Database db = await databaseProvider.db();
    var result = await db.delete("${dao.tableName}",
        where: "orderMaterialId = ?", whereArgs: [orderMaterialId]);
    return result > 0;
  }
}
