import 'package:gio_gift_app/business_logic/dao/generated_number_dao.dart';
import 'package:gio_gift_app/business_logic/models/generated_number.dart';
import 'package:gio_gift_app/business_logic/database/database_provider.dart';
import 'package:gio_gift_app/business_logic/repositories/generated_number_repository.dart';
import 'package:sqflite/sqflite.dart';

class GeneratedNumberRepositoryImpl implements GeneratedNumberRepository {
  final dao = GeneratedNumberDao();

  @override
  DatabaseProvider databaseProvider = DatabaseProvider.dbProvider;

  GeneratedNumberRepositoryImpl();

  @override
  Future<void> createGeneratedNumber(GeneratedNumber generatedNumber) async {
    final Database db = await databaseProvider.db();
    await db.insert(dao.tableName, dao.toMap(generatedNumber));
  }

  @override
  Future<GeneratedNumber> getGeneratedNumber(
      String generatedNumberName, int month, int year) async {
    final Database db = await databaseProvider.db();
    var queryResult = await db.query(dao.tableName,
        where: "generatedNumberName = ? AND month = ? AND year = ?",
        whereArgs: [generatedNumberName, month, year]);
    if (queryResult.isNotEmpty) {
      return dao.fromMap(queryResult.first);
    }

    return null;
  }

  @override
  Future<void> updateGeneratedNumber(GeneratedNumber generatedNumber) async {
    final Database db = await databaseProvider.db();
    await db.update(dao.tableName, dao.toMap(generatedNumber),
        where: "generatedNumberId = ?",
        whereArgs: [generatedNumber.generatedNumberId]);
  }
}
