import 'package:gio_gift_app/business_logic/dao/raw_material_dao.dart';
import 'package:gio_gift_app/business_logic/models/raw_material.dart';
import 'package:gio_gift_app/business_logic/database/database_provider.dart';
import 'package:gio_gift_app/business_logic/repositories/raw_material_repository.dart';

class RawMaterialRepositoryImpl implements RawMaterialRepository {
  final dao = RawMaterialDao();
  @override
  DatabaseProvider databaseProvider = DatabaseProvider.dbProvider;

  @override
  Future<bool> delete(String rawMaterialId) async {
    final db = await databaseProvider.db();
    var result = await db.delete(dao.tableName,
        where: "rawMaterialId = ?", whereArgs: [rawMaterialId]);
    return result > 0;
  }

  @override
  Future<RawMaterial> get(String rawMaterialId) async {
    final db = await databaseProvider.db();
    var queryResult = await db.query(dao.tableName,
        where: "rawMaterialId = ?", whereArgs: [rawMaterialId]);
    if (queryResult.isNotEmpty) {
      return dao.fromMap(queryResult.first);
    }

    return null;
  }

  @override
  Future<List<RawMaterial>> getAvailableRawMaterials() async {
    final db = await databaseProvider.db();
    var queryResult =
        await db.rawQuery("SELECT * FROM rawMaterial WHERE qty > 0");
    if (queryResult.isNotEmpty) {
      return dao.fromList(queryResult);
    }

    return null;
  }

  @override
  Future<List<RawMaterial>> getRawMaterials() async {
    final db = await databaseProvider.db();
    var queryResult = await db.query(dao.tableName);
    return dao.fromList(queryResult);
  }

  @override
  Future<bool> insert(RawMaterial rawMaterial) async {
    final db = await databaseProvider.db();
    var result = await db.insert(dao.tableName, dao.toMap(rawMaterial));
    return result > 0;
  }

  @override
  Future<bool> update(RawMaterial rawMaterial) async {
    final db = await databaseProvider.db();
    var result = await db.update(dao.tableName, dao.toMap(rawMaterial),
        where: "rawMaterialId = ?", whereArgs: [rawMaterial.rawMaterialId]);
    return result > 0;
  }
}
