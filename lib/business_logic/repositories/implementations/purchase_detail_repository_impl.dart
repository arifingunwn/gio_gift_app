import 'package:gio_gift_app/business_logic/dao/purchase_detail_dao.dart';
import 'package:gio_gift_app/business_logic/models/purchase_detail.dart';
import 'package:gio_gift_app/business_logic/database/database_provider.dart';
import 'package:gio_gift_app/business_logic/repositories/purchase_detail_repository.dart';
import 'package:sqflite/sqflite.dart';

class PurchaseDetailRepositoryImpl implements PurchaseDetailRepository {
  final dao = PurchaseDetailDao();

  @override
  DatabaseProvider databaseProvider = DatabaseProvider.dbProvider;

  @override
  Future<PurchaseDetail> get(String purchaseDetailId) async {
    final Database db = await databaseProvider.db();
    var queryResult = await db.query(dao.tableName,
        where: "purchaseDetailId = ?", whereArgs: [purchaseDetailId]);
    if (queryResult.isNotEmpty) {
      return dao.fromMap(queryResult.first);
    }

    return null;
  }

  @override
  Future<List<PurchaseDetail>> getPurchaseDetails(String purchaseId) async {
    final Database db = await databaseProvider.db();
    var queryResult = await db
        .query(dao.tableName, where: "purchaseId = ?", whereArgs: [purchaseId]);
    if (queryResult.isNotEmpty) {
      return dao.fromList(queryResult);
    }

    return null;
  }

  @override
  Future<bool> insert(PurchaseDetail purchaseDetail) async {
    final Database db = await databaseProvider.db();
    var result = await db.insert(dao.tableName, dao.toMap(purchaseDetail));
    return result > 0;
  }

  @override
  Future<bool> delete(String purchaseDetailId) async {
    final Database db = await databaseProvider.db();
    var result = await db.delete(dao.tableName,
        where: "purchaseDetailId = ?", whereArgs: [purchaseDetailId]);
    return result > 0;
  }
}
