import 'package:gio_gift_app/business_logic/dao/purchase_dao.dart';
import 'package:gio_gift_app/business_logic/models/purchase.dart';
import 'package:gio_gift_app/business_logic/database/database_provider.dart';
import 'package:sqflite/sqflite.dart';
import '../purchase_repository.dart';

class PurchaseRepositoryImpl implements PurchaseRepository {
  PurchaseDao dao = PurchaseDao();

  @override
  DatabaseProvider databaseProvider = DatabaseProvider.dbProvider;

  @override
  Future<Purchase> get(String purchaseId) async {
    final Database db = await databaseProvider.db();
    var queryResult = await db.rawQuery(
        "SELECT * FROM ${dao.tableName} WHERE purchaseId = ?", [purchaseId]);
    if (queryResult.isNotEmpty) {
      return dao.fromMap(queryResult.first);
    }

    return null;
  }

  @override
  Future<List<Purchase>> getPurchases() async {
    final Database db = await databaseProvider.db();
    var queryResult = await db.rawQuery("SELECT * FROM ${dao.tableName}");
    return dao.fromList(queryResult);
  }

  @override
  Future<bool> insert(Purchase purchase) async {
    final Database db = await databaseProvider.db();
    var result = await db.insert(dao.tableName, dao.toMap(purchase));
    return result > 0;
  }

  @override
  Future<bool> update(Purchase purchase) async {
    final Database db = await databaseProvider.db();
    var result = await db.update(dao.tableName, dao.toMap(purchase),
        where: "purchaseId = ?", whereArgs: [purchase.purchaseId]);
    return result > 0;
  }
}
