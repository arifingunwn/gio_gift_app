import 'package:gio_gift_app/business_logic/dao/dashboard_data_dao.dart';
import 'package:gio_gift_app/business_logic/database/database_provider.dart';
import 'package:gio_gift_app/business_logic/models/dashboard_data.dart';
import 'package:gio_gift_app/business_logic/repositories/dashboard_repository.dart';
import 'package:sqflite/sqflite.dart';

class DashboardRepositoryImpl implements DashboardRepository {
  final dao = DashboardDataDao();
  DatabaseProvider databaseProvider = DatabaseProvider.dbProvider;

  @override
  Future<DashboardData> getDashboardData() async {
    final Database db = await databaseProvider.db();
    final String query = """ 
      SELECT 
        (SELECT COUNT(1) FROM [order] WHERE orderStatus = 'PENDING') as orderPendingCount,
        (SELECT COUNT(1) FROM [order] WHERE orderStatus = 'READYTOSHIP') as orderReadyToShipCount,
        (SELECT COUNT(1) FROM [order] WHERE orderStatus = 'INPROGRESS') as orderInProgressCount
    """;
    var queryResult = await db.rawQuery(query);
    if (queryResult.isNotEmpty) {
      return dao.fromMap(queryResult.first);
    }

    return null;
  }
}
