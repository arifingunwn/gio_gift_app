import 'package:gio_gift_app/business_logic/dao/order_material_dao.dart';
import 'package:gio_gift_app/business_logic/models/order_material.dart';
import 'package:gio_gift_app/business_logic/database/database_provider.dart';
import 'package:sqflite/sqflite.dart';
import '../order_material_repository.dart';

class OrderMaterialRepositoryImpl implements OrderMaterialRepository {
  final dao = OrderMaterialDao();
  @override
  DatabaseProvider databaseProvider = DatabaseProvider.dbProvider;

  @override
  Future<void> delete(String orderMaterialId) async {
    final Database db = await databaseProvider.db();
    var result = await db.delete("${dao.tableName}",
        where: "orderMaterialId = ?", whereArgs: [orderMaterialId]);
    return result > 0;
  }

  @override
  Future<OrderMaterial> get(String orderId) async {
    try {
      final Database db = await databaseProvider.db();
      var queryResult = await db.rawQuery(
          "SELECT * FROM ${dao.tableName} WHERE orderId = ?", [orderId]);
      if (queryResult.isNotEmpty) {
        return dao.fromMap(queryResult.first);
      }

      return null;
    } catch (e) {
      throw e;
    }
  }

  @override
  Future<void> insert(OrderMaterial orderMaterial) async {
    final Database db = await databaseProvider.db();
    var result = await db.insert(dao.tableName, dao.toMap(orderMaterial));
    return result > 0;
  }
}
