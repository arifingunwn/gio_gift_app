import 'package:gio_gift_app/business_logic/dao/customer_dao.dart';
import 'package:gio_gift_app/business_logic/database/database_provider.dart';
import 'package:gio_gift_app/business_logic/models/customer.dart';
import 'package:gio_gift_app/business_logic/repositories/customer_repository.dart';
import 'package:sqflite/sqflite.dart';

class CustomerRepositoryImpl implements CustomerRepository {
  final dao = CustomerDao();

  @override
  DatabaseProvider databaseProvider = DatabaseProvider.dbProvider;

  @override
  Future<bool> delete(String customerId) async {
    final Database db = await databaseProvider.db();
    var result = await db.delete(dao.tableName,
        where: "customerId = ?", whereArgs: [customerId]);
    return result > 0;
  }

  @override
  Future<Customer> get(customerId) async {
    final Database db = await databaseProvider.db();
    var queryResult = await db
        .query(dao.tableName, where: "customerId = ?", whereArgs: [customerId]);
    if (queryResult.isNotEmpty) {
      return dao.fromMap(queryResult.first);
    }

    return null;
  }

  @override
  Future<Customer> getCustomerByPhoneNumber(String phoneNumber) async {
    final Database db = await databaseProvider.db();
    var queryResult = await db.query(dao.tableName,
        where: "phoneNumber = ?", whereArgs: [phoneNumber]);
    if (queryResult.isNotEmpty) {
      return dao.fromMap(queryResult.first);
    }

    return null;
  }

  @override
  Future<List<Customer>> getCustomers() async {
    final Database db = await databaseProvider.db();
    var queryResult = await db.query(dao.tableName);
    if (queryResult.isNotEmpty) {
      return dao.fromList(queryResult);
    }

    return null;
  }

  @override
  Future<bool> insert(Customer customer) async {
    final Database db = await databaseProvider.db();
    var result = await db.insert(dao.tableName, dao.toMap(customer));
    return result > 0;
  }

  @override
  Future<bool> update(Customer customer) async {
    final Database db = await databaseProvider.db();
    var result = await db.update(dao.tableName, dao.toMap(customer),
        where: "customerId = ?", whereArgs: [customer.customerId]);
    return result > 0;
  }
}
