import 'package:gio_gift_app/business_logic/dao/transaction_dao.dart';
import 'package:gio_gift_app/business_logic/models/transaction.dart';
import 'package:gio_gift_app/business_logic/database/database_provider.dart';
import 'package:gio_gift_app/business_logic/repositories/transaction_repository.dart';

class TransactionRepositoryImpl implements TransactionRepository {
  final dao = TransactionDao();
  @override
  DatabaseProvider databaseProvider = DatabaseProvider.dbProvider;

  @override
  Future<List<Transaction>> getTransactions(
      DateTime startDate, DateTime endDate, int offset, int limit) async {
    final db = await databaseProvider.db();
    var queryResult = await db.rawQuery(
        "SELECT * FROM [Transaction] WHERE datetime(transactionDate) >= datetime(?) AND datetime(transactionDate) <= datetime(?) ORDER BY datetime(transactionDate) DESC LIMIT ?, ?",
        [startDate.toString(), endDate.toString(), offset, limit]);
    if (queryResult.isNotEmpty) {
      return dao.fromList(queryResult);
    }

    return null;
  }

  @override
  Future<Transaction> getLastTransaction() async {
    final db = await databaseProvider.db();
    var queryResult = await db.rawQuery(
        "SELECT * FROM [Transaction] ORDER BY datetime(transactionDate) DESC LIMIT 1");
    if (queryResult.isNotEmpty) {
      return dao.fromMap(queryResult.first);
    }

    return null;
  }

  @override
  Future<bool> insert(Transaction trans) async {
    final db = await databaseProvider.db();
    var result = await db.insert(dao.tableName, dao.toMap(trans));
    return result > 0;
  }

  @override
  Future<bool> update(Transaction trans) async {
    final db = await databaseProvider.db();
    var result = await db.update(dao.tableName, dao.toMap(trans),
        where: "TransactionId = ?", whereArgs: [trans.transactionId]);
    return result > 0;
  }
}
