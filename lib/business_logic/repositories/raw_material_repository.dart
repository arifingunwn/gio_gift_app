import 'package:gio_gift_app/business_logic/database/database_provider.dart';
import '../models/raw_material.dart';

abstract class RawMaterialRepository {
  DatabaseProvider databaseProvider;
  Future<RawMaterial> get(String rawMaterialId);
  Future<List<RawMaterial>> getRawMaterials();
  Future<List<RawMaterial>> getAvailableRawMaterials();
  Future<bool> insert(RawMaterial rawMaterial);
  Future<bool> update(RawMaterial rawMaterial);
  Future<bool> delete(String rawMaterialId);
}
