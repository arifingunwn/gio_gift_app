import 'package:gio_gift_app/business_logic/database/database_provider.dart';
import 'package:gio_gift_app/business_logic/models/generated_number.dart';

abstract class GeneratedNumberRepository {
  DatabaseProvider databaseProvider;
  Future<GeneratedNumber> getGeneratedNumber(
      String generatedNumberName, int month, int year);
  Future<void> createGeneratedNumber(GeneratedNumber generatedNumber);
  Future<void> updateGeneratedNumber(GeneratedNumber generatedNumber);
}
