import 'package:gio_gift_app/business_logic/database/database_provider.dart';
import 'package:gio_gift_app/business_logic/models/purchase.dart';

abstract class PurchaseRepository {
  DatabaseProvider databaseProvider;
  Future<Purchase> get(String purchaseId);
  Future<List<Purchase>> getPurchases();
  Future<bool> insert(Purchase purchase);
  Future<bool> update(Purchase purchase);
}
