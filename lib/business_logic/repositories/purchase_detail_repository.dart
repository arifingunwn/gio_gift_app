import 'package:gio_gift_app/business_logic/database/database_provider.dart';

import '../models/purchase_detail.dart';

abstract class PurchaseDetailRepository {
  DatabaseProvider databaseProvider;
  Future<PurchaseDetail> get(String purchaseDetailId);
  Future<bool> insert(PurchaseDetail purchaseDetail);
  Future<bool> delete(String purchaseDetailId);
  Future<List<PurchaseDetail>> getPurchaseDetails(String purchaseId);
}
