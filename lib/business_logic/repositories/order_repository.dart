import 'package:gio_gift_app/business_logic/database/database_provider.dart';
import 'package:gio_gift_app/business_logic/models/order.dart';

abstract class OrderRepository {
  DatabaseProvider databaseProvider;

  Future<Order> get(String orderId);
  Future<bool> insert(Order order);
  Future<bool> update(Order order);
  Future<List<Order>> getOrders();
  Future<List<Order>> getOrdersByStatus(String orderStatus);
  Future<List<Order>> getOrderInThisMonth();
}
