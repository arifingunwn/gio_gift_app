import 'package:flutter/material.dart';

class DashboardData {
  final int orderPendingCount;
  final int orderReadyToShipCount;
  final int orderInProgressCount;

  const DashboardData(
      {@required this.orderPendingCount,
      @required this.orderReadyToShipCount,
      @required this.orderInProgressCount});
}
