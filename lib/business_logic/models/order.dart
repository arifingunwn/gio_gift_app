import 'dart:core';
import 'package:flutter/cupertino.dart';

class Order {
  String orderId;
  String orderNo;
  String orderName;
  String customerName;
  String receiverName;
  String address;
  String city;
  String phoneNumber;
  DateTime dueDate;
  String courierName;
  double shippingCost = 0.0;
  String wishCardDescription;
  String orderDescription;
  double price = 0.0;
  String orderStatus;
  DateTime createdAt;

  Order(
      {this.orderId,
      this.orderNo,
      this.orderName,
      this.customerName,
      this.receiverName,
      this.address,
      this.city,
      this.phoneNumber,
      this.dueDate,
      this.courierName,
      this.shippingCost,
      this.wishCardDescription,
      this.orderDescription,
      this.price,
      this.orderStatus,
      this.createdAt});

  Order copyWith({
    String orderId,
    String orderNo,
    String orderName,
    String customerName,
    String receiverName,
    String address,
    String city,
    String phoneNumber,
    DateTime dueDate,
    String courierName,
    double shippingCost,
    String wishCardDescription,
    String orderDescription,
    double price,
    String orderStatus,
    DateTime createdAt,
  }) {
    return Order(
      orderId: orderId ?? this.orderId,
      orderNo: orderNo ?? this.orderNo,
      orderName: orderName ?? this.orderName,
      customerName: customerName ?? this.customerName,
      receiverName: receiverName ?? this.receiverName,
      address: address ?? this.address,
      city: city ?? this.city,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      dueDate: dueDate ?? this.dueDate,
      courierName: courierName ?? this.courierName,
      shippingCost: shippingCost ?? this.shippingCost,
      wishCardDescription: wishCardDescription ?? this.wishCardDescription,
      orderDescription: orderDescription ?? this.orderDescription,
      price: price ?? this.price,
      orderStatus: orderStatus ?? this.orderStatus,
      createdAt: createdAt ?? this.createdAt,
    );
  }

  // Map<String, Object> toDocument() {
  //   return {
  //     "OrderNo": orderNo,
  //     "OrderName": orderName,
  //     "CustomerName": customerName,
  //     "ReceiverName": receiverName,
  //     "Address": address,
  //     "City": city,
  //     "PhoneNumber": phoneNumber,
  //     "DueDate": dueDate,
  //     "CourierName": courierName,
  //     "ShippingCost": shippingCost,
  //     "WishCardDescription": wishCardDescription,
  //     "OrderDescription": orderDescription,
  //     "Price": price,
  //     "OrderStatus": orderStatus,
  //     "CreatedAt": createdAt
  //   };
  // }

  // static fromSnapshot(DocumentSnapshot snapshot) {
  //   return Order(
  //       orderId: snapshot.id,
  //       orderName: snapshot["OrderName"],
  //       orderNo: snapshot["OrderNo"],
  //       customerName: snapshot["CustomerName"],
  //       receiverName: snapshot["ReceiverName"],
  //       address: snapshot["Address"],
  //       city: snapshot["City"],
  //       phoneNumber: snapshot["PhoneNumber"],
  //       dueDate: snapshot["DueDate"].toDate(),
  //       courierName: snapshot["CourierName"],
  //       shippingCost: double.parse(snapshot["ShippingCost"].toString()),
  //       wishCardDescription: snapshot["WishCardDescription"],
  //       orderDescription: snapshot["OrderDescription"],
  //       price: double.parse(snapshot["Price"].toString()),
  //       orderStatus: snapshot["OrderStatus"],
  //       createdAt: snapshot["CreatedAt"].toDate());
  // }
}

enum OrderStatus { ALL, INPROGRESS, PENDING, READYTOSHIP, COMPLETE }
