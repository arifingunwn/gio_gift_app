import 'package:gio_gift_app/business_logic/models/raw_material.dart';

import 'order_material.dart';

class OrderMaterialDetail {
  String orderMaterialDetailId;
  OrderMaterial orderMaterial;
  RawMaterial rawMaterial;
  double qtyUsed = 0;
  double cogs = 0;

  OrderMaterialDetail(
      {this.orderMaterialDetailId,
      this.orderMaterial,
      this.rawMaterial,
      this.qtyUsed,
      this.cogs});
  OrderMaterialDetail copyWith(
      {String orderMaterialDetailId,
      String orderMaterialId,
      RawMaterial rawMaterial,
      double qtyUsed,
      double cogs}) {
    return OrderMaterialDetail(
        orderMaterialDetailId:
            orderMaterialDetailId ?? this.orderMaterialDetailId,
        orderMaterial: OrderMaterial(orderMaterialId: orderMaterialId) ??
            this.orderMaterial,
        rawMaterial: rawMaterial ?? this.rawMaterial,
        qtyUsed: qtyUsed ?? this.qtyUsed,
        cogs: cogs ?? this.cogs);
  }
}
