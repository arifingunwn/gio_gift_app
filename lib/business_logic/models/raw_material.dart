import 'dart:core';

class RawMaterial {
  final String rawMaterialId;
  final String materialName;
  final double qty;
  final double cogs;

  const RawMaterial(
      {this.rawMaterialId, this.materialName, this.qty, this.cogs});

  RawMaterial copyWith({String materialName, double qty, double cogs}) {
    return RawMaterial(
        rawMaterialId: this.rawMaterialId,
        materialName: materialName ?? this.materialName,
        qty: qty ?? this.qty,
        cogs: cogs ?? this.cogs);
  }

  // Map<String, Object> toDocument() {
  //   return {
  //     "MaterialName": materialName,
  //     "Qty": qty ?? 0.0,
  //     "cogs": cogs ?? 0.0
  //   };
  // }
  // static fromSnapshot(DocumentSnapshot snapshot) {
  //   return RawMaterial(
  //       rawMaterialId: snapshot.id,
  //       materialName: snapshot["MaterialName"],
  //       qty: snapshot["Qty"]?.toDouble(),
  //       cogs: snapshot["COGS"]?.toDouble());
  // }
}
