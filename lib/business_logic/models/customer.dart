class Customer {
  String customerId;
  String customerName;
  String phoneNumber;
  String address;
  String city;
  DateTime createdAt;

  Customer(
      {this.customerId,
      this.customerName,
      this.phoneNumber,
      this.address,
      this.city,
      this.createdAt});

  // Map<String, Object> toDocument() {
  //   return {
  //     "CustomerName": customerName,
  //     "Address": address,
  //     "City": city,
  //     "PhoneNumber": phoneNumber,
  //     "CreatedAt": createdAt
  //   };
  // }

  // static fromSnapshot(DocumentSnapshot snapshot) {
  //   return Customer(
  //       customerId: snapshot.id,
  //       customerName: snapshot["CustomerName"],
  //       address: snapshot["Address"],
  //       city: snapshot["City"],
  //       phoneNumber: snapshot["PhoneNumber"],
  //       createdAt: snapshot["CreatedAt"].toDate());
  // }
}
