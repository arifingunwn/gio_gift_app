import 'order.dart';
import 'order_material_detail.dart';

class OrderMaterial {
  String orderMaterialId;
  Order order;
  List<OrderMaterialDetail> orderMaterialDetails;
  DateTime createdAt;

  OrderMaterial(
      {this.orderMaterialId,
      this.order,
      this.orderMaterialDetails,
      this.createdAt});

  OrderMaterial copyWith(
      {String orderMaterialId,
      String orderId,
      List<OrderMaterialDetail> orderMaterialDetails,
      DateTime createdAt}) {
    return OrderMaterial(
        orderMaterialId: orderMaterialId ?? this.orderMaterialId,
        order: Order(orderId: orderId) ?? this.order,
        orderMaterialDetails: orderMaterialDetails ?? this.orderMaterialDetails,
        createdAt: createdAt ?? this.createdAt);
  }

  // static OrderMaterial fromSnapshot(DocumentSnapshot snapshot) {
  //   return OrderMaterial(
  //       orderMaterialId: snapshot.id,
  //       createdAt: snapshot["CreatedAt"].toDate());
  // }
}
