import 'package:gio_gift_app/business_logic/models/Purchase_detail.dart';

class Purchase {
  String purchaseId;
  String purchaseNo;
  String supplierName;
  List<PurchaseDetail> purchaseDetail;
  double totalAmount;
  DateTime purchaseDate;
  String description;
  DateTime createdAt;

  Purchase(
      {this.purchaseId,
      this.purchaseNo,
      this.supplierName,
      this.purchaseDetail,
      this.totalAmount,
      this.purchaseDate,
      this.description,
      this.createdAt});

  Purchase copyWith({
    String purchaseNo,
    String supplierName,
    List<PurchaseDetail> purchaseDetail,
    double totalAmount,
    DateTime purchaseDate,
    String description,
    DateTime createdAt,
  }) {
    return Purchase(
      purchaseId: this.purchaseId,
      purchaseNo: purchaseNo ?? this.purchaseNo,
      supplierName: supplierName ?? this.supplierName,
      purchaseDetail: purchaseDetail ?? this.purchaseDetail,
      totalAmount: totalAmount ?? this.totalAmount,
      purchaseDate: purchaseDate ?? this.purchaseDate,
      description: description ?? this.description,
      createdAt: createdAt ?? this.createdAt,
    );
  }
  // Map<String, Object> toDocument() {
  //   return {
  //     "PurchaseNo": purchaseNo,
  //     "SupplierName": supplierName,
  //     "TotalAmount": totalAmount,
  //     "PurchaseDate": purchaseDate,
  //     "Description": description,
  //     "CreatedAt": createdAt
  //   };
  // }

  // static fromSnapshot(DocumentSnapshot snapshot) {
  //   return Purchase(
  //       purchaseId: snapshot.id,
  //       purchaseNo: snapshot["PurchaseNo"],
  //       supplierName: snapshot["SupplierName"],
  //       totalAmount: double.parse(snapshot["TotalAmount"].toString()),
  //       purchaseDate: snapshot["PurchaseDate"].toDate(),
  //       description: snapshot["Description"],
  //       createdAt: snapshot["CreatedAt"].toDate());
  // }
}
