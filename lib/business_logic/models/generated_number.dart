class GeneratedNumber {
  String generatedNumberId;
  String generatedNumberName;
  int lastGeneratedNumber;
  int month;
  int year;

  GeneratedNumber(
      {this.generatedNumberId,
      this.generatedNumberName,
      this.lastGeneratedNumber,
      this.month,
      this.year});

  GeneratedNumber copyWith({int lastGeneratedNumber}) {
    return GeneratedNumber(
        generatedNumberId: this.generatedNumberId,
        generatedNumberName: this.generatedNumberName,
        lastGeneratedNumber: lastGeneratedNumber ?? this.lastGeneratedNumber,
        month: this.month,
        year: this.year);
  }

  // Map<String, Object> toDocument() {
  //   return {
  //     "GeneratedNumberName": this.generatedNumberName,
  //     "LastGeneratedNumber": this.lastGeneratedNumber,
  //     "Month": this.month,
  //     "Year": this.year
  //   };
  // }

  // static GeneratedNumber fromSnapshot(DocumentSnapshot snapshot) {
  //   return GeneratedNumber(
  //     generatedNumberId: snapshot.id,
  //     generatedNumberName: snapshot["GeneratedNumberName"],
  //     lastGeneratedNumber: int.parse(snapshot["LastGeneratedNumber"].toString()),
  //     month: int.parse(snapshot["Month"].toString()),
  //     year: int.parse(snapshot["Year"].toString()),
  //   );
  // }
}
