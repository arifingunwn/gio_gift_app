import 'raw_material.dart';

class PurchaseDetail {
  String purchaseDetailId;
  String purchaseId;
  RawMaterial rawMaterial;
  double qty;
  double price;

  PurchaseDetail(
      {this.purchaseDetailId,
      this.purchaseId,
      this.rawMaterial,
      this.qty,
      this.price});

  double get totalPrice => qty * price;

  PurchaseDetail copyWith(
      {String purchaseDetailId,
      String purchaseId,
      RawMaterial rawMaterial,
      double qty,
      double price}) {
    return PurchaseDetail(
        purchaseDetailId: purchaseDetailId ?? this.purchaseDetailId,
        purchaseId: purchaseId ?? this.purchaseId,
        rawMaterial: rawMaterial ?? this.rawMaterial,
        qty: qty ?? this.qty,
        price: price ?? this.price);
  }

  // Map<String, Object> toDocument() {
  //   return {
  //     "PurchaseId": purchaseId,
  //     "RawMaterialId": rawMaterial.rawMaterialId,
  //     "Qty": qty,
  //     "Price": price
  //   };
  // }

  // static PurchaseDetail fromSnapshot(DocumentSnapshot snapshot) {
  //   return PurchaseDetail(
  //       purchaseDetailId: snapshot.id,
  //       purchaseId: snapshot["PurchaseId"],
  //       qty: double.parse(snapshot["Qty"].toString()),
  //       price: double.parse(snapshot["Price"].toString()),
  //       rawMaterial: RawMaterial(rawMaterialId: snapshot["RawMaterialId"]));
  // }
}
