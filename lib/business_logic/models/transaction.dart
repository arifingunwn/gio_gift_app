class Transaction {
  final String transactionId;
  final DateTime transactionDate;
  final TransactionType transactionType;
  final String referenceId;
  final String referenceNo;
  final String description;
  final double debit;
  final double credit;
  final double balance;
  final DateTime createdAt;

  String get getTransactionTypeString =>
      transactionType.toString().split('.').last;

  Transaction(
      {this.transactionId,
      this.transactionType,
      this.referenceId,
      this.referenceNo,
      this.description,
      this.debit,
      this.credit,
      this.balance,
      this.transactionDate,
      this.createdAt});
}

enum TransactionType {
  ADD_PURCHASE,
  UPDATE_PURCHASE,
  PROCEED_ORDER,
  UPDATE_ORDER,
  SHIP_ORDER,
  CANCEL_ORDER,
  PAYMENT,
  RECEIPTS
}
