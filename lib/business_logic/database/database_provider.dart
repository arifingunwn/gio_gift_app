import 'dart:async';
import 'package:gio_gift_app/business_logic/dao/customer_dao.dart';
import 'package:gio_gift_app/business_logic/dao/generated_number_dao.dart';
import 'package:gio_gift_app/business_logic/dao/order_dao.dart';
import 'package:gio_gift_app/business_logic/dao/order_material_dao.dart';
import 'package:gio_gift_app/business_logic/dao/order_material_detail_dao.dart';
import 'package:gio_gift_app/business_logic/dao/purchase_dao.dart';
import 'package:gio_gift_app/business_logic/dao/purchase_detail_dao.dart';
import 'package:gio_gift_app/business_logic/dao/raw_material_dao.dart';
import 'package:gio_gift_app/business_logic/dao/transaction_dao.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseProvider {
  static final dbProvider = DatabaseProvider._internal();
  static DatabaseProvider get = dbProvider;
  Database _db;

  DatabaseProvider._internal();

  Future<Database> db() async {
    if (_db == null) await _init();
    return _db;
  }

  Future _init() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, "giogiftapp.db");

    print(path);

    _db = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute(CustomerDao().createTableQuery);
      await db.execute(GeneratedNumberDao().createTableQuery);
      await db.execute(OrderDao().createTableQuery);
      await db.execute(OrderMaterialDao().createTableQuery);
      await db.execute(OrderMaterialDetailDao().createTableQuery);
      await db.execute(PurchaseDao().createTableQuery);
      await db.execute(PurchaseDetailDao().createTableQuery);
      await db.execute(RawMaterialDao().createTableQuery);
      await db.execute(TransactionDao().createTableQuery);
    });
  }
}
