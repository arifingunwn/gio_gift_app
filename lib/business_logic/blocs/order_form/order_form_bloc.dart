import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/order/order_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/order/order_event.dart';
import 'package:gio_gift_app/business_logic/models/order.dart';
import 'package:gio_gift_app/business_logic/services/customer_service.dart';
import 'package:gio_gift_app/business_logic/services/generated_number_service.dart';
import 'package:gio_gift_app/business_logic/services/order_service.dart';
import 'package:gio_gift_app/business_logic/models/customer.dart';
import 'package:gio_gift_app/locator.dart';
import 'package:uuid/uuid.dart';
import 'order_form_event.dart';
import 'order_form_state.dart';

class OrderFormBloc extends Bloc<OrderFormEvent, OrderFormState> {
  OrderService _orderService;
  CustomerService _customerService;
  GeneratedNumberService _generatedNumberService;
  OrderBloc _orderBloc;

  OrderFormBloc(this._orderBloc) : super(OrderFormInitial()) {
    _orderService = sl();
    _customerService = sl();
    _generatedNumberService = sl();
  }

  @override
  Stream<OrderFormState> mapEventToState(OrderFormEvent event) async* {
    if (event is OrderFormSave) {
      yield* _mapOrderFormSaveEventToState(event);
    }
    if (event is OrderFormCheckPhoneNumber) {
      yield* _mapOrderFormCheckPhoneNumberEventToState(event);
    }
    if (event is OrderFormLoadedEdit) {
      yield* _mapOrderFormLoadEditEventToState(event);
    }
  }

  Stream<OrderFormState> _mapOrderFormSaveEventToState(
      OrderFormSave event) async* {
    yield OrderFormSaveLoading();
    try {
      if (event.isSaveCustomer) {
        _customerService.addCustomer(Customer(
          customerId: Uuid().v4(),
          phoneNumber: event.order.phoneNumber,
          customerName: event.order.customerName,
          address: event.order.address,
          city: event.order.city,
        ));
      }

      final DateTime now = DateTime.now();
      if (event.isEdit) {
        await _orderService.updateOrder(
          orderId: event.order.orderId,
          phoneNumber: event.order.phoneNumber,
          customerName: event.order.customerName,
          receiverName: event.order.receiverName,
          address: event.order.address,
          city: event.order.city,
          orderName: event.order.orderName,
          dueDate: event.order.dueDate,
          wishCardDescription: event.order.wishCardDescription,
          orderDescription: event.order.orderDescription,
          price: event.order.price,
        );
      } else {
        final String orderNo = await _generatedNumberService.getGeneratedNumber(
            "ORDER", now.month, now.year);
        await _orderService.addOrder(
            orderId: Uuid().v4(),
            orderNo: orderNo,
            phoneNumber: event.order.phoneNumber,
            customerName: event.order.customerName,
            address: event.order.address,
            city: event.order.city,
            orderName: event.order.orderName,
            dueDate: event.order.dueDate,
            wishCardDescription: event.order.wishCardDescription,
            orderDescription: event.order.orderDescription,
            price: event.order.price,
            receiverName: event.order.receiverName,
            orderStatus: OrderStatus.PENDING);
      }

      _orderBloc.add(OrderFetch(orderStatus: "ALL"));
      yield OrderFormSaveSuccess();
    } catch (e) {
      yield OrderFormSaveError(e.toString());
    }
  }

  Stream<OrderFormState> _mapOrderFormCheckPhoneNumberEventToState(
      OrderFormCheckPhoneNumber event) async* {
    yield OrderFormCustomerFetchLoading();
    try {
      Customer customer =
          await _customerService.getCustomerByPhoneNumber(event.phoneNumber);
      if (customer != null) {
        yield OrderFormCustomerFetchSuccess(customer: customer);
      } else {
        yield OrderFormCustomerFetchEmpty();
      }
    } catch (e) {
      yield OrderFormCustomerFetchEmpty();
    }
  }

  Stream<OrderFormState> _mapOrderFormLoadEditEventToState(
      OrderFormLoadedEdit event) async* {
    yield OrderFormLoading();
    try {
      var order = await _orderService.get(event.orderId);
      yield OrderFormLoadEdit(order);
    } catch (e) {}
  }
}
