import 'package:equatable/equatable.dart';
import 'package:gio_gift_app/business_logic/models/customer.dart';
import 'package:gio_gift_app/business_logic/models/order.dart';

abstract class OrderFormState extends Equatable {
  const OrderFormState();

  @override
  List<Object> get props => [];
}

class OrderFormInitial extends OrderFormState {}

class OrderFormLoadEdit extends OrderFormState {
  final Order order;

  const OrderFormLoadEdit(this.order);

  List<Object> get props => [order];
}

class OrderFormSaveLoading extends OrderFormState {}

class OrderFormSaveSuccess extends OrderFormState {}

class OrderFormSaveError extends OrderFormState {
  final String error;

  const OrderFormSaveError(this.error);

  @override
  List<Object> get props => [];
}

class OrderFormCustomerFetchLoading extends OrderFormState {}

class OrderFormCustomerFetchEmpty extends OrderFormState {}

class OrderFormCustomerFetchSuccess extends OrderFormState {
  final Customer customer;
  OrderFormCustomerFetchSuccess({this.customer});

  @override
  List<Object> get props => [];
}

class OrderFormLoading extends OrderFormState {}
