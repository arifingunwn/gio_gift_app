import 'package:equatable/equatable.dart';
import 'package:gio_gift_app/business_logic/models/order.dart';

abstract class OrderFormEvent extends Equatable {
  const OrderFormEvent();

  @override
  List<Object> get props => [];
}

class OrderFormSave extends OrderFormEvent {
  final Order order;
  final bool isEdit;
  final bool isSaveCustomer;

  const OrderFormSave(this.order, this.isEdit, this.isSaveCustomer);

  @override
  List<Object> get props => [order, isEdit, isSaveCustomer];
}

class OrderFormLoadedEdit extends OrderFormEvent {
  final String orderId;

  const OrderFormLoadedEdit(this.orderId);

  @override
  List<Object> get props => [orderId];
}

class OrderFormCheckPhoneNumber extends OrderFormEvent {
  final String phoneNumber;

  const OrderFormCheckPhoneNumber({this.phoneNumber});

  @override
  List<Object> get props => [phoneNumber];
}
