part of 'purchase_form_bloc.dart';

class PurchaseFormState extends Equatable {
  final String supplierName;
  final DateTime purchaseDate;
  final String description;
  final bool isLoading;
  final bool isEdit;
  final bool isError;
  final bool isSuccess;
  final List<PurchaseDetail> purchaseDetails;

  double get getCalculatedTotalAmount =>
      purchaseDetails.fold(0, (prev, curr) => prev + curr.totalPrice);

  const PurchaseFormState(
      {this.supplierName,
      this.purchaseDate,
      this.description,
      this.isLoading,
      this.isEdit,
      this.isError,
      this.isSuccess,
      this.purchaseDetails});

  factory PurchaseFormState.initial() {
    return PurchaseFormState(
        supplierName: "",
        purchaseDate: DateTime.now(),
        description: "",
        isLoading: false,
        isEdit: false,
        isError: false,
        isSuccess: false,
        purchaseDetails: List<PurchaseDetail>());
  }

  factory PurchaseFormState.loadEdit(String supplierName, DateTime purchaseDate,
      String description, List<PurchaseDetail> purchaseDetails) {
    return PurchaseFormState(
        supplierName: supplierName,
        purchaseDate: purchaseDate,
        description: description,
        isLoading: false,
        isEdit: true,
        isError: true,
        isSuccess: false,
        purchaseDetails: purchaseDetails);
  }

  factory PurchaseFormState.updatePurchaseDetails(
      PurchaseFormState state, List<PurchaseDetail> purchaseDetails) {
    return state.copyWith(purchaseDetails: purchaseDetails);
  }

  factory PurchaseFormState.loading(PurchaseFormState state) {
    return state.copyWith(isLoading: true);
  }

  factory PurchaseFormState.error(PurchaseFormState state) {
    return state.copyWith(isError: true);
  }

  factory PurchaseFormState.success(PurchaseFormState state) {
    return state.copyWith(isSuccess: true);
  }

  PurchaseFormState copyWith(
      {String supplierName,
      DateTime purchaseDate,
      String description,
      bool isLoading,
      bool isEdit,
      bool isError,
      bool isSuccess,
      List<PurchaseDetail> purchaseDetails}) {
    return PurchaseFormState(
        supplierName: supplierName ?? this.supplierName,
        purchaseDate: purchaseDate ?? this.purchaseDate,
        description: description ?? this.description,
        isLoading: isLoading ?? this.isLoading,
        isEdit: isEdit ?? this.isEdit,
        isError: isError ?? this.isError,
        isSuccess: isSuccess ?? this.isSuccess,
        purchaseDetails: purchaseDetails ?? this.purchaseDetails);
  }

  @override
  List<Object> get props => [
        supplierName,
        purchaseDate,
        description,
        isLoading,
        isEdit,
        isError,
        isSuccess,
        purchaseDetails
      ];
}
