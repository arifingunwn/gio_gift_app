part of 'purchase_form_bloc.dart';

abstract class PurchaseFormEvent extends Equatable {
  const PurchaseFormEvent();

  @override
  List<Object> get props => [];
}

class PurchaseFormLoadEmpty extends PurchaseFormEvent {}

class PurchaseFormLoadEdit extends PurchaseFormEvent {
  final String purchaseId;

  const PurchaseFormLoadEdit(this.purchaseId);

  @override
  List<Object> get props => [purchaseId];
}

class PurchaseFormAddItem extends PurchaseFormEvent {
  final RawMaterial rawMaterial;
  final double qty;
  final double price;

  const PurchaseFormAddItem(this.rawMaterial, this.qty, this.price);

  @override
  List<Object> get props => [rawMaterial, qty, price];
}

class PurchaseFormRemoveItem extends PurchaseFormEvent {
  final String rawMaterialId;

  const PurchaseFormRemoveItem(this.rawMaterialId);

  @override
  List<Object> get props => [rawMaterialId];
}

class PurchaseFormSave extends PurchaseFormEvent {
  final String purchaseId;
  final String supplierName;
  final DateTime purchaseDate;
  final String description;
  final bool isEdit;

  const PurchaseFormSave(this.purchaseId, this.supplierName, this.purchaseDate,
      this.description, this.isEdit);

  @override
  List<Object> get props =>
      [purchaseId, supplierName, purchaseDate, description, isEdit];
}
