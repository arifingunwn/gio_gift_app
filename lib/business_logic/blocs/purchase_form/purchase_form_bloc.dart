import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:gio_gift_app/business_logic/blocs/purchase/purchase_bloc.dart';
import 'package:gio_gift_app/business_logic/models/raw_material.dart';
import 'package:gio_gift_app/business_logic/services/purchase_detail_service.dart';
import 'package:gio_gift_app/business_logic/services/purchase_service.dart';
import 'package:gio_gift_app/business_logic/models/purchase.dart';
import 'package:gio_gift_app/business_logic/models/purchase_detail.dart';
import 'package:gio_gift_app/locator.dart';

part 'purchase_form_event.dart';
part 'purchase_form_state.dart';

class PurchaseFormBloc extends Bloc<PurchaseFormEvent, PurchaseFormState> {
  final PurchaseBloc _purchaseBloc;

  StreamSubscription _purchaseItemSubscription;

  PurchaseFormBloc(this._purchaseBloc) : super(PurchaseFormState.initial());

  @override
  Future<void> close() {
    _purchaseItemSubscription?.cancel();
    return super.close();
  }

  @override
  Stream<PurchaseFormState> mapEventToState(
    PurchaseFormEvent event,
  ) async* {
    if (event is PurchaseFormLoadEmpty) {
      yield PurchaseFormState.initial();
    }

    if (event is PurchaseFormLoadEdit) {
      yield* _mapPurchaseFormLoadEditEventToState(event);
    }

    if (event is PurchaseFormAddItem) {
      yield* _mapPurchaseFormAddItemEventToState(event);
    }

    if (event is PurchaseFormRemoveItem) {
      yield* _mapPurchaseFormRemoveItemEventToState(event);
    }

    if (event is PurchaseFormSave) {
      yield* _mapPurchaseFormSaveEventToState(event);
    }
  }

  Stream<PurchaseFormState> _mapPurchaseFormLoadEditEventToState(
      PurchaseFormLoadEdit event) async* {
    yield PurchaseFormState.loading(state);
    try {
      PurchaseService purchaseService = sl();
      PurchaseDetailService purchaseDetailService = sl();
      final Purchase purchase = await purchaseService.get(event.purchaseId);
      final List<PurchaseDetail> purchaseDetails =
          await purchaseDetailService.getPurchaseDetails(event.purchaseId);

      yield PurchaseFormState.loadEdit(purchase.supplierName,
          purchase.purchaseDate, purchase.description, purchaseDetails);
    } catch (e) {
      PurchaseFormState.error(state);
    }
  }

  Stream<PurchaseFormState> _mapPurchaseFormSaveEventToState(
      PurchaseFormSave event) async* {
    try {
      PurchaseService purchaseService = sl();

      yield PurchaseFormState.loading(state);

      if (event.isEdit) {
        await purchaseService.updatePurchase(
            event.purchaseId,
            event.supplierName,
            event.purchaseDate,
            event.description,
            state.purchaseDetails);
      } else {
        await purchaseService.addPurchase(event.supplierName,
            event.purchaseDate, event.description, state.purchaseDetails);
      }

      yield PurchaseFormState.success(state);
      _purchaseBloc.add(PurchaseFetch());
    } catch (e) {
      print(e);
      yield PurchaseFormState.error(state);
    }
  }

  Stream<PurchaseFormState> _mapPurchaseFormAddItemEventToState(
      PurchaseFormAddItem event) async* {
    yield PurchaseFormState.loading(state);
    try {
      var isExists = state.purchaseDetails.firstWhere(
          (obj) =>
              obj.rawMaterial.rawMaterialId == event.rawMaterial.rawMaterialId,
          orElse: () => null);
      if (isExists != null) {
        return;
      }

      final purchaseDetail = PurchaseDetail(
          rawMaterial: event.rawMaterial, qty: event.qty, price: event.price);
      final newPurchaseDetails = [...state.purchaseDetails, purchaseDetail];

      yield PurchaseFormState.updatePurchaseDetails(state, newPurchaseDetails);
    } catch (e) {
      print(e);
      yield PurchaseFormState.error(state);
    }
  }

  Stream<PurchaseFormState> _mapPurchaseFormRemoveItemEventToState(
      PurchaseFormRemoveItem event) async* {
    final newPurchaseDetails = state.purchaseDetails
        .map((detail) {
          if (detail.rawMaterial.rawMaterialId != event.rawMaterialId) {
            return detail;
          }
        })
        .where((x) => x != null)
        .toList();
    yield PurchaseFormState.updatePurchaseDetails(state, newPurchaseDetails);
  }
}
