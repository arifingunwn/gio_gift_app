import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:gio_gift_app/business_logic/services/purchase_service.dart';
import 'package:gio_gift_app/business_logic/models/purchase.dart';

part 'purchase_event.dart';
part 'purchase_state.dart';

class PurchaseBloc extends Bloc<PurchaseEvent, PurchaseState> {
  final PurchaseService _purchaseService;

  PurchaseBloc(this._purchaseService) : super(PurchaseInitial());

  @override
  Stream<PurchaseState> mapEventToState(
    PurchaseEvent event,
  ) async* {
    if (event is PurchaseFetch) {
      yield* _mapPurchaseFetchEventToState();
    }

    if (event is PurchaseListUpdate) {
      yield PurchaseFetchSuccess(purchases: event.purchases);
    }
  }

  Stream<PurchaseState> _mapPurchaseFetchEventToState() async* {
    try {
      var purchases = await _purchaseService.getPurchases();
      add(PurchaseListUpdate(purchases: purchases));
    } catch (e) {
      print(e);
    }
  }
}
