part of 'purchase_bloc.dart';

abstract class PurchaseState extends Equatable {
  const PurchaseState();

  @override
  List<Object> get props => [];
}

class PurchaseInitial extends PurchaseState {}

class PurchaseFetchSuccess extends PurchaseState {
  final List<Purchase> purchases;

  const PurchaseFetchSuccess({this.purchases});

  @override
  List<Object> get props => [purchases];
}

class PurchaseFetchLoading extends PurchaseState {}

class PurchaseFetchError extends PurchaseState {
  final String error;

  const PurchaseFetchError({this.error});

  @override
  List<Object> get props => [error];
}
