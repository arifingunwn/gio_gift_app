part of 'purchase_bloc.dart';

abstract class PurchaseEvent extends Equatable {
  const PurchaseEvent();

  @override
  List<Object> get props => [];
}

class PurchaseFetch extends PurchaseEvent {}

class PurchaseListUpdate extends PurchaseEvent {
  final List<Purchase> purchases;

  const PurchaseListUpdate({this.purchases});

  @override
  List<Object> get props => [purchases];
}
