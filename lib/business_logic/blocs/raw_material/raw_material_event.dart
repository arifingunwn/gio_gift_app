import 'package:equatable/equatable.dart';
import 'package:gio_gift_app/business_logic/models/raw_material.dart';

abstract class RawMaterialEvent extends Equatable {
  const RawMaterialEvent();

  @override
  List<Object> get props => [];
}

class RawMaterialFetch extends RawMaterialEvent {
  const RawMaterialFetch();

  @override
  List<Object> get props => [];
}

class RawMaterialFilter extends RawMaterialEvent {
  final String filterString;

  const RawMaterialFilter(this.filterString);

  @override
  List<Object> get props => [filterString];
}

class RawMaterialUpdated extends RawMaterialEvent {
  final List<RawMaterial> rawMaterials;

  const RawMaterialUpdated(this.rawMaterials);

  @override
  List<Object> get props => [rawMaterials];
}

class RawMaterialAdded extends RawMaterialEvent {
  final String materialName;

  const RawMaterialAdded(this.materialName);

  @override
  List<Object> get props => [materialName];

  @override
  String toString() => 'RawMaterial Adding { $materialName }';
}

class RawMaterialUpdate extends RawMaterialEvent {
  final RawMaterial rawMaterial;

  const RawMaterialUpdate(this.rawMaterial);

  @override
  List<Object> get props => [rawMaterial];
}

class RawMaterialDeleted extends RawMaterialEvent {
  final String rawMaterialId;

  const RawMaterialDeleted(this.rawMaterialId);

  @override
  List<Object> get props => [rawMaterialId];
}

class RawMaterialUpdating extends RawMaterialEvent {
  final RawMaterial rawMaterial;

  const RawMaterialUpdating(this.rawMaterial);

  @override
  List<Object> get props => [rawMaterial];

  @override
  String toString() => 'RawMaterial Updating { $rawMaterial }';
}
