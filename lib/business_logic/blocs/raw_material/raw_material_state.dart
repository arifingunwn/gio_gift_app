import 'package:equatable/equatable.dart';
import 'package:gio_gift_app/business_logic/models/raw_material.dart';

abstract class RawMaterialState extends Equatable {
  const RawMaterialState();

  @override
  List<Object> get props => [];
}

class RawMaterialInitial extends RawMaterialState {}

class RawMaterialFetchLoading extends RawMaterialState {}

class RawMaterialFetchError extends RawMaterialState {
  final String error;

  const RawMaterialFetchError({this.error});

  @override
  String toString() => 'Fetch Error { error: $error}';
}

class RawMaterialListUpdated extends RawMaterialState {
  final List<RawMaterial> rawMaterials;

  RawMaterialListUpdated(this.rawMaterials);

  @override
  List<Object> get props => [rawMaterials];
}
