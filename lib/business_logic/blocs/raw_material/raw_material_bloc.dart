import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/raw_material/raw_material_event.dart';
import 'package:gio_gift_app/business_logic/blocs/raw_material/raw_material_state.dart';
import 'package:gio_gift_app/business_logic/models/raw_material.dart';
import 'package:gio_gift_app/business_logic/repositories/raw_material_repository.dart';
import 'package:uuid/uuid.dart';

class RawMaterialBloc extends Bloc<RawMaterialEvent, RawMaterialState> {
  final RawMaterialRepository _rawMaterialRepository;

  RawMaterialBloc(this._rawMaterialRepository)
      : assert(_rawMaterialRepository != null),
        super(RawMaterialInitial());

  @override
  Stream<RawMaterialState> mapEventToState(RawMaterialEvent event) async* {
    if (event is RawMaterialFetch) {
      yield* _mapRawMaterialFetchEventToState(event);
    }
    if (event is RawMaterialFilter) {
      yield* _mapRawMaterialFilterEventToState(event);
    }

    if (event is RawMaterialAdded) {
      yield* _mapRawMaterialAddedToState(event);
    }
    if (event is RawMaterialUpdate) {
      yield* _mapRawMaterialUpdateToState(event);
    }
    if (event is RawMaterialDeleted) {
      yield* _mapRawMaterialDeleteToState(event);
    }
  }

  Stream<RawMaterialState> _mapRawMaterialFetchEventToState(
      RawMaterialFetch event) async* {
    yield RawMaterialFetchLoading();
    try {
      List<RawMaterial> rawMaterials =
          await _rawMaterialRepository.getRawMaterials();
      yield RawMaterialListUpdated(rawMaterials);
    } catch (e) {
      throw e;
    }
  }

  Stream<RawMaterialState> _mapRawMaterialFilterEventToState(
      RawMaterialFilter event) async* {
    final List<RawMaterial> rawMaterials =
        await _rawMaterialRepository.getRawMaterials();

    final List<RawMaterial> filteredList = rawMaterials
        .map((rawMaterial) {
          if (event.filterString.isEmpty ||
              rawMaterial.materialName
                  .toLowerCase()
                  .contains(event.filterString.toLowerCase())) {
            return rawMaterial;
          }
        })
        .where((obj) => obj != null)
        .toList();
    yield RawMaterialListUpdated(filteredList);
  }

  Stream<RawMaterialState> _mapRawMaterialAddedToState(
      RawMaterialAdded event) async* {
    _rawMaterialRepository.insert(RawMaterial(
        rawMaterialId: Uuid().v4(),
        materialName: event.materialName,
        qty: 0,
        cogs: 0));
    if (state is RawMaterialListUpdated) {
      final List<RawMaterial> rawMaterials =
          List.from((state as RawMaterialListUpdated).rawMaterials);
      add(RawMaterialFetch());
    }
  }

  Stream<RawMaterialState> _mapRawMaterialUpdateToState(
      RawMaterialUpdate event) async* {
    _rawMaterialRepository.update(event.rawMaterial);
    if (state is RawMaterialListUpdated) {
      final List<RawMaterial> rawMaterials =
          List.from((state as RawMaterialListUpdated).rawMaterials);
      yield RawMaterialListUpdated(rawMaterials);
    }
  }

  Stream<RawMaterialState> _mapRawMaterialDeleteToState(
      RawMaterialDeleted event) async* {
    _rawMaterialRepository.delete(event.rawMaterialId);
    add(RawMaterialFetch());
  }
}
