part of 'finance_transaction_bloc.dart';

abstract class FinanceTransactionEvent extends Equatable {
  const FinanceTransactionEvent();

  @override
  List<Object> get props => [];
}

class FinanceTransactionFetch extends FinanceTransactionEvent {
  final int limit;
  final int offset;
  final DateTime startDate;
  final DateTime endDate;

  const FinanceTransactionFetch(
      {this.limit, this.offset, this.startDate, this.endDate});

  @override
  List<Object> get props => [limit, offset, startDate, endDate];
}

class FinanceTransactionSetFilter extends FinanceTransactionEvent {
  final String startDate;
  final String endDate;

  const FinanceTransactionSetFilter(this.startDate, this.endDate);

  @override
  List<Object> get props => [startDate, endDate];
}
