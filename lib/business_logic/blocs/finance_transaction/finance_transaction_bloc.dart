import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:gio_gift_app/business_logic/models/transaction.dart';
import 'package:gio_gift_app/business_logic/services/transaction_service.dart';
import 'package:gio_gift_app/locator.dart';
import 'package:rxdart/rxdart.dart';

part 'finance_transaction_event.dart';
part 'finance_transaction_state.dart';

class FinanceTransactionBloc
    extends Bloc<FinanceTransactionEvent, FinanceTransactionState> {
  FinanceTransactionBloc() : super(FinanceTransactionInitial());

  final int paginationLimit = 10;

  @override
  Stream<Transition<FinanceTransactionEvent, FinanceTransactionState>>
      transformEvents(Stream<FinanceTransactionEvent> event, transitionFn) {
    return super.transformEvents(
        event.debounceTime(Duration(milliseconds: 1000)), transitionFn);
  }

  @override
  Stream<FinanceTransactionState> mapEventToState(
    FinanceTransactionEvent event,
  ) async* {
    if (event is FinanceTransactionFetch) {
      yield* _mapFinanceTransactionFetchEventToState(event);
    }
    if (event is FinanceTransactionSetFilter) {
      yield* _mapFinanceTransactionSetFilterEventToState(event);
    }
  }

  Stream<FinanceTransactionState> _mapFinanceTransactionFetchEventToState(
      FinanceTransactionFetch event) async* {
    try {
      TransactionService service = sl();
      if (state is FinanceTransactionInitial) {
        final result = await service.getTransations(
            event.startDate, event.endDate, event.offset, this.paginationLimit);
        yield FinanceTransactionLoaded(
            financeTransactions: result, isReachedMax: false);
      }

      if (state is FinanceTransactionLoaded) {
        final prevState = (state as FinanceTransactionLoaded);

        yield FinanceTransactionLoadingBottom();

        if (prevState.isReachedMax) {
          yield prevState;
          return;
        }

        final newList = await service.getTransations(
            event.startDate,
            event.endDate,
            prevState.financeTransactions.length - 1,
            this.paginationLimit);

        if (newList == null) {
          yield prevState.copyWith(isreachedMax: true);
        } else {
          yield prevState.copyWith(financeTransactions: [
            ...prevState.financeTransactions,
            ...newList
          ]);
        }
      }
    } catch (e) {
      print(e);
      yield FinanceTransactionLoadError("Failed to Fetch Transactions.");
    }
  }

  Stream<FinanceTransactionState> _mapFinanceTransactionSetFilterEventToState(
      FinanceTransactionSetFilter event) async* {
    DateTime startDate = DateTime.tryParse(event.startDate);
    DateTime endDate = DateTime.tryParse(event.endDate);

    add(FinanceTransactionFetch(
        startDate: startDate,
        endDate: endDate,
        offset: 0,
        limit: this.paginationLimit));
  }
}
