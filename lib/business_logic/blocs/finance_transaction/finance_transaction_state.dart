part of 'finance_transaction_bloc.dart';

abstract class FinanceTransactionState extends Equatable {
  const FinanceTransactionState();
  @override
  List<Object> get props => [];
}

class FinanceTransactionInitial extends FinanceTransactionState {}

class FinanceTransactionLoaded extends FinanceTransactionState {
  final List<Transaction> financeTransactions;
  final bool isReachedMax;

  const FinanceTransactionLoaded({this.financeTransactions, this.isReachedMax});

  FinanceTransactionLoaded copyWith(
      {List<Transaction> financeTransactions, bool isreachedMax}) {
    return FinanceTransactionLoaded(
        financeTransactions: financeTransactions ?? this.financeTransactions,
        isReachedMax: isreachedMax ?? this.isReachedMax);
  }

  @override
  List<Object> get props => [financeTransactions, isReachedMax];
}

class FinanceTransactionLoading extends FinanceTransactionState {}

class FinanceTransactionLoadingBottom extends FinanceTransactionState {}

class FinanceTransactionLoadError extends FinanceTransactionState {
  final String error;

  const FinanceTransactionLoadError(this.error);

  @override
  List<Object> get props => [error];
}
