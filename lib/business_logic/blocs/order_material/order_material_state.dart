import 'package:equatable/equatable.dart';
import 'package:gio_gift_app/business_logic/models/order_material_detail.dart';

abstract class OrderMaterialState extends Equatable {
  const OrderMaterialState();
  @override
  List<Object> get props => [];
}

class OrderMaterialInitial extends OrderMaterialState {}

class OrderMaterialItemAdded extends OrderMaterialState {
  final OrderMaterialDetail orderMaterialDetail;

  const OrderMaterialItemAdded(this.orderMaterialDetail);

  @override
  List<Object> get props => [orderMaterialDetail];
}

class OrderMaterialItemLoading extends OrderMaterialState {}

class OrderMaterialItemQtyExceeded extends OrderMaterialState {}

class OrderMaterialItemUpdated extends OrderMaterialState {
  final List<OrderMaterialDetail> orderMaterialDetails;

  const OrderMaterialItemUpdated(this.orderMaterialDetails);

  @override
  List<Object> get props => [orderMaterialDetails];
}

class OrderMaterialItemAlreadyExists extends OrderMaterialState {
  final String materialName;

  const OrderMaterialItemAlreadyExists(this.materialName);

  @override
  List<Object> get props => [materialName];
}

class OrderMaterialSaveSuccess extends OrderMaterialState {}

class OrderMaterialSaveLoading extends OrderMaterialState {}

class OrderMaterialSaveError extends OrderMaterialState {
  final String error;

  const OrderMaterialSaveError(this.error);

  @override
  List<Object> get props => [error];
}
