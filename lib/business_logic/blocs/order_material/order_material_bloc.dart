import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/order/order_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/order/order_event.dart';
import 'package:gio_gift_app/business_logic/blocs/order_material/order_material_event.dart';
import 'package:gio_gift_app/business_logic/blocs/order_material/order_material_state.dart';
import 'package:gio_gift_app/business_logic/models/order_material_detail.dart';
import 'package:gio_gift_app/business_logic/models/raw_material.dart';
import 'package:gio_gift_app/business_logic/services/order_material_service.dart';
import 'package:gio_gift_app/business_logic/services/order_service.dart';
import 'package:gio_gift_app/business_logic/models/order.dart';
import 'package:gio_gift_app/business_logic/models/order_material.dart';
import 'package:gio_gift_app/business_logic/services/raw_material_service.dart';
import 'package:gio_gift_app/locator.dart';
import 'package:uuid/uuid.dart';

class OrderMaterialBloc extends Bloc<OrderMaterialEvent, OrderMaterialState> {
  final OrderBloc _orderBloc;
  OrderMaterialService _orderMaterialService;
  OrderService _orderService;
  RawMaterialService _rawMaterialService;
  OrderMaterialBloc(this._orderBloc) : super(OrderMaterialInitial()) {
    _orderMaterialService = sl();
    _orderService = sl();
    _rawMaterialService = sl();
  }

  @override
  Stream<OrderMaterialState> mapEventToState(event) async* {
    if (event is OrderMaterialStartEmpty) {
      yield OrderMaterialItemUpdated(List<OrderMaterialDetail>());
    }

    if (event is OrderMaterialStartEdit) {
      yield* _mapOrderMaterialStartEditEventToState(event);
    }

    if (event is OrderMaterialAddItem) {
      yield* _mapOrderMaterialAddItemEventToState(event);
    }

    if (event is OrderMaterialIncreaseItemQuantity) {
      yield* _mapOrderMaterialIncreaseItemQuantityEventToState(event);
    }

    if (event is OrderMaterialDecreaseItemQuantity) {
      yield* _mapOrderMaterialDecreaseItemQuantityEventToState(event);
    }

    if (event is OrderMaterialUpdateItemQuantity) {
      yield* _mapOrderMaterialUpdateItemQuantityEventToState(event);
    }

    if (event is OrderMaterialSave) {
      yield* _mapOrderMaterialSaveEventToState(event);
    }
  }

  Stream<OrderMaterialState> _mapOrderMaterialStartEditEventToState(
      OrderMaterialStartEdit event) async* {
    yield OrderMaterialItemLoading();
    var orderMaterial =
        await _orderMaterialService.getOrderMaterial(event.orderId);

    yield OrderMaterialItemUpdated(orderMaterial.orderMaterialDetails);
  }

  Stream<OrderMaterialState> _mapOrderMaterialAddItemEventToState(
      OrderMaterialAddItem event) async* {
    if (state is OrderMaterialItemUpdated) {
      final List<OrderMaterialDetail> orderMaterialDetails =
          List.from((state as OrderMaterialItemUpdated).orderMaterialDetails);

      var result = orderMaterialDetails?.firstWhere(
          (o) =>
              o.rawMaterial?.rawMaterialId ==
              event.orderMaterialDetail.rawMaterial.rawMaterialId,
          orElse: () => null);

      if (result != null) {
        yield OrderMaterialItemAlreadyExists(
            event.orderMaterialDetail.rawMaterial.materialName);
      }

      if (result == null) {
        orderMaterialDetails.add(event.orderMaterialDetail);
      }

      yield OrderMaterialItemUpdated(orderMaterialDetails);
    }
  }

  Stream<OrderMaterialState> _mapOrderMaterialIncreaseItemQuantityEventToState(
      OrderMaterialIncreaseItemQuantity event) async* {
    if (state is OrderMaterialItemUpdated) {
      final List<OrderMaterialDetail> orderMaterialDetails =
          List.from((state as OrderMaterialItemUpdated).orderMaterialDetails);

      //check qty
      final RawMaterial rawMaterial =
          await _rawMaterialService.get(event.rawMaterialId);
      if (event.qtyUsed >= rawMaterial.qty) {
        yield OrderMaterialItemQtyExceeded();
        yield OrderMaterialItemUpdated(orderMaterialDetails);
        return;
      }

      final newOrderMaterialDetails = orderMaterialDetails.map((detail) {
        return detail.rawMaterial.rawMaterialId == event.rawMaterialId
            ? detail.copyWith(qtyUsed: detail.qtyUsed + 1)
            : detail;
      }).toList();
      yield OrderMaterialItemUpdated(newOrderMaterialDetails);
    }
  }

  Stream<OrderMaterialState> _mapOrderMaterialDecreaseItemQuantityEventToState(
      OrderMaterialDecreaseItemQuantity event) async* {
    if (state is OrderMaterialItemUpdated) {
      final List<OrderMaterialDetail> orderMaterialDetails =
          (state as OrderMaterialItemUpdated)
              .orderMaterialDetails
              .map((detail) {
                if (detail.rawMaterial.rawMaterialId == event.rawMaterialId) {
                  if (event.qtyUsed > 1) {
                    return detail.copyWith(qtyUsed: detail.qtyUsed - 1);
                  }
                } else {
                  return detail;
                }
              })
              .where((el) => el != null)
              .toList();
      yield OrderMaterialItemUpdated(orderMaterialDetails);
    }
  }

  Stream<OrderMaterialState> _mapOrderMaterialSaveEventToState(
      OrderMaterialSave event) async* {
    if (state is OrderMaterialItemUpdated) {
      final List<OrderMaterialDetail> orderMaterialDetails =
          List.from((state as OrderMaterialItemUpdated).orderMaterialDetails);
      yield OrderMaterialSaveLoading();
      try {
        if (event.isEdit) {
          await _orderMaterialService.deleteOrderMaterial(event.order.orderId);
        }
        _orderMaterialService.addOrderMaterial(OrderMaterial(
            orderMaterialId: Uuid().v4(),
            order: event.order,
            orderMaterialDetails: event.orderMaterialDetail));
        _orderService.updateStatus(
            orderId: event.order.orderId, orderStatus: OrderStatus.READYTOSHIP);
        _orderBloc.add(OrderFetch(
            orderStatus: OrderStatus.ALL.toString().split('.').last));

        yield OrderMaterialSaveSuccess();
      } catch (e) {
        yield OrderMaterialSaveError(e.toString());
        yield OrderMaterialItemUpdated(orderMaterialDetails);
      }
    }
  }

  Stream<OrderMaterialState> _mapOrderMaterialUpdateItemQuantityEventToState(
      OrderMaterialUpdateItemQuantity event) async* {
    if (state is OrderMaterialItemUpdated) {
      final List<OrderMaterialDetail> orderMaterialDetails =
          List.from((state as OrderMaterialItemUpdated).orderMaterialDetails);

      final rawMaterial = await _rawMaterialService.get(event.rawMaterialId);
      if (event.qtyUsed > rawMaterial.qty) {
        yield OrderMaterialItemQtyExceeded();
        yield OrderMaterialItemUpdated(orderMaterialDetails);
        return;
      }

      final newOrderMaterialDetails = orderMaterialDetails.map((detail) {
        return detail.rawMaterial.rawMaterialId == event.rawMaterialId
            ? detail.copyWith(qtyUsed: event.qtyUsed)
            : detail;
      }).toList();
      yield OrderMaterialItemUpdated(newOrderMaterialDetails);
    }
  }
}
