import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:gio_gift_app/business_logic/models/order.dart';
import 'package:gio_gift_app/business_logic/models/order_material_detail.dart';

abstract class OrderMaterialEvent extends Equatable {
  const OrderMaterialEvent();

  @override
  List<Object> get props => [];
}

class OrderMaterialStartEmpty extends OrderMaterialEvent {}

class OrderMaterialStartEdit extends OrderMaterialEvent {
  final String orderId;

  const OrderMaterialStartEdit(this.orderId);

  @override
  List<Object> get props => [orderId];
}

class OrderMaterialSave extends OrderMaterialEvent {
  final bool isEdit;
  final Order order;
  final List<OrderMaterialDetail> orderMaterialDetail;

  const OrderMaterialSave(
      {@required this.order,
      @required this.orderMaterialDetail,
      @required this.isEdit});
}

class OrderMaterialAddItem extends OrderMaterialEvent {
  final OrderMaterialDetail orderMaterialDetail;

  const OrderMaterialAddItem({this.orderMaterialDetail});
  @override
  List<Object> get props => [orderMaterialDetail];
}

class OrderMaterialIncreaseItemQuantity extends OrderMaterialEvent {
  final String rawMaterialId;
  final double qtyUsed;

  const OrderMaterialIncreaseItemQuantity(this.rawMaterialId, this.qtyUsed);

  @override
  List<Object> get props => [rawMaterialId, qtyUsed];
}

class OrderMaterialDecreaseItemQuantity extends OrderMaterialEvent {
  final String rawMaterialId;
  final double qtyUsed;

  const OrderMaterialDecreaseItemQuantity(this.rawMaterialId, this.qtyUsed);

  @override
  List<Object> get props => [rawMaterialId, qtyUsed];
}

class OrderMaterialUpdateItemQuantity extends OrderMaterialEvent {
  final String rawMaterialId;
  final double qtyUsed;

  const OrderMaterialUpdateItemQuantity(this.rawMaterialId, this.qtyUsed);

  @override
  List<Object> get props => [rawMaterialId, qtyUsed];
}
