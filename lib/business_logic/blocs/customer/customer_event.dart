import 'package:equatable/equatable.dart';
import 'package:gio_gift_app/business_logic/models/customer.dart';

abstract class CustomerEvent extends Equatable {
  const CustomerEvent();

  @override
  List<Object> get props => [];
}

class CustomerFetch extends CustomerEvent {}

class CustomerListUpdated extends CustomerEvent {
  final List<Customer> customerList;

  const CustomerListUpdated(this.customerList);

  @override
  List<Object> get props => [customerList];
}
