import 'package:equatable/equatable.dart';
import 'package:gio_gift_app/business_logic/models/customer.dart';

abstract class CustomerState extends Equatable {
  const CustomerState();
}

class CustomerFetchLoading extends CustomerState {
  @override
  List<Object> get props => [];
}

class CustomerFetchEmpty extends CustomerState {
  @override
  List<Object> get props => [];
}

class CustomerFetchSuccess extends CustomerState {
  final List<Customer> customerList;

  const CustomerFetchSuccess(this.customerList);

  @override
  List<Object> get props => [customerList];
}

class CustomerFetchError extends CustomerState {
  final String error;

  const CustomerFetchError(this.error);

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'Error Fetching Customer {$error}';
}
