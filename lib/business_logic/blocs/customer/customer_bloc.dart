import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/customer/customer_event.dart';
import 'package:gio_gift_app/business_logic/blocs/customer/customer_state.dart';
import 'package:gio_gift_app/business_logic/services/customer_service.dart';

class CustomerBloc extends Bloc<CustomerEvent, CustomerState> {
  final CustomerService _customerService;

  CustomerBloc(this._customerService)
      : assert(_customerService != null),
        super(CustomerFetchLoading());

  @override
  Stream<CustomerState> mapEventToState(event) async* {
    if (event is CustomerFetch) {
      yield* _mapCustomerFetchEventToState();
    }

    if (event is CustomerListUpdated) {
      yield CustomerFetchSuccess(event.customerList);
    }
  }

  Stream<CustomerState> _mapCustomerFetchEventToState() async* {
    yield CustomerFetchLoading();
    try {
      var customers = await _customerService.getCustomers();
      add(CustomerListUpdated(customers));
    } catch (e) {
      yield CustomerFetchError(e.toString());
    }
  }
}
