import 'package:equatable/equatable.dart';
import 'package:gio_gift_app/business_logic/models/order.dart';

abstract class OrderState extends Equatable {
  const OrderState();
}

class OrderFetchLoading extends OrderState {
  @override
  List<Object> get props => [];
}

class OrderFetchEmpty extends OrderState {
  @override
  List<Object> get props => [];
}

class OrderFetchSuccess extends OrderState {
  final List<Order> orderList;

  const OrderFetchSuccess(this.orderList);

  @override
  List<Object> get props => [orderList];
}

class OrderFetchError extends OrderState {
  final String error;

  const OrderFetchError(this.error);

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'Error Fetching Order {$error}';
}
