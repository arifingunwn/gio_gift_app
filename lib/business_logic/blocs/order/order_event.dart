import 'package:equatable/equatable.dart';
import 'package:gio_gift_app/business_logic/models/order.dart';

abstract class OrderEvent extends Equatable {
  const OrderEvent();

  @override
  List<Object> get props => [];
}

class OrderFetch extends OrderEvent {
  final String orderStatus;

  const OrderFetch({this.orderStatus});

  @override
  List<Object> get props => [orderStatus];
}

class OrderListUpdated extends OrderEvent {
  final List<Order> orderList;

  const OrderListUpdated(this.orderList);

  @override
  List<Object> get props => [orderList];
}

class OrderSave extends OrderEvent {
  final Order order;

  const OrderSave(this.order);

  @override
  List<Object> get props => [order];
}

class OrderCheckPhoneNumber extends OrderEvent {
  final String phoneNumber;

  const OrderCheckPhoneNumber(this.phoneNumber);

  @override
  List<Object> get props => [phoneNumber];
}
