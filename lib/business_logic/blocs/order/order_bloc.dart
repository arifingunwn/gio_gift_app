import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/order/order_event.dart';
import 'package:gio_gift_app/business_logic/blocs/order/order_state.dart';
import 'package:gio_gift_app/business_logic/services/order_service.dart';

class OrderBloc extends Bloc<OrderEvent, OrderState> {
  final OrderService _orderService;

  OrderBloc(this._orderService)
      : assert(_orderService != null),
        super(OrderFetchLoading());

  @override
  Stream<OrderState> mapEventToState(OrderEvent event) async* {
    if (event is OrderFetch) {
      yield* _mapOrderFetchEventToState(event);
    }

    if (event is OrderListUpdated) {
      yield OrderFetchSuccess(event.orderList);
    }
  }

  Stream<OrderState> _mapOrderFetchEventToState(OrderFetch event) async* {
    yield OrderFetchLoading();
    try {
      var order = await _orderService.getOrders(event.orderStatus);
      add(OrderListUpdated(order));
    } catch (e) {
      yield OrderFetchError(e.toString());
    }
  }
}
