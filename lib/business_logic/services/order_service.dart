import 'package:flutter/material.dart';
import 'package:gio_gift_app/business_logic/models/order.dart';
import 'package:gio_gift_app/business_logic/models/transaction.dart';
import 'package:gio_gift_app/business_logic/repositories/order_repository.dart';
import 'package:gio_gift_app/business_logic/services/transaction_service.dart';
import 'package:gio_gift_app/locator.dart';

abstract class OrderService {
  Future<Order> get(String orderId);
  Future<List<Order>> getOrders(String orderStatus);
  Future<bool> addOrder(
      {@required String orderId,
      @required String orderNo,
      @required String phoneNumber,
      @required String customerName,
      @required String receiverName,
      @required String address,
      @required String city,
      @required String orderName,
      @required DateTime dueDate,
      @required String wishCardDescription,
      @required String orderDescription,
      @required double price,
      @required OrderStatus orderStatus});
  Future<bool> updateOrder(
      {@required String orderId,
      @required String phoneNumber,
      @required String customerName,
      @required String receiverName,
      @required String address,
      @required String city,
      @required String orderName,
      @required DateTime dueDate,
      @required String wishCardDescription,
      @required String orderDescription,
      @required double price});
  Future<bool> proceedOrder(String orderId);
  Future<bool> updateStatus({String orderId, OrderStatus orderStatus});
  Future<bool> updateShipping(
      {@required String orderId,
      @required String courierName,
      @required double shippingCost});
}

class OrderServiceImpl implements OrderService {
  Future<Order> get(String orderId) async {
    OrderRepository orderRepository = sl();
    var order = await orderRepository.get(orderId);
    return order;
  }

  Future<List<Order>> getOrders(String orderStatus) {
    OrderRepository _orderRepository = sl();
    if (orderStatus == "ALL") {
      return _orderRepository.getOrders();
    }

    return _orderRepository.getOrdersByStatus(orderStatus);
  }

  Future<bool> addOrder(
      {@required String orderId,
      @required String orderNo,
      @required String phoneNumber,
      @required String customerName,
      @required String receiverName,
      @required String address,
      @required String city,
      @required String orderName,
      @required DateTime dueDate,
      @required String wishCardDescription,
      @required String orderDescription,
      @required double price,
      @required OrderStatus orderStatus}) async {
    OrderRepository _orderRepository = sl();
    DateTime now = DateTime.now();

    final order = Order(
      orderId: orderId,
      orderNo: orderNo,
      phoneNumber: phoneNumber,
      customerName: customerName,
      receiverName: receiverName,
      address: address,
      city: city,
      orderName: orderName,
      dueDate: dueDate,
      wishCardDescription: wishCardDescription,
      orderDescription: orderDescription,
      price: price,
      orderStatus: orderStatus.toString().split('.').last,
      createdAt: now,
    );

    return await _orderRepository.insert(order);
  }

  Future<bool> updateStatus({String orderId, OrderStatus orderStatus}) async {
    OrderRepository _orderRepository = sl();

    Order order = await _orderRepository.get(orderId);

    return await _orderRepository.update(
        order.copyWith(orderStatus: orderStatus.toString().split('.').last));
  }

  Future<bool> proceedOrder(String orderId) async {
    OrderRepository orderRepository = sl();
    TransactionService transactionService = sl();

    final order = await orderRepository.get(orderId);
    bool result = await orderRepository.update(order.copyWith(
        orderStatus: OrderStatus.INPROGRESS.toString().split('.').last));
    if (result) {
      String transactionDescription =
          "Proceeding Order ${order.orderNo} - ${order.orderName}";
      return await transactionService.debit(
          DateTime.now(),
          TransactionType.PROCEED_ORDER,
          order.orderId,
          order.orderNo,
          transactionDescription,
          order.price);
    } else {
      return false;
    }
  }

  Future<bool> updateShipping(
      {@required String orderId,
      @required String courierName,
      @required double shippingCost}) async {
    OrderRepository _orderRepository = sl();
    TransactionService _transactionService = sl();
    Order order = await _orderRepository.get(orderId);

    bool result = await _orderRepository.update(order.copyWith(
        courierName: courierName,
        shippingCost: shippingCost,
        orderStatus: OrderStatus.COMPLETE.toString().split('.').last));
    if (result) {
      String description = "Shipping Cost of Order ${order.orderNo}";
      return await _transactionService.credit(
          DateTime.now(),
          TransactionType.SHIP_ORDER,
          order.orderId,
          order.orderNo,
          description,
          shippingCost);
    } else {
      return false;
    }
  }

  @override
  Future<bool> updateOrder(
      {@required String orderId,
      @required String phoneNumber,
      @required String customerName,
      @required String receiverName,
      @required String address,
      @required String city,
      @required String orderName,
      @required DateTime dueDate,
      @required String wishCardDescription,
      @required String orderDescription,
      @required double price}) async {
    OrderRepository orderRepository = sl();
    var order = await orderRepository.get(orderId);
    final newOrder = order.copyWith(
      phoneNumber: phoneNumber,
      customerName: customerName,
      receiverName: receiverName,
      address: address,
      city: city,
      orderName: orderName,
      dueDate: dueDate,
      wishCardDescription: wishCardDescription,
      orderDescription: orderDescription,
      price: price,
    );

    DateTime now = DateTime.now();
    if (order.orderStatus != OrderStatus.PENDING.toString().split('.').last) {
      //revert transaction
      TransactionService transactionService = sl();
      String transDesc = "Update Order ${order.orderNo} reverse balance";

      await transactionService.credit(now, TransactionType.UPDATE_ORDER,
          order.orderId, order.orderNo, transDesc, order.price);

      //add new balance
      String transDescNew = "Update Order ${order.orderNo} new balance";
      await transactionService.debit(now, TransactionType.UPDATE_ORDER,
          order.orderId, order.orderNo, transDescNew, newOrder.price);
    }

    return await orderRepository.update(newOrder);
  }
}
