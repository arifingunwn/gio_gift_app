import 'package:gio_gift_app/business_logic/models/generated_number.dart';
import 'package:gio_gift_app/business_logic/repositories/generated_number_repository.dart';
import 'package:gio_gift_app/locator.dart';
import 'package:uuid/uuid.dart';

abstract class GeneratedNumberService {
  Future<String> getGeneratedNumber(
      String generatedNumberName, int month, int year);
}

class GeneratedNumberServiceImpl extends GeneratedNumberService {
  GeneratedNumberRepository _generatedNumberRepository = sl();
  Future<String> getGeneratedNumber(
      String generatedNumberName, int month, int year) async {
    String prefix = "";
    if (generatedNumberName == "ORDER") {
      prefix = "ORD";
    }
    if (generatedNumberName == "PURCHASE") {
      prefix = "PUR";
    }

    var lastGenerated = await _generatedNumberRepository.getGeneratedNumber(
        generatedNumberName, month, year);

    if (lastGenerated == null) {
      lastGenerated = GeneratedNumber(
          generatedNumberId: Uuid().v4(),
          generatedNumberName: generatedNumberName,
          lastGeneratedNumber: 0,
          month: month,
          year: year);

      await _generatedNumberRepository.createGeneratedNumber(lastGenerated);
    }

    int generatedNumber = lastGenerated.lastGeneratedNumber + 1;

    await _generatedNumberRepository.updateGeneratedNumber(
        lastGenerated.copyWith(lastGeneratedNumber: generatedNumber));

    String paddedGeneratedNumber = generatedNumber.toString().padLeft(4, '0');
    String paddedMonth = lastGenerated.month.toString().padLeft(2, '0');
    return "$prefix-${lastGenerated.year}-$paddedMonth-$paddedGeneratedNumber";
  }
}
