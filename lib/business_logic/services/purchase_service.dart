import 'package:gio_gift_app/business_logic/models/purchase.dart';
import 'package:gio_gift_app/business_logic/models/purchase_detail.dart';
import 'package:gio_gift_app/business_logic/models/transaction.dart';
import 'package:gio_gift_app/business_logic/repositories/purchase_detail_repository.dart';
import 'package:gio_gift_app/business_logic/repositories/purchase_repository.dart';
import 'package:gio_gift_app/business_logic/repositories/raw_material_repository.dart';
import 'package:gio_gift_app/business_logic/services/generated_number_service.dart';
import 'package:gio_gift_app/business_logic/services/transaction_service.dart';
import 'package:gio_gift_app/locator.dart';
import 'package:uuid/uuid.dart';

abstract class PurchaseService {
  Future<Purchase> get(String purchaseId);
  Future<bool> addPurchase(String supplierName, DateTime purchaseDate,
      String description, List<PurchaseDetail> purchaseDetails);
  Future<bool> updatePurchase(
      String purchaseId,
      String supplierName,
      DateTime purchaseDate,
      String description,
      List<PurchaseDetail> purchaseDetails);
  Future<List<Purchase>> getPurchases();
}

class PurchaseServiceImpl implements PurchaseService {
  Future<Purchase> get(String purchaseId) async {
    PurchaseRepository _purchaseRepository = sl();
    return await _purchaseRepository.get(purchaseId);
  }

  Future<bool> addPurchase(String supplierName, DateTime purchaseDate,
      String description, List<PurchaseDetail> purchaseDetails) async {
    PurchaseRepository _purchaseRepository = sl();
    PurchaseDetailRepository _purchaseDetailRepository = sl();
    RawMaterialRepository _rawMaterialRepository = sl();
    GeneratedNumberService _generatedNumberService = sl();
    TransactionService _transactionService = sl();

    final String purchaseId = Uuid().v4();
    final DateTime now = DateTime.now();
    final double totalAmount =
        purchaseDetails.fold(0, (prev, curr) => prev + curr.totalPrice);

    final String purchaseNo = await _generatedNumberService.getGeneratedNumber(
        "PURCHASE", now.month, now.year);
    final purchase = Purchase(
        purchaseId: purchaseId,
        purchaseNo: purchaseNo,
        supplierName: supplierName,
        purchaseDate: purchaseDate,
        description: description,
        totalAmount: totalAmount,
        createdAt: now);

    bool result = await _purchaseRepository.insert(purchase);
    if (!result) throw Exception("Failed to insert purchase");

    purchaseDetails.forEach((detail) async {
      final purchaseDetail = PurchaseDetail(
          purchaseDetailId: Uuid().v4(),
          purchaseId: purchaseId,
          qty: detail.qty,
          price: detail.price,
          rawMaterial: detail.rawMaterial);
      result = await _purchaseDetailRepository.insert(purchaseDetail);
      if (!result) throw Exception("Failed to insert purchaseDetail");

      final rawMaterial =
          await _rawMaterialRepository.get(detail.rawMaterial.rawMaterialId);
      result = await _rawMaterialRepository
          .update(rawMaterial.copyWith(qty: rawMaterial.qty + detail.qty));
      if (!result) throw Exception("Failed to increase material qty");
    });

    if (result) {
      String description = "Payment of Purchase ${purchase.purchaseNo}";
      return await _transactionService.credit(now, TransactionType.ADD_PURCHASE,
          purchase.purchaseId, purchase.purchaseNo, description, totalAmount);
    }

    return false;
  }

  Future<bool> updatePurchase(
      String purchaseId,
      String supplierName,
      DateTime purchaseDate,
      String description,
      List<PurchaseDetail> purchaseDetails) async {
    PurchaseRepository purchaseRepository = sl();
    PurchaseDetailRepository purchaseDetailRepository = sl();
    RawMaterialRepository rawMaterialRepository = sl();
    TransactionService transactionService = sl();

    final oldPurchase = await purchaseRepository.get(purchaseId);
    if (oldPurchase == null) throw Exception("Purchase Invalid.");

    final newTotalAmount =
        purchaseDetails.fold(0, (prev, curr) => prev + curr.totalPrice);
    final newPurchase = oldPurchase.copyWith(
      supplierName: supplierName,
      purchaseDate: purchaseDate,
      description: description,
      totalAmount: newTotalAmount,
    );

    var result = await purchaseRepository.update(newPurchase);
    if (!result) throw Exception("Failed Update Purchase.");

    final List<PurchaseDetail> oldPurchaseDetails =
        await purchaseDetailRepository.getPurchaseDetails(purchaseId);
    if (oldPurchaseDetails == null) throw Exception("Invalid Purchase.");

    oldPurchaseDetails.forEach((detail) async {
      //Revert RawMaterial Qty
      final rawMaterial =
          await rawMaterialRepository.get(detail.rawMaterial.rawMaterialId);
      result = await rawMaterialRepository
          .update(rawMaterial.copyWith(qty: rawMaterial.qty - detail.qty));
      if (!result) throw Exception("Failed to Update RawMaterial Qty.");

      //Delete Old PurchaseDetail
      result = await purchaseDetailRepository.delete(detail.purchaseDetailId);
      if (!result) throw Exception("Failed Delete Old Purchase Detail");
    });

    purchaseDetails.forEach((detail) async {
      //Insert New Purchase Detail
      result = await purchaseDetailRepository
          .insert(detail.copyWith(purchaseDetailId: Uuid().v4()));
      if (!result) throw Exception("Failed to insert new PurchaseDetail.");

      //Update RawMaterial with new Qty
      final rawMaterial =
          await rawMaterialRepository.get(detail.rawMaterial.rawMaterialId);
      result = await rawMaterialRepository
          .update(rawMaterial.copyWith(qty: rawMaterial.qty + detail.qty));
      if (!result) throw Exception("Failed to Update new RawMaterial Qty.");
    });

    //Revert Old transaction
    final String revertTransDesc =
        "Revert Purchase TotalAmount for Purchase Update of ${newPurchase.purchaseNo}";
    result = await transactionService.debit(
        DateTime.now(),
        TransactionType.UPDATE_PURCHASE,
        purchaseId,
        newPurchase.purchaseNo,
        revertTransDesc,
        oldPurchase.totalAmount);
    if (!result) throw Exception("Failed to revert transaction.");

    final String newTransDesc =
        "Insert new Purchase Total Amount for ${newPurchase.purchaseNo}";
    result = await transactionService.credit(
        DateTime.now(),
        TransactionType.UPDATE_PURCHASE,
        purchaseId,
        oldPurchase.purchaseNo,
        newTransDesc,
        newTotalAmount);

    if (!result) throw Exception("Failed to insert new Transaction");

    return true;
  }

  Future<List<Purchase>> getPurchases() {
    PurchaseRepository _purchaseRepository = sl();
    return _purchaseRepository.getPurchases();
  }
}
