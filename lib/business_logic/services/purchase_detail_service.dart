import 'package:gio_gift_app/business_logic/models/purchase_detail.dart';
import 'package:gio_gift_app/business_logic/models/raw_material.dart';
import 'package:gio_gift_app/business_logic/repositories/purchase_detail_repository.dart';
import 'package:gio_gift_app/business_logic/repositories/raw_material_repository.dart';
import 'package:gio_gift_app/locator.dart';

abstract class PurchaseDetailService {
  Future<void> addPurchaseDetail(PurchaseDetail purchaseDetail);
  Future<List<PurchaseDetail>> getPurchaseDetails(String purchaseId);
}

class PurchaseDetailServiceImpl extends PurchaseDetailService {
  Future<void> addPurchaseDetail(PurchaseDetail purchaseDetail) async {
    PurchaseDetailRepository _purchaseDetailRepository = sl();
    RawMaterialRepository _rawMaterialRepository = sl();

    await _purchaseDetailRepository.insert(purchaseDetail);
    RawMaterial rawMaterial = await _rawMaterialRepository
        .get(purchaseDetail.rawMaterial.rawMaterialId);

    //Calculate Cost of Goods Sold in Weighted Average Method
    double cogs = rawMaterial.cogs;
    if (cogs != 0) {
      double lastTotalPrice = rawMaterial.qty * rawMaterial.cogs;
      double incomingTotalPrice = purchaseDetail.qty * purchaseDetail.price;
      cogs = (lastTotalPrice + incomingTotalPrice) /
          (rawMaterial.qty + purchaseDetail.qty);
    } else {
      cogs = purchaseDetail.price;
    }

    await _rawMaterialRepository.update(rawMaterial.copyWith(
        qty: rawMaterial.qty + purchaseDetail.qty, cogs: cogs));
  }

  Future<List<PurchaseDetail>> getPurchaseDetails(String purchaseId) async {
    PurchaseDetailRepository _purchaseDetailRepository = sl();
    RawMaterialRepository _rawMaterialRepository = sl();

    List<PurchaseDetail> purchaseDetails =
        await _purchaseDetailRepository.getPurchaseDetails(purchaseId);

    return Future.wait(purchaseDetails.map((pd) async {
      RawMaterial rawMaterial =
          await _rawMaterialRepository.get(pd.rawMaterial.rawMaterialId);
      return pd.copyWith(rawMaterial: rawMaterial);
    }));
  }
}
