import 'package:gio_gift_app/business_logic/models/order_material.dart';
import 'package:gio_gift_app/business_logic/models/order_material_detail.dart';
import 'package:gio_gift_app/business_logic/models/raw_material.dart';
import 'package:gio_gift_app/business_logic/repositories/order_material_detail_repository.dart';
import 'package:gio_gift_app/business_logic/repositories/order_material_repository.dart';
import 'package:gio_gift_app/business_logic/repositories/raw_material_repository.dart';
import 'package:gio_gift_app/locator.dart';
import 'package:uuid/uuid.dart';

abstract class OrderMaterialService {
  Future<OrderMaterial> getOrderMaterial(String orderId);
  Future<void> deleteOrderMaterial(String orderId);
  void addOrderMaterial(OrderMaterial orderMaterial);
}

class OrderMaterialServiceImpl implements OrderMaterialService {
  Future<OrderMaterial> getOrderMaterial(String orderId) async {
    OrderMaterialRepository _orderMaterialRepository = sl();
    OrderMaterialDetailRepository _orderMaterialDetailRepository = sl();

    var orderMaterial = await _orderMaterialRepository.get(orderId);

    if (orderMaterial != null) {
      final orderMaterialDetails = await _orderMaterialDetailRepository
          .getOrderMaterialDetails(orderMaterial.orderMaterialId);

      return orderMaterial.copyWith(orderMaterialDetails: orderMaterialDetails);
    }

    return orderMaterial;
  }

  Future<void> deleteOrderMaterial(String orderId) async {
    OrderMaterialRepository _orderMaterialRepository = sl();
    OrderMaterialDetailRepository _orderMaterialDetailRepository = sl();
    RawMaterialRepository _rawMaterialRepository = sl();

    final OrderMaterial orderMaterial =
        await _orderMaterialRepository.get(orderId);
    final List<OrderMaterialDetail> orderMaterialDetails =
        await _orderMaterialDetailRepository
            .getOrderMaterialDetails(orderMaterial.orderMaterialId);

    orderMaterialDetails.forEach((detail) async {
      final RawMaterial rawMaterial =
          await _rawMaterialRepository.get(detail.rawMaterial.rawMaterialId);
      await _rawMaterialRepository
          .update(rawMaterial.copyWith(qty: rawMaterial.qty + detail.qtyUsed));
    });

    await _orderMaterialRepository.delete(orderMaterial.orderMaterialId);
    await _orderMaterialDetailRepository
        .deleteByOrderMaterialId(orderMaterial.orderMaterialId);
  }

  void addOrderMaterial(OrderMaterial orderMaterial) async {
    OrderMaterialRepository _orderMaterialRepository = sl();
    OrderMaterialDetailRepository _orderMaterialDetailRepository = sl();
    RawMaterialRepository _rawMaterialRepository = sl();

    _orderMaterialRepository.insert(orderMaterial);

    orderMaterial.orderMaterialDetails.forEach((detail) async {
      final RawMaterial rawMaterial =
          await _rawMaterialRepository.get(detail.rawMaterial.rawMaterialId);

      await _orderMaterialDetailRepository.insert(detail.copyWith(
          orderMaterialDetailId: Uuid().v4(),
          orderMaterialId: orderMaterial.orderMaterialId,
          cogs: rawMaterial.cogs));
      await _rawMaterialRepository
          .update(rawMaterial.copyWith(qty: rawMaterial.qty - detail.qtyUsed));
    });
  }
}
