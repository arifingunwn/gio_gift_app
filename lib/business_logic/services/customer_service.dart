import 'package:gio_gift_app/business_logic/models/customer.dart';
import 'package:gio_gift_app/business_logic/repositories/customer_repository.dart';
import 'package:gio_gift_app/locator.dart';

abstract class CustomerService {
  void addCustomer(Customer customer);
  Future<List<Customer>> getCustomers();
  Future<Customer> getCustomerByPhoneNumber(String phoneNumber);
}

class CustomerServiceImpl implements CustomerService {
  void addCustomer(Customer customer) async {
    CustomerRepository _customerRepository = sl();
    customer.createdAt = DateTime.now();
    _customerRepository.insert(customer);
  }

  Future<List<Customer>> getCustomers() {
    CustomerRepository _customerRepository = sl();
    return _customerRepository.getCustomers();
  }

  Future<Customer> getCustomerByPhoneNumber(String phoneNumber) async {
    CustomerRepository _customerRepository = sl();
    return await _customerRepository.getCustomerByPhoneNumber(phoneNumber);
  }
}
