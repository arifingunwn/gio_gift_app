import 'package:gio_gift_app/business_logic/models/dashboard_data.dart';
import 'package:gio_gift_app/business_logic/repositories/dashboard_repository.dart';
import 'package:gio_gift_app/locator.dart';

abstract class DashboardService {
  Future<DashboardData> getDashboardData();
}

class DashboardServiceImpl implements DashboardService {
  @override
  Future<DashboardData> getDashboardData() async {
    DashboardRepository repo = sl();

    return await repo.getDashboardData();
  }
}
