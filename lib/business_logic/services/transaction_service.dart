import 'package:gio_gift_app/business_logic/models/transaction.dart';
import 'package:gio_gift_app/business_logic/repositories/transaction_repository.dart';
import 'package:gio_gift_app/locator.dart';
import 'package:uuid/uuid.dart';

abstract class TransactionService {
  Future<List<Transaction>> getTransations(
      DateTime startDate, DateTime endDate, int offset, int limit);
  Future<bool> debit(
      DateTime transactionDate,
      TransactionType transactionType,
      String referenceId,
      String referenceNo,
      String description,
      double amount);
  Future<bool> credit(
      DateTime transactionDate,
      TransactionType transactionType,
      String referenceId,
      String referenceNo,
      String description,
      double amount);
}

class TransactionServiceImpl implements TransactionService {
  TransactionRepository transactionRepository = sl();

  @override
  Future<List<Transaction>> getTransations(
      DateTime startDate, DateTime endDate, int offset, int limit) async {
    return await transactionRepository.getTransactions(
        startDate, endDate, offset, limit);
  }

  @override
  Future<bool> credit(
      DateTime transactionDate,
      TransactionType transactionType,
      String referenceId,
      String referenceNo,
      String description,
      double amount) async {
    double lastEndingBalance = 0;
    var lastTrans = await transactionRepository.getLastTransaction();
    if (lastTrans != null) {
      lastEndingBalance = lastTrans.balance;
    }

    final newTrans = Transaction(
        transactionId: Uuid().v4(),
        transactionDate: transactionDate,
        transactionType: transactionType,
        referenceId: referenceId,
        referenceNo: referenceNo,
        description: description,
        credit: amount,
        debit: 0,
        balance: lastEndingBalance - amount,
        createdAt: DateTime.now());
    return await transactionRepository.insert(newTrans);
  }

  @override
  Future<bool> debit(
      DateTime transactionDate,
      TransactionType transactionType,
      String referenceId,
      String referenceNo,
      String description,
      double amount) async {
    double lastEndingBalance = 0;
    var lastTrans = await transactionRepository.getLastTransaction();
    if (lastTrans != null) {
      lastEndingBalance = lastTrans.balance;
    }

    final newTrans = Transaction(
        transactionId: Uuid().v4(),
        transactionDate: transactionDate,
        transactionType: transactionType,
        referenceId: referenceId,
        referenceNo: referenceNo,
        description: description,
        credit: 0,
        debit: amount,
        balance: lastEndingBalance + amount,
        createdAt: DateTime.now());
    return await transactionRepository.insert(newTrans);
  }
}
