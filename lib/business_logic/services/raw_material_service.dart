import 'package:gio_gift_app/business_logic/models/raw_material.dart';
import 'package:gio_gift_app/business_logic/repositories/raw_material_repository.dart';
import 'package:gio_gift_app/locator.dart';

abstract class RawMaterialService {
  Future<RawMaterial> get(String rawMaterialId);
  void addRawMaterial(RawMaterial rawMaterial);
  Future<List<RawMaterial>> getAll();
  Future<List<RawMaterial>> getAvailableRawMaterials(String filterString);
}

class RawMaterialServiceImpl implements RawMaterialService {
  Future<RawMaterial> get(String rawMaterialId) async {
    RawMaterialRepository _rawMaterialRepository = sl();
    return await _rawMaterialRepository.get(rawMaterialId);
  }

  void addRawMaterial(RawMaterial rawMaterial) async {
    RawMaterialRepository _rawMaterialRepository = sl();
    _rawMaterialRepository.insert(rawMaterial);
  }

  Future<List<RawMaterial>> getAll() async {
    RawMaterialRepository _rawMaterialRepository = sl();
    return await _rawMaterialRepository.getRawMaterials();
  }

  Future<List<RawMaterial>> getAvailableRawMaterials(
      String filterString) async {
    RawMaterialRepository _rawMaterialRepository = sl();
    var result = await _rawMaterialRepository.getAvailableRawMaterials();
    if (result != null) {
      return result
          .map((rawMaterial) {
            if (filterString.isEmpty ||
                rawMaterial.materialName
                    .toLowerCase()
                    .contains(filterString.toLowerCase())) {
              return rawMaterial;
            }
          })
          .where((x) => x != null)
          .toList();
    }
  }
}
