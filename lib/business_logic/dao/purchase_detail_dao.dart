import 'package:gio_gift_app/business_logic/models/purchase_detail.dart';
import 'package:gio_gift_app/business_logic/models/raw_material.dart';
import 'dao.dart';

class PurchaseDetailDao implements Dao<PurchaseDetail> {
  final String tableName = "purchaseDetail";

  @override
  String get createTableQuery =>
      "CREATE TABLE $tableName(purchaseDetailId TEXT PRIMARY KEY NOT NULL, purchaseId TEXT NOT NULL, rawMaterialId TEXT NOT NULL, qty REAL NOT NULL, price REAL NOT NULL)";

  @override
  List<PurchaseDetail> fromList(List<Map<String, dynamic>> query) {
    return query.map((order) {
      return fromMap(order);
    }).toList();
  }

  @override
  fromMap(Map<String, dynamic> query) {
    return PurchaseDetail(
        purchaseDetailId: query["purchaseDetailId"],
        purchaseId: query["purchaseId"],
        rawMaterial: RawMaterial(rawMaterialId: query["rawMaterialId"]),
        qty: query["qty"],
        price: query["price"]);
  }

  @override
  Map<String, dynamic> toMap(object) {
    return <String, dynamic>{
      "purchaseDetailId": object.purchaseDetailId,
      "purchaseId": object.purchaseId,
      "rawMaterialId": object.rawMaterial.rawMaterialId,
      "qty": object.qty,
      "price": object.price,
    };
  }
}
