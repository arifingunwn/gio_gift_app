import 'package:gio_gift_app/business_logic/models/order.dart';
import 'package:gio_gift_app/business_logic/models/order_material.dart';
import 'dao.dart';

class OrderMaterialDao implements Dao<OrderMaterial> {
  final String tableName = "orderMaterial";

  @override
  String get createTableQuery =>
      "CREATE TABLE $tableName(orderMaterialId TEXT PRIMARY KEY NOT NULL, orderId TEXT NOT NULL, createdAt TEXT)";

  @override
  List<OrderMaterial> fromList(List<Map<String, dynamic>> query) {
    return query.map((order) {
      return fromMap(order);
    }).toList();
  }

  @override
  fromMap(Map<String, dynamic> query) {
    return OrderMaterial(
        orderMaterialId: query["orderMaterialId"],
        order: Order(orderId: query["orderId"]),
        createdAt: DateTime.parse(query["createdAt"]));
  }

  @override
  Map<String, dynamic> toMap(object) {
    return <String, dynamic>{
      "orderMaterialId": object.orderMaterialId,
      "orderId": object.order.orderId,
      "createdAt": object.order.createdAt.toString()
    };
  }
}
