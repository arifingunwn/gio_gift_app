import 'package:gio_gift_app/business_logic/models/raw_material.dart';
import 'dao.dart';

class RawMaterialDao implements Dao<RawMaterial> {
  final String tableName = "rawMaterial";

  @override
  String get createTableQuery =>
      "CREATE TABLE $tableName(rawMaterialId TEXT PRIMARY KEY NOT NULL, materialName TEXT NOT NULL, qty REAL NOT NULL, cogs REAL NOT NULL)";

  @override
  List<RawMaterial> fromList(List<Map<String, dynamic>> query) {
    return query.map((order) {
      return fromMap(order);
    }).toList();
  }

  @override
  fromMap(Map<String, dynamic> query) {
    return RawMaterial(
      rawMaterialId: query["rawMaterialId"],
      materialName: query["materialName"],
      qty: query["qty"],
      cogs: query["cogs"],
    );
  }

  @override
  Map<String, dynamic> toMap(object) {
    return <String, dynamic>{
      "rawMaterialId": object.rawMaterialId,
      "materialName": object.materialName,
      "qty": object.qty,
      "cogs": object.cogs,
    };
  }
}
