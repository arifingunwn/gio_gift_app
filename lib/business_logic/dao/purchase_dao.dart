import 'package:gio_gift_app/business_logic/models/purchase.dart';
import 'dao.dart';

class PurchaseDao implements Dao<Purchase> {
  final String tableName = "purchase";

  @override
  String get createTableQuery =>
      "CREATE TABLE $tableName(purchaseId TEXT PRIMARY KEY NOT NULL, purchaseNo TEXT NOT NULL, supplierName TEXT NOT NULL, totalAmount NUMERIC NOT NULL, purchaseDate TEXT NOT NULL, description TEXT, createdAt TEXT)";

  @override
  List<Purchase> fromList(List<Map<String, dynamic>> query) {
    return query.map((order) {
      return fromMap(order);
    }).toList();
  }

  @override
  fromMap(Map<String, dynamic> query) {
    return Purchase(
        purchaseId: query["purchaseId"],
        purchaseNo: query["purchaseNo"],
        supplierName: query["supplierName"],
        totalAmount: double.parse(query["totalAmount"].toString()),
        purchaseDate: DateTime.parse(query["purchaseDate"].toString()),
        description: query["description"],
        createdAt: DateTime.parse(query["createdAt"].toString()));
  }

  @override
  Map<String, dynamic> toMap(object) {
    return <String, dynamic>{
      "purchaseId": object.purchaseId,
      "purchaseNo": object.purchaseNo,
      "supplierName": object.supplierName,
      "totalAmount": object.totalAmount,
      "purchaseDate": object.purchaseDate.toString(),
      "description": object.description,
      "createdAt": object.createdAt.toString(),
    };
  }
}
