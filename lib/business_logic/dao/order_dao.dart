import 'package:gio_gift_app/business_logic/models/order.dart';
import 'dao.dart';

class OrderDao implements Dao<Order> {
  final String tableName = "order";

  @override
  // TODO: implement createTableQuery
  String get createTableQuery =>
      "CREATE TABLE [$tableName](orderId TEXT PRIMARY KEY NOT NULL, orderNo TEXT NOT NULL, orderName TEXT NOT NULL, customerName TEXT NOT NULL, receiverName TEXT NOT NULL, address TEXT, city TEXT, phoneNumber TEXT NOT NULL, dueDate TEXT NOT NULL, courierName TEXT, shippingCost NUMERIC, wishCardDescription TEXT, orderDescription TEXT, price NUMERIC NOT NULL, orderStatus TEXT NOT NULL, createdAt TEXT)";

  @override
  List<Order> fromList(List<Map<String, dynamic>> query) {
    return query.map((order) {
      return fromMap(order);
    }).toList();
  }

  @override
  fromMap(Map<String, dynamic> query) {
    return Order(
      orderId: query["orderId"],
      orderName: query["orderName"],
      orderNo: query["orderNo"],
      customerName: query["customerName"],
      receiverName: query["receiverName"],
      address: query["address"],
      city: query["city"],
      phoneNumber: query["phoneNumber"],
      dueDate: DateTime.parse(query["dueDate"]),
      courierName: query["courierName"],
      shippingCost: double.parse((query["shippingCost"] ?? 0).toString()),
      wishCardDescription: query["wishCardDescription"],
      orderDescription: query["orderDescription"],
      price: double.parse(query["price"].toString()),
      orderStatus: query["orderStatus"],
      createdAt: DateTime.parse(query["createdAt"]),
    );
  }

  @override
  Map<String, dynamic> toMap(object) {
    return <String, dynamic>{
      "orderId": object.orderId,
      "orderName": object.orderName,
      "orderNo": object.orderNo,
      "customerName": object.customerName,
      "receiverName": object.receiverName,
      "address": object.address,
      "city": object.city,
      "phoneNumber": object.phoneNumber,
      "dueDate": object.dueDate.toString(),
      "courierName": object.courierName,
      "shippingCost": object.shippingCost,
      "wishCardDescription": object.wishCardDescription,
      "orderDescription": object.orderDescription,
      "price": object.price,
      "orderStatus": object.orderStatus,
      "createdAt": object.createdAt.toString()
    };
  }
}
