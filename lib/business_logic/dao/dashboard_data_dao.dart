import 'package:gio_gift_app/business_logic/models/dashboard_data.dart';

import 'dao.dart';

class DashboardDataDao implements Dao<DashboardData> {
  final String tableName = "";

  @override
  String get createTableQuery => "";

  @override
  List<DashboardData> fromList(List<Map<String, dynamic>> query) {
    return query.map((order) {
      return fromMap(order);
    }).toList();
  }

  @override
  fromMap(Map<String, dynamic> query) {
    return DashboardData(
      orderPendingCount: query["orderPendingCount"],
      orderReadyToShipCount: query["orderReadyToShipCount"],
      orderInProgressCount: query["orderInProgressCount"],
    );
  }

  @override
  Map<String, dynamic> toMap(object) {
    return <String, dynamic>{
      "orderPendingCount": object.orderPendingCount,
      "orderReadyToShipCount": object.orderReadyToShipCount,
      "orderInProgressCount": object.orderInProgressCount,
    };
  }
}
