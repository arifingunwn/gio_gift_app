import 'package:gio_gift_app/business_logic/models/customer.dart';
import 'dao.dart';

class CustomerDao implements Dao<Customer> {
  final String tableName = "customer";

  @override
  String get createTableQuery =>
      "CREATE TABLE $tableName(customerId TEXT PRIMARY KEY NOT NULL, customerName TEXT NOT NULL, phoneNumber TEXT NOT NULL, address TEXT, city TEXT, createdAt TEXT)";

  @override
  List<Customer> fromList(List<Map<String, dynamic>> query) {
    return query.map((order) {
      return fromMap(order);
    }).toList();
  }

  @override
  fromMap(Map<String, dynamic> query) {
    return Customer(
        customerId: query["customerId"],
        customerName: query["customerName"],
        phoneNumber: query["phoneNumber"],
        address: query["address"],
        city: query["city"],
        createdAt: DateTime.parse(query["createdAt"]));
  }

  @override
  Map<String, dynamic> toMap(object) {
    return <String, dynamic>{
      "customerId": object.customerId,
      "customerName": object.customerName,
      "phoneNumber": object.phoneNumber,
      "address": object.address,
      "city": object.city,
      "createdAt": object.createdAt.toString(),
    };
  }
}
