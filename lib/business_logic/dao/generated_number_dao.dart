import 'package:gio_gift_app/business_logic/models/generated_number.dart';
import 'dao.dart';

class GeneratedNumberDao implements Dao<GeneratedNumber> {
  final String tableName = "generatedNumber";

  @override
  String get createTableQuery =>
      "CREATE TABLE $tableName(generatedNumberId TEXT PRIMARY KEY NOT NULL, generatedNumberName TEXT NOT NULL, lastGeneratedNumber INT NOT NULL, month INT NOT NULL, year INT NOT NULL)";

  @override
  List<GeneratedNumber> fromList(List<Map<String, dynamic>> query) {
    return query.map((order) {
      return fromMap(order);
    }).toList();
  }

  @override
  fromMap(Map<String, dynamic> query) {
    return GeneratedNumber(
      generatedNumberId: query["generatedNumberId"],
      generatedNumberName: query["generatedNumberName"],
      lastGeneratedNumber: query["lastGeneratedNumber"],
      month: query["month"],
      year: query["year"],
    );
  }

  @override
  Map<String, dynamic> toMap(object) {
    return <String, dynamic>{
      "generatedNumberId": object.generatedNumberId,
      "generatedNumberName": object.generatedNumberName,
      "lastGeneratedNumber": object.lastGeneratedNumber,
      "month": object.month,
      "year": object.year,
    };
  }
}
