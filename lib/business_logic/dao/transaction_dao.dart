import 'package:enum_to_string/enum_to_string.dart';
import 'package:gio_gift_app/business_logic/models/transaction.dart';
import 'dao.dart';

class TransactionDao implements Dao<Transaction> {
  final String tableName = "[transaction]";

  @override
  String get createTableQuery =>
      "CREATE TABLE $tableName(transactionId TEXT PRIMARY KEY NOT NULL, transactionDate TEXT NOT NULL, transactionType TEXT NOT NULL, referenceId TEXT NOT NULL, referenceNo TEXT, description TEXT NOT NULL, debit REAL NOT NULL, credit REAL NOT NULL, balance REAL NOT NULL, createdAt TEXT NOT NULL)";

  @override
  List<Transaction> fromList(List<Map<String, dynamic>> query) {
    return query.map((order) {
      return fromMap(order);
    }).toList();
  }

  @override
  fromMap(Map<String, dynamic> query) {
    return Transaction(
        transactionId: query["transactionId"],
        transactionDate: DateTime.parse(query["transactionDate"].toString()),
        transactionType: EnumToString.fromString(
            TransactionType.values, query["transactionType"].toString()),
        referenceNo: query["referenceNo"],
        referenceId: query["referenceId"],
        description: query["description"],
        debit: query["debit"],
        credit: query["credit"],
        balance: query["balance"],
        createdAt: DateTime.parse(query["createdAt"]));
  }

  @override
  Map<String, dynamic> toMap(object) {
    return <String, dynamic>{
      "transactionId": object.transactionId,
      "transactionDate": object.transactionDate.toString(),
      "transactionType": object.getTransactionTypeString,
      "referenceId": object.referenceId,
      "referenceNo": object.referenceNo,
      "description": object.description,
      "debit": object.debit,
      "credit": object.credit,
      "balance": object.balance,
      "createdAt": object.createdAt.toString(),
    };
  }
}
