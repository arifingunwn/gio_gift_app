import 'package:gio_gift_app/business_logic/models/order_material.dart';
import 'package:gio_gift_app/business_logic/models/order_material_detail.dart';
import 'package:gio_gift_app/business_logic/models/raw_material.dart';
import 'dao.dart';

class OrderMaterialDetailDao implements Dao<OrderMaterialDetail> {
  final String tableName = "orderMaterialDetail";

  @override
  String get createTableQuery =>
      "CREATE TABLE $tableName(orderMaterialDetailId TEXT PRIMARY KEY NOT NULL, orderMaterialId TEXT NOT NULL, rawMaterialId TEXT NOT NULL, qtyUsed REAL NOT NULL, cogs REAL NOT NULL)";

  @override
  List<OrderMaterialDetail> fromList(List<Map<String, dynamic>> query) {
    return query.map((order) {
      return fromMap(order);
    }).toList();
  }

  @override
  fromMap(Map<String, dynamic> query) {
    return OrderMaterialDetail(
        orderMaterialDetailId: query["orderMaterialDetailId"],
        orderMaterial: OrderMaterial(orderMaterialId: query["orderMaterialId"]),
        rawMaterial: RawMaterial(
          rawMaterialId: query["rawMaterialId"],
          materialName: query["materialName"],
          qty: double.parse(query["qty"].toString()),
          cogs: double.parse(query["cogs"].toString()),
        ),
        qtyUsed: double.parse(query["qtyUsed"].toString()),
        cogs: double.parse(query["cogs"].toString()));
  }

  @override
  Map<String, dynamic> toMap(object) {
    return <String, dynamic>{
      "orderMaterialDetailId": object.orderMaterialDetailId,
      "orderMaterialId": object.orderMaterial.orderMaterialId,
      "rawMaterialId": object.rawMaterial.rawMaterialId,
      "qtyUsed": object.qtyUsed,
      "cogs": object.cogs
    };
  }
}
