import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gio_gift_app/views/customer/customer_screen.dart';
import 'package:gio_gift_app/views/finance/finance_transaction_screen.dart';
import 'package:gio_gift_app/views/order/order_screen.dart';
import 'package:gio_gift_app/views/purchase/purchase_screen.dart';
import 'package:gio_gift_app/views/raw_material/raw_material_screen.dart';
import 'views/homescreen.dart';
import 'package:gio_gift_app/locator.dart' as service_locator;

void main() async {
  service_locator.init();
  Bloc.observer = SimpleBlocDelegate();
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Gio Gift Studio',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => HomeScreen(),
        '/Order': (context) => OrderScreen(),
        '/Purchase': (context) => PurchaseScreen(),
        '/RawMaterial': (context) => RawMaterialScreen(),
        '/Customer': (context) => CustomerScreen(),
        '/FinanceTransaction': (context) => FinanceTransactionScreen(),
      },
    );
  }
}

class SimpleBlocDelegate extends BlocObserver {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    print(event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }

  @override
  void onError(Cubit bloc, Object error, StackTrace stacktrace) {
    super.onError(bloc, error, stacktrace);
    print(error);
  }
}
