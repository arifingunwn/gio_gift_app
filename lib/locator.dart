import 'package:get_it/get_it.dart';
import 'package:gio_gift_app/business_logic/repositories/implementations/order_repository_impl.dart';
import 'package:gio_gift_app/business_logic/repositories/implementations/transaction_repository_impl.dart';
import 'package:gio_gift_app/business_logic/repositories/order_repository.dart';
import 'package:gio_gift_app/business_logic/repositories/transaction_repository.dart';
import 'package:gio_gift_app/business_logic/services/order_service.dart';
import 'package:gio_gift_app/business_logic/services/purchase_detail_service.dart';
import 'package:gio_gift_app/business_logic/services/purchase_service.dart';
import 'package:gio_gift_app/business_logic/services/raw_material_service.dart';
import 'package:gio_gift_app/business_logic/services/transaction_service.dart';
import 'business_logic/repositories/customer_repository.dart';
import 'business_logic/repositories/dashboard_repository.dart';
import 'business_logic/repositories/generated_number_repository.dart';
import 'business_logic/repositories/implementations/customer_repository_impl.dart';
import 'business_logic/repositories/implementations/dashboard_repository_impl.dart';
import 'business_logic/repositories/implementations/generated_number_repository_impl.dart';
import 'business_logic/repositories/implementations/order_material_detail_repository_impl.dart';
import 'business_logic/repositories/implementations/order_material_repository_impl.dart';
import 'business_logic/repositories/implementations/purchase_detail_repository_impl.dart';
import 'business_logic/repositories/implementations/purchase_repository_impl.dart';
import 'business_logic/repositories/implementations/raw_material_repository_impl.dart';
import 'business_logic/repositories/order_material_detail_repository.dart';
import 'business_logic/repositories/order_material_repository.dart';
import 'business_logic/repositories/purchase_detail_repository.dart';
import 'business_logic/repositories/purchase_repository.dart';
import 'business_logic/repositories/raw_material_repository.dart';
import 'business_logic/services/customer_service.dart';
import 'business_logic/services/dashboard_service.dart';
import 'business_logic/services/generated_number_service.dart';
import 'business_logic/services/order_material_service.dart';

final sl = GetIt.instance;

void init() {
  //Service
  sl.registerLazySingleton<CustomerService>(() => CustomerServiceImpl());
  sl.registerLazySingleton<GeneratedNumberService>(
      () => GeneratedNumberServiceImpl());
  sl.registerLazySingleton<OrderService>(() => OrderServiceImpl());
  sl.registerLazySingleton<OrderMaterialService>(
      () => OrderMaterialServiceImpl());
  sl.registerLazySingleton<PurchaseService>(() => PurchaseServiceImpl());
  sl.registerLazySingleton<PurchaseDetailService>(
      () => PurchaseDetailServiceImpl());
  sl.registerLazySingleton<RawMaterialService>(() => RawMaterialServiceImpl());
  sl.registerLazySingleton<DashboardService>(() => DashboardServiceImpl());
  sl.registerLazySingleton<TransactionService>(() => TransactionServiceImpl());

  //Repositories
  sl.registerLazySingleton<OrderRepository>(() => OrderRepositoryImpl());
  sl.registerLazySingleton<OrderMaterialRepository>(
      () => OrderMaterialRepositoryImpl());
  sl.registerLazySingleton<OrderMaterialDetailRepository>(
      () => OrderMaterialDetailRepositoryImpl());
  sl.registerLazySingleton<CustomerRepository>(() => CustomerRepositoryImpl());
  sl.registerLazySingleton<GeneratedNumberRepository>(
      () => GeneratedNumberRepositoryImpl());
  sl.registerLazySingleton<PurchaseRepository>(() => PurchaseRepositoryImpl());
  sl.registerLazySingleton<PurchaseDetailRepository>(
      () => PurchaseDetailRepositoryImpl());
  sl.registerLazySingleton<RawMaterialRepository>(
      () => RawMaterialRepositoryImpl());
  sl.registerLazySingleton<DashboardRepository>(
      () => DashboardRepositoryImpl());
  sl.registerLazySingleton<TransactionRepository>(
      () => TransactionRepositoryImpl());
}
