import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/finance_transaction/finance_transaction_bloc.dart';
import 'package:gio_gift_app/business_logic/models/transaction.dart';
import 'package:intl/intl.dart';
import 'package:timeline_tile/timeline_tile.dart';

class FinanceTransactionScreen extends StatefulWidget {
  @override
  _FinanceTransactionScreenState createState() =>
      _FinanceTransactionScreenState();
}

class _FinanceTransactionScreenState extends State<FinanceTransactionScreen> {
  final numberFormat = NumberFormat();
  final dateFormat = DateFormat();
  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;
  final _transactionBloc = FinanceTransactionBloc();

  ThemeData theme;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onListViewScroll);
  }

  @override
  Widget build(BuildContext context) {
    theme = Theme.of(context);

    var now = DateTime.now();
    _transactionBloc.add(FinanceTransactionFetch(
      startDate: DateTime(now.year, now.month, 1),
      endDate: DateTime(now.year, now.month + 1, now.day),
      offset: 0,
    ));

    return Scaffold(
      appBar: AppBar(
        title: Text("Finance Transaction"),
        backgroundColor: Colors.orange[800],
        elevation: 0,
        actions: [
          IconButton(
            icon: Icon(Icons.filter_alt),
            onPressed: () {
              _showFilterDialog(context);
            },
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: BlocProvider(
        create: (context) {
          return _transactionBloc;
        },
        child: Column(
          children: [
            Container(
              height: 60,
              decoration: BoxDecoration(color: Colors.orange[800], boxShadow: [
                BoxShadow(
                    blurRadius: 2,
                    spreadRadius: 2,
                    color: Colors.black.withOpacity(0.2)),
              ]),
              padding: EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Debit",
                      style: theme.textTheme.bodyText1
                          .copyWith(color: Colors.white)),
                  Text("Balance",
                      style: theme.textTheme.bodyText1
                          .copyWith(color: Colors.white)),
                  Text("Credit",
                      style: theme.textTheme.bodyText1
                          .copyWith(color: Colors.white)),
                ],
              ),
            ),
            BlocBuilder(
              cubit: _transactionBloc,
              builder: (context, state) {
                if (state is FinanceTransactionLoaded) {
                  if (state.financeTransactions == null) {
                    return Center(child: Text("Empty"));
                  }
                  return Expanded(
                    child: ListView.builder(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      controller: _scrollController,
                      itemCount: state.financeTransactions.length,
                      itemBuilder: (context, index) {
                        return _buildTimeLineTile(
                            index,
                            state.financeTransactions[index].transactionType,
                            state.financeTransactions[index].debit,
                            state.financeTransactions[index].credit,
                            state.financeTransactions[index].transactionDate,
                            state.financeTransactions[index].description,
                            state.financeTransactions[index].balance);
                      },
                    ),
                  );
                }
                return Center(child: CircularProgressIndicator());
              },
            ),
            BlocBuilder(
              cubit: _transactionBloc,
              builder: (context, state) {
                if (state is FinanceTransactionLoadingBottom) {
                  return Expanded(
                    child: Container(
                        height: 50, child: CircularProgressIndicator()),
                  );
                }
                if (state is FinanceTransactionLoaded) {
                  if (state.isReachedMax) {
                    return Container(
                      height: 50,
                      child: Text("No more Data"),
                    );
                  }
                }
                return SizedBox();
              },
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.add),
      ),
    );
  }

  Future<bool> _showFilterDialog(BuildContext context) {
    TextEditingController startDateController = new TextEditingController();
    TextEditingController endDateController = new TextEditingController();

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return AlertDialog(
                title: Text("Filter"),
                actions: [
                  FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text("Cancel")),
                  FlatButton(
                      onPressed: () {
                        _transactionBloc.add(FinanceTransactionSetFilter(
                            startDateController.text, endDateController.text));
                        Navigator.pop(context);
                      },
                      child: Text("Ok"))
                ],
                content: Container(
                  padding: EdgeInsets.all(10),
                  height: 150,
                  child: Column(
                    children: [
                      TextFormField(
                        controller: startDateController,
                        keyboardType: TextInputType.number,
                        readOnly: true,
                        onTap: () {
                          _showDatePicker(context, startDateController);
                        },
                        decoration: InputDecoration(
                          labelText: "Start Date",
                        ),
                        validator: (val) {
                          return val.isEmpty
                              ? "Start Date can't be empty"
                              : null;
                        },
                      ),
                      TextFormField(
                        controller: endDateController,
                        keyboardType: TextInputType.number,
                        readOnly: true,
                        onTap: () {
                          _showDatePicker(context, endDateController);
                        },
                        decoration: InputDecoration(
                          labelText: "End Date",
                        ),
                        validator: (val) {
                          return val.isEmpty ? "End Date can't be empty" : null;
                        },
                      )
                    ],
                  ),
                ),
              );
            },
          );
        }).then((_) {
      return true;
    });
  }

  Future<void> _showDatePicker(
      BuildContext context, TextEditingController controller) async {
    final DateTime now = DateTime.now();
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: now,
        firstDate: DateTime(2020, 1),
        lastDate: DateTime(2100, 12));

    if (picked != null && picked != now) {
      controller.text = DateFormat('yyyy-MM-dd').format(picked).toString();
    }
  }

  Widget _buildTimeLineTile(
      int index,
      TransactionType transactionType,
      double debit,
      double credit,
      DateTime transDate,
      String description,
      double balance) {
    return TimelineTile(
      alignment: TimelineAlign.center,
      beforeLineStyle: LineStyle(color: Colors.deepOrange),
      afterLineStyle: LineStyle(color: Colors.deepOrange),
      indicatorStyle: IndicatorStyle(
          height: 30,
          width: 120,
          drawGap: true,
          indicatorXY: 0.5,
          color: Colors.deepOrange,
          indicator: Center(
              child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
                text: DateFormat('dd MMM yyyy hh:mm').format(transDate),
                style: theme.textTheme.subtitle2.copyWith(fontSize: 10),
                children: [
                  TextSpan(text: "\n"),
                  TextSpan(
                      text: "Rp. " + numberFormat.format(balance),
                      style: theme.textTheme.subtitle2.copyWith(fontSize: 12)),
                ]),
          ))),
      startChild:
          debit > 0 ? _buildDebit(debit, description, transactionType) : null,
      endChild: credit > 0
          ? _buildCredit(credit, description, transactionType)
          : null,
      isFirst: index == 0,
    );
  }

  Widget _buildDebit(
      double amount, String description, TransactionType transactionType) {
    return Stack(
      overflow: Overflow.visible,
      children: [
        Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(color: Colors.white, boxShadow: [
              BoxShadow(
                  blurRadius: 2,
                  spreadRadius: 2,
                  color: Colors.black.withOpacity(0.2))
            ]),
            height: 80,
            width: 200,
            child: RichText(
              text: TextSpan(
                  text: description,
                  style: theme.textTheme.bodyText1.copyWith(fontSize: 10),
                  children: [
                    TextSpan(text: "\n"),
                    TextSpan(
                        text: "+" + numberFormat.format(amount),
                        style: theme.textTheme.subtitle2
                            .copyWith(fontSize: 11, color: Colors.green))
                  ]),
            )),
        Positioned(
          right: -10,
          bottom: -10,
          child: _getIconByTransactionType(transactionType),
        )
      ],
    );
  }

  Widget _buildCredit(
      double amount, String description, TransactionType transactionType) {
    return Stack(
      overflow: Overflow.visible,
      children: [
        Container(
            padding: EdgeInsets.all(10),
            height: 80,
            width: 200,
            decoration: BoxDecoration(color: Colors.white, boxShadow: [
              BoxShadow(
                  blurRadius: 2,
                  spreadRadius: 2,
                  color: Colors.black.withOpacity(0.2))
            ]),
            child: RichText(
              text: TextSpan(
                  text: description,
                  style: theme.textTheme.bodyText1.copyWith(fontSize: 10),
                  children: [
                    TextSpan(text: "\n"),
                    TextSpan(
                        text: "-" + numberFormat.format(amount),
                        style: theme.textTheme.subtitle2
                            .copyWith(fontSize: 12, color: Colors.red))
                  ]),
            )),
        Positioned(
          left: -10,
          bottom: -10,
          child: _getIconByTransactionType(transactionType),
        )
      ],
    );
  }

  Icon _getIconByTransactionType(TransactionType transactionType) {
    switch (transactionType) {
      case TransactionType.ADD_PURCHASE:
        return Icon(Icons.add_shopping_cart, color: Colors.green);
      case TransactionType.UPDATE_PURCHASE:
        return Icon(
          Icons.remove_shopping_cart,
          color: Colors.orange,
        );
      case TransactionType.PROCEED_ORDER:
        return Icon(Icons.assignment, color: Colors.blue);
      case TransactionType.CANCEL_ORDER:
        return Icon(
          Icons.cancel,
          color: Colors.red,
        );
      case TransactionType.SHIP_ORDER:
        return Icon(Icons.local_shipping);
      case TransactionType.UPDATE_ORDER:
        return Icon(
          Icons.assignment,
          color: Colors.orange,
        );
      case TransactionType.PAYMENT:
        return Icon(
          Icons.money_off,
          color: Colors.red,
        );
      case TransactionType.RECEIPTS:
        return Icon(
          Icons.money_off,
          color: Colors.green,
        );
      default:
        return null;
    }
  }

  void _onListViewScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      var now = DateTime.now();
      _transactionBloc.add(FinanceTransactionFetch(
        startDate: DateTime(now.year, now.month, 1),
        endDate: DateTime(now.year, now.month + 1, now.day),
      ));
    }
  }

  // FutureBuilder<dynamic> _buildFuture() {
  //   TextStyle dataColumnStyle = theme.textTheme.bodyText1
  //       .copyWith(fontSize: 12, fontWeight: FontWeight.bold);
  //   TextStyle dataRowStyle = theme.textTheme.bodyText1.copyWith(fontSize: 10);
  //   FutureBuilder(
  //     future: _transactionService.getTransations(),
  //     builder: (context, AsyncSnapshot<List<Transaction>> snapshot) {
  //       if (snapshot.hasData) {
  //         return DataTable(
  //           columnSpacing: 20,
  //           horizontalMargin: 20,
  //           headingRowColor: MaterialStateColor.resolveWith((state) {
  //             return Colors.yellow[900];
  //           }),
  //           columns: [
  //             DataColumn(
  //                 label: Text(
  //               "Date",
  //               style: dataColumnStyle,
  //             )),
  //             DataColumn(
  //                 label: Text(
  //               "Description",
  //               style: dataColumnStyle,
  //             )),
  //             DataColumn(
  //                 label: Text(
  //               "Debit",
  //               style: dataColumnStyle,
  //             )),
  //             DataColumn(label: Text("Credit", style: dataColumnStyle)),
  //             DataColumn(label: Text("Balance", style: dataColumnStyle)),
  //           ],
  //           rows: snapshot.data.map<DataRow>((transFinance) {
  //             return DataRow(cells: [
  //               DataCell(Text(
  //                 DateFormat('dd-MMM-yyyy')
  //                     .format(transFinance.transactionDate),
  //                 style: dataRowStyle,
  //               )),
  //               DataCell(Text(transFinance.description, style: dataRowStyle)),
  //               DataCell(Text(numberFormat.format(transFinance.debit),
  //                   style: dataRowStyle.copyWith(
  //                       color: Colors.green, fontWeight: FontWeight.bold))),
  //               DataCell(Text(numberFormat.format(transFinance.credit),
  //                   style: dataRowStyle.copyWith(
  //                       color: Colors.red, fontWeight: FontWeight.bold))),
  //               DataCell(Text(numberFormat.format(transFinance.balance),
  //                   style: dataRowStyle.copyWith(fontWeight: FontWeight.bold)))
  //             ]);
  //           }).toList(),
  //         );
  //       }

  //       return Center(
  //         child: CircularProgressIndicator(),
  //       );
  //     },
  //   );
  // }
}
