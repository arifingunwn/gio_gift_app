import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/customer/customer_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/customer/customer_event.dart';
import 'package:gio_gift_app/business_logic/blocs/customer/customer_state.dart';
import 'package:gio_gift_app/business_logic/services/customer_service.dart';
import 'package:gio_gift_app/locator.dart';

class CustomerScreen extends StatefulWidget {
  @override
  _CustomerScreenState createState() => _CustomerScreenState();
}

class _CustomerScreenState extends State<CustomerScreen> {
  CustomerBloc _customerBloc;
  CustomerService _customerService;

  @override
  void initState() {
    super.initState();

    _customerService = sl();
    _customerBloc = new CustomerBloc(_customerService);
  }

  @override
  void dispose() {
    _customerBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _customerBloc.add(CustomerFetch());
    return Scaffold(
      appBar: AppBar(
        title: Text("Customer"),
        backgroundColor: Colors.blue[800],
      ),
      body: BlocProvider(
        create: (BuildContext context) {
          return _customerBloc;
        },
        child: Container(
          child: BlocConsumer(
            cubit: _customerBloc,
            listener: (context, state) => {
              if (state is CustomerFetchError)
                {
                  print(state.error.toString()),
                  Scaffold.of(context).showSnackBar(
                    SnackBar(
                      content: Text('${state.error}'),
                      backgroundColor: Colors.red,
                    ),
                  )
                }
            },
            builder: (context, state) {
              if (state is CustomerFetchSuccess) {
                return Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                        child: ListView.builder(
                            itemCount: state.customerList.length,
                            itemBuilder: (context, index) {
                              return Card(
                                child: Container(
                                  padding: EdgeInsets.all(20),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Text(
                                            state.customerList[index]
                                                .customerName,
                                            style: TextStyle(fontSize: 18),
                                          ),
                                          Spacer(),
                                          Text(
                                            state.customerList[index]
                                                .phoneNumber,
                                            style: TextStyle(fontSize: 18),
                                          )
                                        ],
                                      ),
                                      Text(
                                        state.customerList[index].city,
                                        style: TextStyle(fontSize: 16),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            }))
                  ],
                );
              }

              return Center(
                child: CircularProgressIndicator(),
              );
            },
          ),
        ),
      ),
    );
  }
}
