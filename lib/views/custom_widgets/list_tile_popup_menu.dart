import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListTilePopUpMenu extends StatefulWidget {
  final String id;
  final String title;
  final String subtitle;
  final String leading;

  final ValueSetter<String> onEdit;
  final ValueSetter<String> onDelete;

  ListTilePopUpMenu(
      {Key key,
      @required this.id,
      @required this.title,
      @required this.subtitle,
      this.leading,
      @required this.onDelete,
      @required this.onEdit})
      : super(key: key);
  @override
  _ListTilePopUpMenuState createState() => _ListTilePopUpMenuState();
}

class _ListTilePopUpMenuState extends State<ListTilePopUpMenu> {
  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      offset: Offset(100, 0),
      itemBuilder: (_) => [
        new PopupMenuItem(
          child: Text("Edit"),
          value: "EDIT",
        ),
        new PopupMenuItem(
          child: Text("Delete"),
          value: "DELETE",
        ),
      ],
      onSelected: (value) {
        _popUpMenuTap(value);
      },
      child: ListTile(
        title: Text(this.widget.title),
        subtitle: Text(this.widget.subtitle),
        trailing: Icon(Icons.more_vert),
        isThreeLine: true,
      ),
    );
  }

  void _popUpMenuTap(value) {
    if (value == "EDIT") {
      this.widget.onEdit(this.widget.id);
    }

    if (value == "DELETE") {
      this.widget.onDelete(this.widget.id);
    }
  }
}
