import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/order/order_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/order_form/order_form_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/order_form/order_form_event.dart';
import 'package:gio_gift_app/business_logic/blocs/order_form/order_form_state.dart';
import 'package:gio_gift_app/business_logic/models/order.dart';
import 'package:gio_gift_app/business_logic/utils/debouncer.dart';
import 'package:intl/intl.dart';

class OrderAddScreen extends StatefulWidget {
  final bool isEdit;
  final String orderId;

  final OrderBloc orderbloc;

  const OrderAddScreen({Key key, this.isEdit, this.orderbloc, this.orderId})
      : super(key: key);

  @override
  _OrderAddScreenState createState() => _OrderAddScreenState(this.orderbloc);
}

class _OrderAddScreenState extends State<OrderAddScreen> {
  Order _order = Order();
  final OrderBloc _orderBloc;
  OrderFormBloc _orderFormBloc;

  final TextStyle labelStyle = TextStyle(fontSize: 16);
  final TextStyle itemStyle = TextStyle(fontSize: 18);

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _customerNameController = TextEditingController();
  final TextEditingController _receiverNameController = TextEditingController();
  final TextEditingController _addressController = TextEditingController();
  final TextEditingController _cityController = TextEditingController();
  final TextEditingController _orderNameController = TextEditingController();
  final TextEditingController _dueDateController = TextEditingController();
  final TextEditingController _wishCardDescriptionController =
      TextEditingController();
  final TextEditingController _orderDescriptionController =
      TextEditingController();
  final TextEditingController _priceController = TextEditingController();

  final Debouncer _debouncer = Debouncer(1000);

  bool _isSaveCustomer = true;

  _OrderAddScreenState(this._orderBloc);

  @override
  void initState() {
    super.initState();
    _orderFormBloc = new OrderFormBloc(_orderBloc);

    if (widget.isEdit) {
      _order.orderId = widget.orderId;
    }
  }

  @override
  void dispose() {
    _orderFormBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isEdit) {
      _orderFormBloc.add(OrderFormLoadedEdit(widget.orderId));
    }

    return Scaffold(
      backgroundColor: Colors.blue[800],
      appBar: AppBar(
        title: Text(widget.isEdit ? "Edit Order" : "Add New Order"),
        backgroundColor: Colors.blue[800],
        elevation: 0,
      ),
      body: Form(
        key: _formKey,
        child: BlocConsumer<OrderFormBloc, OrderFormState>(
          cubit: _orderFormBloc,
          listener: (context, state) {
            if (state is OrderFormSaveSuccess) {
              Navigator.pop(context, "/Order");
            }
            if (state is OrderFormSaveError) {
              print(state.error.toString());
              return Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text('${state.error}'),
                  backgroundColor: Colors.red,
                ),
              );
            }
            if (state is OrderFormCustomerFetchSuccess) {
              _customerNameController.text = state.customer.customerName;
              _receiverNameController.text = state.customer.customerName;
              _addressController.text = state.customer.address;
              _cityController.text = state.customer.city;
              setState(() {
                _isSaveCustomer = false;
              });
            }

            if (state is OrderFormCustomerFetchEmpty) {
              _customerNameController.text = "";
              _receiverNameController.text = "";
              _addressController.text = "";
              _cityController.text = "";
            }

            if (state is OrderFormLoadEdit) {
              _phoneNumberController.text = state.order.phoneNumber;
              _customerNameController.text = state.order.customerName;
              _receiverNameController.text = state.order.receiverName;
              _addressController.text = state.order.address;
              _cityController.text = state.order.city;
              _orderNameController.text = state.order.orderName;
              _dueDateController.text =
                  DateFormat('yyyy-MM-dd').format(state.order.dueDate);
              _wishCardDescriptionController.text =
                  state.order.wishCardDescription;
              _orderDescriptionController.text = state.order.orderDescription;
              _priceController.text = state.order.price.toString();
            }
          },
          builder: (context, state) {
            return Container(
              padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: ListView(
                children: [
                  Card(
                    child: Container(
                      padding: EdgeInsets.all(20),
                      child: Column(
                        children: [
                          _phoneNumberTextField(),
                          Row(
                            children: [
                              Expanded(
                                flex: 10,
                                child: _customerNameTextField(state),
                              ),
                              Spacer(),
                              Expanded(
                                flex: 10,
                                child: _customerReceiverNameTextField(state),
                              ),
                            ],
                          ),
                          _addressTextField(state),
                          _cityTextField(state),
                          CheckboxListTile(
                              contentPadding: EdgeInsets.zero,
                              controlAffinity: ListTileControlAffinity.leading,
                              title: Text("Save Customer Data"),
                              value: this._isSaveCustomer,
                              onChanged: (val) {
                                if (state is OrderFormCustomerFetchEmpty) {
                                  setState(() {
                                    _isSaveCustomer = val;
                                  });
                                }
                              }),
                        ],
                      ),
                    ),
                  ),
                  Card(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                      child: Column(
                        children: [
                          _orderNameTextField(),
                          _dueDateTextField(),
                          _wishCardDescriptionTextField(),
                          _orderDescriptionTextField(),
                          _priceTextField(),
                        ],
                      ),
                    ),
                  ),
                  MaterialButton(
                    minWidth: double.infinity,
                    color: Colors.blue,
                    textColor: Colors.white,
                    child: Text(
                      "Save",
                      style: TextStyle(fontSize: 18),
                    ),
                    onPressed: () {
                      _saveForm();
                    },
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _phoneNumberTextField() {
    return TextFormField(
      controller: _phoneNumberController,
      keyboardType: TextInputType.number,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly
      ],
      onChanged: (val) {
        _onPhoneNumberChanged(val);
      },
      decoration: InputDecoration(labelText: "Phone Number"),
      onSaved: (val) {
        _order.phoneNumber = val;
      },
      validator: (val) {
        return val.isEmpty ? "Please input the phone number" : null;
      },
    );
  }

  Widget _customerNameTextField(OrderFormState state) {
    return TextFormField(
      decoration: InputDecoration(
          labelText: "Customer Name",
          suffix: state is OrderFormCustomerFetchLoading
              ? CircularProgressIndicator()
              : null),
      onSaved: (val) {
        _order.customerName = val;
      },
      controller: _customerNameController,
      validator: (val) {
        return val.isEmpty ? "Customer Name can't be empty" : null;
      },
    );
  }

  Widget _customerReceiverNameTextField(OrderFormState state) {
    return TextFormField(
      decoration: InputDecoration(
          labelText: "Receiver Name",
          suffix: state is OrderFormCustomerFetchLoading
              ? CircularProgressIndicator()
              : null),
      controller: _receiverNameController,
      onSaved: (val) {
        _order.receiverName = val;
      },
      validator: (val) {
        return val.isEmpty ? "Recepient name can't be empty" : null;
      },
    );
  }

  Widget _addressTextField(OrderFormState state) {
    return TextFormField(
      decoration: InputDecoration(
          labelText: "Address",
          suffix: state is OrderFormCustomerFetchLoading
              ? CircularProgressIndicator()
              : null),
      textInputAction: TextInputAction.newline,
      keyboardType: TextInputType.multiline,
      maxLines: 3,
      controller: _addressController,
      onSaved: (val) {
        _order.address = val;
      },
      validator: (val) {
        return val.isEmpty ? "Address can't be empty" : null;
      },
    );
  }

  Widget _cityTextField(OrderFormState state) {
    return TextFormField(
      decoration: InputDecoration(
          labelText: "City",
          suffix: state is OrderFormCustomerFetchLoading
              ? CircularProgressIndicator()
              : null),
      controller: _cityController,
      onSaved: (val) {
        _order.city = val;
      },
      validator: (val) {
        return val.isEmpty ? "City can't be empty" : null;
      },
    );
  }

  Widget _orderNameTextField() {
    return TextFormField(
      controller: _orderNameController,
      decoration: InputDecoration(
        labelText: "Order Name",
      ),
      onSaved: (val) {
        _order.orderName = val;
      },
      validator: (val) {
        return val.isEmpty ? "Order Name can't be empty" : null;
      },
    );
  }

  Widget _dueDateTextField() {
    return TextFormField(
      controller: _dueDateController,
      decoration: InputDecoration(labelText: "Due Date"),
      readOnly: true,
      onTap: () {
        _showDatePicker(context);
      },
      onSaved: (val) {
        _order.dueDate = DateTime.parse(val);
      },
      validator: (val) {
        return val.isEmpty ? "Due Date can't be empty" : null;
      },
    );
  }

  Widget _wishCardDescriptionTextField() {
    return TextFormField(
      controller: _wishCardDescriptionController,
      textInputAction: TextInputAction.newline,
      keyboardType: TextInputType.multiline,
      maxLines: 3,
      onSaved: (val) {
        _order.wishCardDescription = val;
      },
      decoration: InputDecoration(
        labelText: "Wish Card",
      ),
    );
  }

  Widget _orderDescriptionTextField() {
    return TextFormField(
        controller: _orderDescriptionController,
        textInputAction: TextInputAction.newline,
        keyboardType: TextInputType.multiline,
        maxLines: 3,
        onSaved: (val) {
          _order.orderDescription = val;
        },
        decoration: InputDecoration(
          labelText: "Description",
        ));
  }

  Widget _priceTextField() {
    return TextFormField(
      controller: _priceController,
      keyboardType: TextInputType.number,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly
      ],
      decoration: InputDecoration(
        labelText: "Price",
      ),
      onSaved: (val) {
        _order.price = double.parse(val);
      },
      validator: (val) {
        return val.isEmpty ? "Price can't be empty" : null;
      },
    );
  }

  void _onPhoneNumberChanged(String val) {
    _debouncer.run(() {
      _orderFormBloc.add(OrderFormCheckPhoneNumber(phoneNumber: val));
    });
  }

  Future<void> _saveForm() async {
    if (!_formKey.currentState.validate()) return;
    _formKey.currentState.save();
    _orderFormBloc.add(OrderFormSave(_order, widget.isEdit, _isSaveCustomer));
  }

  Future<void> _showDatePicker(BuildContext context) async {
    final DateTime now = DateTime.now();
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: now,
        firstDate: DateTime(2020, 1),
        lastDate: DateTime(2100, 12));

    if (picked != null && picked != now) {
      _dueDateController.text =
          DateFormat('yyyy-MM-dd').format(picked).toString();
    }
  }
}
