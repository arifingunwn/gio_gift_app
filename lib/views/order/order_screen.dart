import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/order/order_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/order/order_event.dart';
import 'package:gio_gift_app/business_logic/blocs/order/order_state.dart';
import 'package:gio_gift_app/business_logic/services/order_service.dart';
import 'package:gio_gift_app/business_logic/models/order.dart';
import 'package:gio_gift_app/locator.dart';
import 'package:gio_gift_app/views/order/order_detail_screen.dart';
import 'package:intl/intl.dart';

import 'order_add_screen.dart';

class OrderScreen extends StatefulWidget {
  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  final numberFormat = NumberFormat();
  OrderBloc _orderBloc;
  OrderService _orderService;

  List<String> orderStatuses =
      OrderStatus.values.map((val) => val.toString().split('.').last).toList();
  int selectedIndex = 0;

  @override
  void initState() {
    super.initState();

    _orderService = sl();
    _orderBloc = new OrderBloc(_orderService);
  }

  @override
  void dispose() {
    _orderBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final OrderStatus args = ModalRoute.of(context).settings.arguments;
    if (args != null) {
      int filteredIndex = orderStatuses
          .indexWhere((obj) => obj == args.toString().split('.').last);
      selectedIndex = filteredIndex;
    }

    _orderBloc.add(OrderFetch(orderStatus: orderStatuses[selectedIndex]));

    return Scaffold(
      backgroundColor: Colors.blue[800],
      appBar: AppBar(
        title: Text("Order"),
        backgroundColor: Colors.blue[800],
        elevation: 0,
        centerTitle: false,
      ),
      body: BlocProvider(
        create: (BuildContext context) {
          return _orderBloc;
        },
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(20),
                height: 70,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: OrderStatus.values.length,
                  itemBuilder: (context, index) {
                    return _buildFilterStatus(index);
                  },
                ),
              ),
              BlocConsumer(
                  listener: (context, state) => {
                        if (state is OrderFetchError)
                          {
                            print(state.error.toString()),
                            Scaffold.of(context).showSnackBar(
                              SnackBar(
                                content: Text('${state.error}'),
                                backgroundColor: Colors.red,
                              ),
                            )
                          }
                      },
                  cubit: _orderBloc,
                  builder: (context, state) {
                    if (state is OrderFetchSuccess) {
                      return ListView.builder(
                          shrinkWrap: true,
                          itemCount: state.orderList.length,
                          itemBuilder: (context, index) {
                            return _buildOrderTile(
                                state.orderList, context, index);
                          });
                    }

                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context,
              CupertinoPageRoute(
                  builder: (context) => OrderAddScreen(
                        orderbloc: _orderBloc,
                        isEdit: false,
                      )));
        },
        backgroundColor: Colors.blue,
        child: Icon(Icons.add),
      ),
    );
  }

  Widget _buildOrderTile(List<Order> orders, BuildContext context, int index) {
    return new Container(
      child: Card(
        child: InkWell(
          onTap: () {
            Navigator.push(
                context,
                CupertinoPageRoute(
                    builder: (context) =>
                        OrderDetailScreen(orders[index], _orderBloc)));
          },
          child: Column(
            children: [
              Padding(
                child: Row(
                  children: [
                    Text(
                      orders[index].customerName.toUpperCase(),
                      style: TextStyle(fontSize: 24),
                    ),
                    Spacer(),
                    Text(DateFormat('dd/MM/yyyy')
                        .format(orders[index].dueDate)
                        .toString())
                  ],
                ),
                padding: EdgeInsets.all(10),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                child: Row(
                  children: [
                    Text(orders[index].orderName),
                    Spacer(),
                    Text(
                        "Rp. " +
                            numberFormat.format(orders[index].price).toString(),
                        style: TextStyle(fontSize: 20, color: Colors.blue[900]))
                  ],
                ),
              ),
              Padding(
                child: Row(
                  children: [
                    Text(
                      orders[index].orderNo,
                      style: TextStyle(fontSize: 18, color: Colors.blue[900]),
                    ),
                    Spacer(),
                    _getOrderStatus(orders[index].orderStatus)
                  ],
                ),
                padding: EdgeInsets.all(10),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildFilterStatus(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedIndex = index;
        });
      },
      child: Container(
        margin: EdgeInsets.only(right: 10),
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        decoration: (selectedIndex == index
            ? BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.blue[100])
            : null),
        child: Text(
          orderStatuses[index],
          style: TextStyle(
              color: (selectedIndex == index ? Colors.blue[800] : Colors.white),
              fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  Widget _getOrderStatus(String orderStatus) {
    Color boxColor;
    Color textColor;

    if (orderStatus == OrderStatus.READYTOSHIP.toString().split('.').last) {
      boxColor = Colors.orange;
      textColor = Colors.white;
    } else if (orderStatus ==
        OrderStatus.INPROGRESS.toString().split('.').last) {
      boxColor = Colors.purple;
      textColor = Colors.white;
    } else if (orderStatus == OrderStatus.COMPLETE.toString().split('.').last) {
      boxColor = Colors.blue[800];
      textColor = Colors.white;
    } else {
      boxColor = Colors.grey;
      textColor = Colors.white;
    }

    return Container(
      padding: EdgeInsets.all(5),
      child: Text(orderStatus,
          style: TextStyle(
              color: textColor, fontSize: 12, fontWeight: FontWeight.bold)),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10), color: boxColor),
    );
  }
}
