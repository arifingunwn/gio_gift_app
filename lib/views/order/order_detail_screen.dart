import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gio_gift_app/business_logic/blocs/order/order_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/order/order_event.dart';
import 'package:gio_gift_app/business_logic/services/order_service.dart';
import 'package:gio_gift_app/business_logic/services/order_material_service.dart';
import 'package:gio_gift_app/business_logic/models/order.dart';
import 'package:gio_gift_app/business_logic/models/order_material.dart';
import 'package:gio_gift_app/locator.dart';
import 'package:gio_gift_app/views/order/order_add_screen.dart';
import 'package:gio_gift_app/views/order_material/order_material_screen.dart';
import 'package:intl/intl.dart';

class OrderDetailScreen extends StatefulWidget {
  final Order order;
  final OrderBloc orderBloc;

  const OrderDetailScreen(this.order, this.orderBloc, {Key key})
      : super(key: key);

  @override
  _OrderDetailScreenState createState() => _OrderDetailScreenState(orderBloc);
}

class _OrderDetailScreenState extends State<OrderDetailScreen> {
  final NumberFormat numberFormat = new NumberFormat();
  final TextStyle labelStyle = TextStyle(fontSize: 16, color: Colors.grey[600]);
  final TextStyle itemStyle = TextStyle(fontSize: 18);

  OrderService _orderService;
  OrderMaterialService _orderMaterialService;
  OrderBloc _orderBloc;

  _OrderDetailScreenState(this._orderBloc);

  @override
  void initState() {
    super.initState();
    _orderService = sl();
    _orderMaterialService = sl();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[800],
      appBar: AppBar(
        title: Text(widget.order.orderNo),
        backgroundColor: Colors.blue[800],
        elevation: 0,
        actions: [
          (widget.order.orderStatus !=
                  OrderStatus.COMPLETE.toString().split('.').last)
              ? FlatButton(
                  child: Text(
                    "Edit",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    Navigator.push(
                        context,
                        CupertinoPageRoute(
                            builder: (context) => OrderAddScreen(
                                  isEdit: true,
                                  orderId: widget.order.orderId,
                                  orderbloc: _orderBloc,
                                )));
                  },
                )
              : Container()
        ],
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Card(
              child: Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text(
                          "Customer",
                          style: labelStyle,
                        ),
                        Spacer(),
                        Text(
                          widget.order.customerName,
                          style: itemStyle,
                        )
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 10),
                      child: Row(
                        children: [
                          Text(
                            "Receiver",
                            style: labelStyle,
                          ),
                          Spacer(),
                          Text(
                            widget.order.receiverName,
                            style: itemStyle,
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Row(
                        children: [
                          Text(
                            "Phone Number",
                            style: labelStyle,
                          ),
                          Spacer(),
                          Text(
                            widget.order.phoneNumber,
                            style: itemStyle,
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Row(
                        children: [
                          Text(
                            "Due Date",
                            style: labelStyle,
                          ),
                          Spacer(),
                          Text(
                            DateFormat('dd/MM/yyyy')
                                .format(widget.order.dueDate)
                                .toString(),
                            style: itemStyle,
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
            child: Card(
              child: Container(
                padding: const EdgeInsets.all(20),
                child: Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Order Name",
                              style: labelStyle,
                            ),
                            Text(
                              widget.order.orderName,
                              style: itemStyle,
                            )
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Wish Card",
                                style: labelStyle,
                              ),
                              Text(
                                widget.order.wishCardDescription,
                                style: itemStyle,
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Order Description",
                                style: labelStyle,
                              ),
                              Text(
                                widget.order.orderDescription,
                                style: itemStyle,
                              )
                            ],
                          ),
                        ),
                        (widget.order.courierName != null
                            ? Padding(
                                padding: EdgeInsets.only(top: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Courier Name",
                                      style: labelStyle,
                                    ),
                                    Text(
                                      widget.order.courierName,
                                      style: itemStyle,
                                    )
                                  ],
                                ),
                              )
                            : Container(
                                key: UniqueKey(),
                              )),
                        (widget.order.shippingCost != null
                            ? Padding(
                                padding: EdgeInsets.only(top: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Shipping Cost",
                                      style: labelStyle,
                                    ),
                                    Text(
                                      numberFormat
                                          .format(widget.order.shippingCost)
                                          .toString(),
                                      style: itemStyle,
                                    )
                                  ],
                                ),
                              )
                            : Container(
                                key: UniqueKey(),
                              ))
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
          FutureBuilder(
            future:
                _orderMaterialService.getOrderMaterial(widget.order.orderId),
            builder: (context, AsyncSnapshot<OrderMaterial> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }

              if (snapshot.hasData) {
                return Container(
                  padding: EdgeInsets.all(10),
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Text(
                                "Material Used",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.blue[800]),
                              ),
                              Spacer(),
                              FlatButton(
                                  child: Text("Edit"),
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        CupertinoPageRoute(
                                            builder: (context) =>
                                                OrderMaterialScreen(
                                                    widget.order,
                                                    true,
                                                    _orderBloc)));
                                  })
                            ],
                          ),
                          Divider(),
                          ConstrainedBox(
                            constraints: BoxConstraints(minHeight: 200),
                            child: ListView.builder(
                              shrinkWrap: true,
                              itemCount:
                                  snapshot.data.orderMaterialDetails.length,
                              itemBuilder: (context, index) {
                                return ListTile(
                                  contentPadding: EdgeInsets.symmetric(
                                      vertical: 0, horizontal: 10),
                                  dense: true,
                                  leading: Text("${index + 1}."),
                                  title: Text(snapshot
                                      .data
                                      .orderMaterialDetails[index]
                                      .rawMaterial
                                      .materialName),
                                  trailing: Text(snapshot
                                      .data.orderMaterialDetails[index].qtyUsed
                                      .toString()),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              }
              return Container();
            },
          ),
          (widget.order.orderStatus ==
                  OrderStatus.PENDING.toString().split('.').last
              ? MaterialButton(
                  onPressed: () {
                    _proceedOrder(context);
                  },
                  color: Colors.blue[500],
                  height: 60,
                  elevation: 2,
                  child: Text(
                    "Proceed Order",
                    style: TextStyle(fontSize: 18, color: Colors.white),
                  ),
                )
              : Container(
                  key: UniqueKey(),
                )),
          (widget.order.orderStatus ==
                  OrderStatus.INPROGRESS.toString().split('.').last
              ? MaterialButton(
                  onPressed: () => {
                    Navigator.push(
                        context,
                        CupertinoPageRoute(
                            builder: (context) => OrderMaterialScreen(
                                widget.order, false, _orderBloc)))
                  },
                  color: Colors.blue[500],
                  height: 60,
                  elevation: 2,
                  child: Text(
                    "Add Material",
                    style: TextStyle(fontSize: 18, color: Colors.white),
                  ),
                )
              : Container(
                  key: UniqueKey(),
                )),
          (widget.order.orderStatus ==
                  OrderStatus.READYTOSHIP.toString().split('.').last
              ? MaterialButton(
                  onPressed: () => {
                    _showShippingDialog(context, widget.order.orderId)
                        ?.then((_) {
                      Navigator.pop(context);
                    })
                  },
                  color: Colors.green[500],
                  height: 60,
                  elevation: 2,
                  child: Text(
                    "SHIP ORDER",
                    style: TextStyle(fontSize: 18, color: Colors.white),
                  ),
                )
              : Container(
                  key: UniqueKey(),
                )),
          Padding(
            padding: EdgeInsets.only(top: 10),
            child: MaterialButton(
              onPressed: () => {},
              color: Colors.red[500],
              height: 60,
              elevation: 2,
              child: Text(
                "Cancel",
                style: TextStyle(fontSize: 18, color: Colors.white),
              ),
            ),
          )
        ],
      ),
    );
  }

  Future<bool> _showShippingDialog(BuildContext context, String orderId) {
    String selectedCourier = "Ficko";
    TextEditingController shippingCostController = new TextEditingController();

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return AlertDialog(
                title: Text("Shipping"),
                actions: [
                  FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text("Cancel")),
                  FlatButton(
                      onPressed: () {
                        _orderService.updateShipping(
                            orderId: orderId,
                            courierName: selectedCourier,
                            shippingCost:
                                double.parse(shippingCostController.text));
                        _orderBloc.add(OrderFetch(
                            orderStatus:
                                OrderStatus.ALL.toString().split('.').last));
                        Navigator.pop(context, "/Order");
                      },
                      child: Text("Save"))
                ],
                content: Container(
                  padding: EdgeInsets.all(10),
                  height: 200,
                  child: Column(
                    children: [
                      DropdownButton(
                          hint: Text("Courier"),
                          isExpanded: true,
                          items: [
                            DropdownMenuItem<String>(
                              value: "Ficko",
                              child: Text("Ficko"),
                            ),
                            DropdownMenuItem<String>(
                              value: "Gosend",
                              child: Text("Gosend"),
                            ),
                            DropdownMenuItem<String>(
                              value: "GrabDelivery",
                              child: Text("GrabDelivery"),
                            ),
                          ],
                          onChanged: (String val) {
                            setState(() {
                              selectedCourier = val;
                            });
                          },
                          value: selectedCourier),
                      TextFormField(
                        controller: shippingCostController,
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        decoration: InputDecoration(
                          labelText: "Shipping Cost",
                        ),
                        validator: (val) {
                          return val.isEmpty
                              ? "Shipping Cost can't be empty"
                              : null;
                        },
                      )
                    ],
                  ),
                ),
              );
            },
          );
        }).then((_) {
      return true;
    });
  }

  void _proceedOrder(BuildContext context) {
    _orderService.proceedOrder(widget.order.orderId);
    _orderBloc.add(
        OrderFetch(orderStatus: OrderStatus.ALL.toString().split('.').last));
    Navigator.pop(context, "/Order");
  }
}
