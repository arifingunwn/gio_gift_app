import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/purchase/purchase_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/purchase_form/purchase_form_bloc.dart';
import 'package:gio_gift_app/business_logic/models/purchase_detail.dart';
import 'package:gio_gift_app/views/purchase/purchase_add_item_screen.dart';
import 'package:intl/intl.dart';

class PurchaseAddScreen extends StatefulWidget {
  final bool isEdit;
  final String purchaseId;
  final PurchaseBloc purchaseBloc;

  const PurchaseAddScreen(
      {Key key, this.isEdit, this.purchaseId, this.purchaseBloc})
      : super(key: key);

  @override
  _PurchaseAddScreenState createState() =>
      _PurchaseAddScreenState(this.purchaseBloc);
}

class _PurchaseAddScreenState extends State<PurchaseAddScreen> {
  ThemeData theme;
  final NumberFormat numberFormat = NumberFormat();
  final TextEditingController _totalPriceController = TextEditingController();
  final TextEditingController _supplierNameController = TextEditingController();
  final TextEditingController _purchaseDateController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final GlobalKey<FormState> _formState = GlobalKey<FormState>();
  bool isKeyboardShowed = false;
  double screenHeight;

  PurchaseFormBloc _purchaseFormBloc;
  PurchaseBloc _purchaseBloc;

  _PurchaseAddScreenState(this._purchaseBloc);

  @override
  void initState() {
    super.initState();
    _purchaseFormBloc = PurchaseFormBloc(_purchaseBloc);
    _totalPriceController.text = "0";
  }

  @override
  void dispose() {
    _purchaseFormBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    theme = Theme.of(context);
    screenHeight = MediaQuery.of(context).size.height;

    if (widget.isEdit) {
      _purchaseFormBloc.add(PurchaseFormLoadEdit(widget.purchaseId));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.isEdit ? "Edit Purchase" : "Add Purchase"),
        backgroundColor: Colors.green[800],
      ),
      bottomNavigationBar: _buildBottomNavigationBar(),
      body: BlocProvider(
          create: (BuildContext context) {
            return _purchaseFormBloc;
          },
          child: ListView(
            children: [
              Container(
                  padding: EdgeInsets.all(10),
                  height: screenHeight,
                  child: ListView(
                    children: [
                      BlocListener<PurchaseFormBloc, PurchaseFormState>(
                        listener: (context, state) {
                          if (state.isEdit) {
                            _supplierNameController.text = state.supplierName;
                            _purchaseDateController.text =
                                DateFormat('yyyy-MM-dd')
                                    .format(state.purchaseDate)
                                    .toString();
                            _descriptionController.text = state.description;
                            _totalPriceController.text = numberFormat
                                .format(state.getCalculatedTotalAmount)
                                .toString();
                          }

                          if (state.isSuccess) {
                            Navigator.pop(context);
                          }
                        },
                        child: Form(
                          key: _formState,
                          child: Card(
                              child: Container(
                            padding: EdgeInsets.all(10),
                            child: Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 10),
                                  child: TextFormField(
                                    controller: _supplierNameController,
                                    validator: (val) {
                                      return val.isEmpty
                                          ? "Supplier Name can't be empty"
                                          : null;
                                    },
                                    decoration: InputDecoration(
                                      labelText: "Supplier Name",
                                      border: InputBorder.none,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 10),
                                  child: TextFormField(
                                    controller: _purchaseDateController,
                                    onTap: () {
                                      _showDatePicker(context);
                                    },
                                    validator: (val) {
                                      return val.isEmpty
                                          ? "Purchase Date can't be empty"
                                          : null;
                                    },
                                    decoration: InputDecoration(
                                      labelText: "Purchase Date",
                                      border: InputBorder.none,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 10),
                                  child: TextFormField(
                                    controller: _descriptionController,
                                    decoration: InputDecoration(
                                      labelText: "Description",
                                      border: InputBorder.none,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          )),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              CupertinoPageRoute(
                                  builder: (context) => PurchaseAddItemScreen(
                                      _purchaseFormBloc)));
                        },
                        child: Card(
                            margin: EdgeInsets.only(top: 10),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.green[700],
                                  borderRadius: BorderRadius.circular(10)),
                              padding: EdgeInsets.all(15),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.add_box,
                                    color: Colors.white,
                                  ),
                                  Text(
                                    "  Add Item",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                  Spacer(),
                                  Icon(Icons.chevron_right, color: Colors.white)
                                ],
                              ),
                            )),
                      ),
                      Container(
                        height: screenHeight * 0.3,
                        padding: EdgeInsets.all(10),
                        width: double.infinity,
                        child: BlocBuilder(
                          cubit: _purchaseFormBloc,
                          builder: (context, PurchaseFormState state) {
                            if (state.purchaseDetails.length > 0) {
                              return ListView.builder(
                                itemCount: state.purchaseDetails.length,
                                itemBuilder: (context, index) {
                                  return _buildPurchaseItem(
                                      state.purchaseDetails[index], index);
                                },
                              );
                            }

                            return Center(
                                child: Text("There is no item added yet."));
                          },
                        ),
                      ),
                    ],
                  )),
            ],
          )),
    );
  }

  void _savePurchase() {
    if (!_formState.currentState.validate()) return;
    _purchaseFormBloc.add(PurchaseFormSave(
        widget.purchaseId,
        _supplierNameController.text,
        DateTime.parse(_purchaseDateController.text),
        _descriptionController.text,
        widget.isEdit));
  }

  Future<void> _showDatePicker(BuildContext context) async {
    final DateTime now = DateTime.now();
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: now,
        firstDate: DateTime(2020, 1),
        lastDate: DateTime(2100, 12));

    if (picked != null && picked != now) {
      _purchaseDateController.text =
          DateFormat('yyyy-MM-dd').format(picked).toString();
    }
  }

  String _getTotalPrice(List<PurchaseDetail> purchaseDetails) {
    String totalPrice = "Rp. 0";
    if (purchaseDetails.length > 0) {
      double total =
          purchaseDetails.fold(0, (sum, curr) => sum + (curr.totalPrice));
      totalPrice = "Rp. " + numberFormat.format(total).toString();
    }
    return totalPrice;
  }

  Widget _buildBottomNavigationBar() {
    return Container(
        height: screenHeight * 0.15,
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
              blurRadius: 3,
              spreadRadius: 2,
              color: Colors.grey.withOpacity(0.3))
        ]),
        child: Column(
          children: [
            SizedBox(height: 20),
            Row(
              children: [
                BlocBuilder(
                  cubit: _purchaseFormBloc,
                  builder: (context, PurchaseFormState state) {
                    return Text.rich(
                      TextSpan(text: "Total:\n", children: [
                        TextSpan(
                            text: _getTotalPrice(state.purchaseDetails),
                            style: TextStyle(fontSize: 26))
                      ]),
                    );
                  },
                ),
                Spacer(),
                FlatButton(
                  minWidth: 150,
                  height: 45,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  onPressed: () {
                    _savePurchase();
                  },
                  color: Colors.green[600],
                  child: Text(
                    "Save",
                    style: TextStyle(fontSize: 16, color: Colors.white),
                  ),
                )
              ],
            )
          ],
        ));
  }

  Widget _buildPurchaseItem(PurchaseDetail purchaseDetail, int index) {
    return Container(
        margin: EdgeInsets.only(top: 10),
        padding: EdgeInsets.all(10),
        height: 60,
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
              blurRadius: 3,
              spreadRadius: 2,
              color: Colors.grey.withOpacity(0.5))
        ]),
        child: Row(
          children: [
            Expanded(flex: 1, child: Text("${index + 1}.")),
            Expanded(
                flex: 6,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      purchaseDetail.rawMaterial.materialName,
                      style: theme.textTheme.bodyText2,
                    ),
                    Text(numberFormat.format(purchaseDetail.qty).toString() +
                        " x @Rp. " +
                        numberFormat.format(purchaseDetail.price).toString())
                  ],
                )),
            Spacer(),
            Text(
                "Rp. " +
                    numberFormat
                        .format(purchaseDetail.qty * purchaseDetail.price)
                        .toString(),
                style: theme.textTheme.bodyText1),
            IconButton(
                onPressed: () {
                  _purchaseFormBloc.add(PurchaseFormRemoveItem(
                      purchaseDetail.rawMaterial.rawMaterialId));
                },
                icon: Icon(Icons.highlight_remove, color: Colors.red)),
          ],
        ));
  }
}
