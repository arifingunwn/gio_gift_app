import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gio_gift_app/business_logic/repositories/implementations/purchase_detail_repository_impl.dart';
import 'package:gio_gift_app/business_logic/repositories/implementations/raw_material_repository_impl.dart';
import 'package:gio_gift_app/business_logic/services/purchase_detail_service.dart';
import 'package:gio_gift_app/business_logic/models/purchase.dart';
import 'package:gio_gift_app/business_logic/models/purchase_detail.dart';
import 'package:gio_gift_app/locator.dart';
import 'package:gio_gift_app/views/purchase/purchase_add_screen.dart';
import 'package:intl/intl.dart';

class PurchaseDetailScreen extends StatefulWidget {
  final Purchase purchase;

  const PurchaseDetailScreen({Key key, this.purchase}) : super(key: key);
  @override
  _PurchaseDetailScreenState createState() => _PurchaseDetailScreenState();
}

class _PurchaseDetailScreenState extends State<PurchaseDetailScreen> {
  double screenHeight;
  final NumberFormat numberFormat = NumberFormat();
  PurchaseDetailService _purchaseDetailService;
  ThemeData theme;

  @override
  void initState() {
    super.initState();
    _purchaseDetailService = sl();
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    theme = Theme.of(context);

    return Scaffold(
      backgroundColor: Colors.green[700],
      bottomNavigationBar: _buildBottomNavigationBar(),
      appBar: AppBar(
        backgroundColor: Colors.green[700],
        elevation: 0,
        title: Text(widget.purchase.purchaseNo),
        actions: [
          FlatButton(
            onPressed: () {
              _goToEditScreen(widget.purchase.purchaseId);
            },
            child: Text(
              "Edit",
              style: theme.textTheme.headline6.copyWith(color: Colors.white),
            ),
          )
        ],
      ),
      body: ListView(
        padding: EdgeInsets.all(10),
        children: [
          Card(
            child: Container(
              padding: EdgeInsets.all(20),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text(
                        "Supplier Name",
                        style: theme.textTheme.bodyText2,
                      ),
                      Spacer(),
                      Text(
                        widget.purchase.supplierName,
                        style: theme.textTheme.bodyText1,
                      )
                    ],
                  ),
                  SizedBox(height: 15),
                  Row(
                    children: [
                      Text(
                        "Purchase Date",
                        style: theme.textTheme.bodyText2,
                      ),
                      Spacer(),
                      Text(
                        DateFormat('dd/MM/yyyy')
                            .format(widget.purchase.purchaseDate)
                            .toString(),
                        style: theme.textTheme.bodyText1,
                      )
                    ],
                  ),
                  SizedBox(height: 15),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text.rich(
                      TextSpan(
                          text: "Description\n\n",
                          style: theme.textTheme.bodyText2,
                          children: [
                            TextSpan(
                                text: widget.purchase.description,
                                style: theme.textTheme.bodyText1)
                          ]),
                    ),
                  ),
                ],
              ),
            ),
          ),
          FutureBuilder(
            future: _purchaseDetailService
                .getPurchaseDetails(widget.purchase.purchaseId),
            builder: (BuildContext context,
                AsyncSnapshot<List<PurchaseDetail>> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }

              if (snapshot.hasData) {
                return ListView.builder(
                  padding: EdgeInsets.all(10),
                  shrinkWrap: true,
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    return _buildPurchaseItem(snapshot.data[index], index);
                  },
                );
              }
            },
          ),
        ],
      ),
    );
  }

  Widget _buildPurchaseItem(PurchaseDetail purchaseDetail, int index) {
    return Container(
        margin: EdgeInsets.only(top: 10),
        padding: EdgeInsets.all(10),
        height: 60,
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
              blurRadius: 3,
              spreadRadius: 2,
              color: Colors.grey.withOpacity(0.5))
        ]),
        child: Row(
          children: [
            Expanded(flex: 1, child: Text("${index + 1}.")),
            Expanded(
                flex: 6,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      purchaseDetail.rawMaterial.materialName,
                      style: theme.textTheme.bodyText2,
                    ),
                    Text(numberFormat.format(purchaseDetail.qty).toString() +
                        " x @Rp. " +
                        numberFormat.format(purchaseDetail.price).toString())
                  ],
                )),
            Spacer(),
            Text(
                "Rp. " +
                    numberFormat
                        .format(purchaseDetail.qty * purchaseDetail.price)
                        .toString(),
                style: theme.textTheme.bodyText1),
          ],
        ));
  }

  Widget _buildBottomNavigationBar() {
    return Container(
        padding: EdgeInsets.all(15),
        height: (this.screenHeight * 0.15),
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
              blurRadius: 3,
              spreadRadius: 3,
              color: Colors.grey.withOpacity(0.3))
        ]),
        child: Row(
          children: [
            Text(
              "Total",
              style: theme.textTheme.headline6,
            ),
            Spacer(),
            Text(
                "Rp. " +
                    numberFormat.format(widget.purchase.totalAmount).toString(),
                style: theme.textTheme.headline4.copyWith(color: Colors.black))
          ],
        ));
  }

  _goToEditScreen(String purchaseId) {
    Navigator.push(
        context,
        CupertinoPageRoute(
            builder: (context) => PurchaseAddScreen(
                  isEdit: true,
                  purchaseId: purchaseId,
                )));
  }
}
