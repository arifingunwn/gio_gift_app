import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/purchase/purchase_bloc.dart';
import 'package:gio_gift_app/business_logic/repositories/implementations/purchase_repository_impl.dart';
import 'package:gio_gift_app/business_logic/services/purchase_service.dart';
import 'package:gio_gift_app/business_logic/models/purchase.dart';
import 'package:gio_gift_app/locator.dart';
import 'package:gio_gift_app/views/purchase/purchase_add_screen.dart';
import 'package:gio_gift_app/views/purchase/purchase_detail_screen.dart';
import 'package:intl/intl.dart';

class PurchaseScreen extends StatefulWidget {
  @override
  _PurchaseScreenState createState() => _PurchaseScreenState();
}

class _PurchaseScreenState extends State<PurchaseScreen> {
  ThemeData theme;
  final NumberFormat numberFormat = NumberFormat();
  PurchaseBloc _purchaseBloc;
  PurchaseService _purchaseService;

  @override
  void initState() {
    super.initState();
    _purchaseService = sl();
    _purchaseBloc = PurchaseBloc(_purchaseService);
  }

  @override
  void dispose() {
    _purchaseBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    theme = Theme.of(context);
    _purchaseBloc.add(PurchaseFetch());
    return Scaffold(
      backgroundColor: Colors.green[700],
      appBar: AppBar(
        title: Text("Purchases"),
        backgroundColor: Colors.green[700],
        elevation: 0,
        centerTitle: false,
      ),
      body: BlocProvider(
        create: (context) {
          return _purchaseBloc;
        },
        child: Column(
          children: [
            Expanded(
                child: BlocConsumer(
              cubit: _purchaseBloc,
              listener: (context, state) {},
              builder: (context, state) {
                if (state is PurchaseFetchSuccess) {
                  return ListView.builder(
                    itemCount: state.purchases.length,
                    itemBuilder: (context, index) {
                      return _buildPurchaseTile(
                          state.purchases[index], context, index);
                    },
                  );
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            ))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context,
              CupertinoPageRoute(
                  builder: (context) => PurchaseAddScreen(
                        isEdit: false,
                        purchaseBloc: _purchaseBloc,
                      )));
        },
        backgroundColor: Colors.white,
        child: Icon(Icons.add, color: Colors.green[700]),
      ),
    );
  }

  Widget _buildPurchaseTile(
      Purchase purchase, BuildContext context, int index) {
    return new Card(
      child: Container(
        padding: EdgeInsets.all(10),
        child: InkWell(
          onTap: () {
            Navigator.push(
                context,
                CupertinoPageRoute(
                    builder: (context) => PurchaseDetailScreen(
                          purchase: purchase,
                        )));
          },
          child: Column(
            children: [
              Padding(
                child: Row(
                  children: [
                    Text(
                      purchase.purchaseNo,
                      style: theme.textTheme.headline5,
                    ),
                    Spacer(),
                    Text(
                        DateFormat('dd/MM/yyyy')
                            .format(purchase.purchaseDate)
                            .toString(),
                        style: theme.textTheme.subtitle1)
                  ],
                ),
                padding: EdgeInsets.all(10),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                child: Row(
                  children: [
                    Text(purchase.supplierName,
                        style: theme.textTheme.subtitle1),
                    Spacer(),
                    Text(
                        "Rp. " +
                            numberFormat
                                .format(purchase.totalAmount)
                                .toString(),
                        style: theme.textTheme.headline6
                            .copyWith(color: Colors.green[900]))
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
