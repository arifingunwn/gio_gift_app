import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gio_gift_app/business_logic/blocs/purchase_form/purchase_form_bloc.dart';
import 'package:gio_gift_app/business_logic/models/raw_material.dart';
import 'package:gio_gift_app/views/raw_material/raw_material_picker.dart';
import 'package:intl/intl.dart';

class PurchaseAddItemScreen extends StatefulWidget {
  final PurchaseFormBloc purchaseFormBloc;
  const PurchaseAddItemScreen(this.purchaseFormBloc, {Key key})
      : super(key: key);

  @override
  _PurchaseAddItemScreenState createState() =>
      _PurchaseAddItemScreenState(purchaseFormBloc);
}

class _PurchaseAddItemScreenState extends State<PurchaseAddItemScreen> {
  final PurchaseFormBloc _purchaseFormBloc;
  final NumberFormat numberFormat = NumberFormat();
  GlobalKey<FormState> _formState = GlobalKey<FormState>();
  TextEditingController _materialNameController = TextEditingController();
  TextEditingController _qtyController = TextEditingController();
  TextEditingController _priceController = TextEditingController();
  TextEditingController _totalPriceController = TextEditingController();
  RawMaterial _rawMaterial;

  _PurchaseAddItemScreenState(this._purchaseFormBloc);

  @override
  void initState() {
    super.initState();

    _qtyController.addListener(_calculateTotalPrice);
    _priceController.addListener(_calculateTotalPrice);

    _totalPriceController.text = "0";
  }

  @override
  void dispose() {
    _qtyController.removeListener(_calculateTotalPrice);
    _priceController.removeListener(_calculateTotalPrice);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Item"),
        backgroundColor: Colors.green[800],
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Form(
          key: _formState,
          child: Column(
            children: [
              TextFormField(
                controller: _materialNameController,
                readOnly: true,
                decoration: InputDecoration(
                    labelText: "Material Name",
                    suffix: FlatButton(
                      shape: CircleBorder(),
                      color: Colors.green[800],
                      child: Icon(Icons.search_rounded, color: Colors.white),
                      onPressed: () {
                        Navigator.push(
                            context,
                            CupertinoPageRoute(
                                builder: (context) => RawMaterialPicker(
                                      getType: RawMaterialGetTypeEnum.GETALL,
                                      onItemSelected: (val) {
                                        setState(() {
                                          _rawMaterial = val;
                                          _materialNameController.text =
                                              val.materialName;
                                        });
                                      },
                                    )));
                      },
                    )),
              ),
              TextFormField(
                  controller: _qtyController,
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.digitsOnly
                  ],
                  decoration: InputDecoration(
                    labelText: "Qty",
                  )),
              TextFormField(
                  controller: _priceController,
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.digitsOnly
                  ],
                  decoration: InputDecoration(
                    labelText: "Price Per Qty",
                  )),
              TextFormField(
                  controller: _totalPriceController,
                  readOnly: true,
                  decoration: InputDecoration(
                    labelText: "Total",
                  )),
              MaterialButton(
                minWidth: double.infinity,
                onPressed: () {
                  _addItem();
                },
                color: Colors.green[800],
                child: Text(
                  "Add Item",
                  style: TextStyle(color: Colors.white),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _calculateTotalPrice() {
    if (_qtyController.text.isEmpty) return;
    if (_priceController.text.isEmpty) return;

    double qty = double.parse(_qtyController.text);
    double price = double.parse(_priceController.text);

    _totalPriceController.text = numberFormat.format(qty * price).toString();
  }

  void _addItem() {
    _purchaseFormBloc.add(PurchaseFormAddItem(
        _rawMaterial,
        double.parse(_qtyController.text),
        double.parse(_priceController.text)));
    Navigator.pop(context);
  }
}
