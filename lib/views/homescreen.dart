import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gio_gift_app/business_logic/models/dashboard_data.dart';
import 'package:gio_gift_app/business_logic/models/order.dart';
import 'package:gio_gift_app/business_logic/repositories/dashboard_repository.dart';
import 'package:gio_gift_app/business_logic/services/dashboard_service.dart';
import 'package:gio_gift_app/locator.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  double _screenHeight;
  double _screenWidth;
  ThemeData theme;

  DashboardService _dashboardService;

  @override
  void initState() {
    super.initState();

    _dashboardService = sl();
  }

  @override
  Widget build(BuildContext context) {
    theme = Theme.of(context);
    _screenHeight = MediaQuery.of(context).size.height;
    _screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
        drawer: _buildDrawer(),
        backgroundColor: Colors.white,
        appBar: AppBar(
          toolbarHeight: 100,
          backgroundColor: Colors.white,
          actions: [
            IconButton(
              icon: Icon(
                Icons.notifications,
                color: Colors.black,
              ),
              onPressed: () {},
            )
          ],
          title: Text(
            "Dashboard",
            style: TextStyle(
                fontSize: 24, color: Colors.black, fontWeight: FontWeight.bold),
          ),
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: Icon(Icons.menu, color: Colors.black),
                onPressed: () {
                  Scaffold.of(context).openDrawer();
                },
              );
            },
          ),
          elevation: 0.0,
        ),
        body: ListView(
          children: [
            Container(
                height: 130,
                padding: EdgeInsets.all(30),
                width: _screenWidth,
                decoration: BoxDecoration(
                    color: Colors.purple[800],
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(50),
                        topRight: Radius.circular(50))),
                child: FutureBuilder(
                  future: _dashboardService.getDashboardData(),
                  builder: (context, AsyncSnapshot<DashboardData> data) {
                    if (data.hasData) {
                      return ListView(
                        scrollDirection: Axis.horizontal,
                        children: [
                          _buildChip(data.data.orderPendingCount.toString(),
                              "Pending Order", () {
                            Navigator.pushNamed(context, "/Order",
                                arguments: OrderStatus.PENDING);
                          }),
                          _buildChip(data.data.orderReadyToShipCount.toString(),
                              "Order Ready to Ship", () {
                            Navigator.pushNamed(context, "/Order",
                                arguments: OrderStatus.READYTOSHIP);
                          }),
                          _buildChip(data.data.orderInProgressCount.toString(),
                              "In Progress Order", () {
                            Navigator.pushNamed(context, "/Order",
                                arguments: OrderStatus.INPROGRESS);
                          }),
                        ],
                      );
                    }

                    return Container();
                  },
                )),
            Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                    height: 100,
                    width: _screenWidth,
                    color: Colors.purple[800]),
                Positioned(
                  child: Container(
                    width: _screenWidth,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            blurRadius: 5,
                            spreadRadius: 3,
                            color: Colors.black.withOpacity(0.2))
                      ],
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(50),
                          topRight: Radius.circular(50)),
                    ),
                    padding: EdgeInsets.all(20),
                    child: Wrap(
                      spacing: 20,
                      runSpacing: 20,
                      direction: Axis.horizontal,
                      alignment: WrapAlignment.spaceAround,
                      children: [
                        createHomeScreenMenu(Icons.menu_book, "Orders",
                            "/Order", Colors.blue[800]),
                        createHomeScreenMenu(CupertinoIcons.shopping_cart,
                            "Purchase", "/Purchase", Colors.green[800]),
                        createHomeScreenMenu(
                          Icons.plumbing,
                          "Raw Materials",
                          "/RawMaterial",
                          Colors.purple[800],
                        ),
                        createHomeScreenMenu(
                          CupertinoIcons.person,
                          "Customer",
                          "/Customer",
                          Colors.blue[800],
                        ),
                        createHomeScreenMenu(
                          CupertinoIcons.money_dollar,
                          "Finance",
                          "/FinanceTransaction",
                          Colors.yellow[900],
                        ),
                        createHomeScreenMenu(
                          CupertinoIcons.doc,
                          "Reports",
                          "/Report",
                          Colors.blue[800],
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ],
        ));
  }

  Widget createHomeScreenMenu(
      IconData icon, String title, String routeName, Color cardColor) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        Navigator.pushNamed(context, routeName);
      },
      child: Container(
        width: 150,
        height: 150,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30), color: cardColor),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              icon,
              size: 100,
              color: Colors.white,
            ),
            Text(title,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.bold))
          ],
        ),
      ),
    );
  }

  Widget _buildChip(String title, String subTitle, Function callback) {
    return GestureDetector(
      onTap: () {
        callback();
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.purple[600], borderRadius: BorderRadius.circular(30)),
        margin: EdgeInsets.only(right: 10),
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Text.rich(TextSpan(
            text: "$title\n",
            style: theme.textTheme.headline5
                .copyWith(color: Colors.white, fontWeight: FontWeight.bold),
            children: [
              TextSpan(
                  text: "$subTitle",
                  style:
                      theme.textTheme.subtitle2.copyWith(color: Colors.white))
            ])),
      ),
    );
  }

  Widget _buildDrawer() {
    return Drawer(
      child: Container(
        height: 400,
        color: Colors.grey[900],
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            Container(
              height: 200,
            ),
            ListTile(
              title: Text("Order",
                  style: TextStyle(fontSize: 18, color: Colors.white)),
              leading: Icon(Icons.assignment, color: Colors.white),
              onTap: () {
                Navigator.pushNamed(context, "/Order");
              },
            ),
            ListTile(
              title: Text("Raw Material",
                  style: TextStyle(fontSize: 18, color: Colors.white)),
              leading: Icon(Icons.ac_unit, color: Colors.white),
              onTap: () {
                Navigator.pushNamed(context, "/RawMaterial");
              },
            ),
            ListTile(
              title: Text("Customer",
                  style: TextStyle(fontSize: 18, color: Colors.white)),
              leading: Icon(Icons.person, color: Colors.white),
              onTap: () {
                Navigator.pushNamed(context, "/Customer");
              },
            ),
            ListTile(
              leading: Icon(Icons.assignment, color: Colors.white),
              title: Text("Report",
                  style: TextStyle(fontSize: 18, color: Colors.white)),
              onTap: () {
                Navigator.pushNamed(context, "/Report");
              },
            ),
          ],
        ),
      ),
    );
  }
}
