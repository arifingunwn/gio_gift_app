import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/order/order_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/order_material/order_material_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/order_material/order_material_event.dart';
import 'package:gio_gift_app/business_logic/blocs/order_material/order_material_state.dart';
import 'package:gio_gift_app/business_logic/models/order_material_detail.dart';
import 'package:gio_gift_app/business_logic/models/order.dart';
import 'package:gio_gift_app/business_logic/utils/debouncer.dart';
import 'package:gio_gift_app/views/raw_material/raw_material_picker.dart';

class OrderMaterialScreen extends StatefulWidget {
  final Order order;
  final bool isEdit;
  final OrderBloc orderBloc;
  const OrderMaterialScreen(this.order, this.isEdit, this.orderBloc, {Key key})
      : super(key: key);
  @override
  _OrderMaterialScreenState createState() =>
      _OrderMaterialScreenState(orderBloc);
}

class _OrderMaterialScreenState extends State<OrderMaterialScreen> {
  OrderMaterialBloc _orderMaterialBloc;
  OrderBloc _orderBloc;

  final _debouncer = Debouncer(500);

  _OrderMaterialScreenState(this._orderBloc);

  @override
  void initState() {
    _orderMaterialBloc = OrderMaterialBloc(_orderBloc);
    super.initState();
  }

  @override
  void dispose() {
    _orderMaterialBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isEdit) {
      _orderMaterialBloc.add(OrderMaterialStartEdit(widget.order.orderId));
    } else {
      _orderMaterialBloc.add(OrderMaterialStartEmpty());
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
            (widget.isEdit ? "Edit Order Material" : "Add Order Material")),
      ),
      body: BlocProvider(
        create: (BuildContext context) {
          return _orderMaterialBloc;
        },
        child: ListView(
          padding: EdgeInsets.all(10),
          children: [
            Card(
              child: Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                        padding: EdgeInsets.all(10),
                        child: Text(
                          "Material Used",
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: Colors.blue[800]),
                        )),
                    Divider(),
                    Padding(
                      padding: EdgeInsets.all(20),
                      child: BlocProvider(
                        create: (BuildContext context) {
                          return _orderMaterialBloc;
                        },
                        child: BlocConsumer(
                          cubit: _orderMaterialBloc,
                          listener: (context, state) {
                            if (state is OrderMaterialItemAlreadyExists) {
                              return Scaffold.of(context).showSnackBar(
                                SnackBar(
                                  content: Text("The item " +
                                      state.materialName +
                                      " already exists."),
                                  backgroundColor: Colors.red,
                                ),
                              );
                            }

                            if (state is OrderMaterialSaveSuccess) {
                              Navigator.pushReplacementNamed(context, '/Order');
                            }
                          },
                          builder: (context, state) {
                            if (state is OrderMaterialItemLoading) {
                              return Center(child: CircularProgressIndicator());
                            }

                            if (state is OrderMaterialItemUpdated) {
                              if (state.orderMaterialDetails.length == 0) {
                                return Center(
                                    child: Text(
                                        "You haven't add any material yet."));
                              }

                              return ListView.separated(
                                shrinkWrap: true,
                                separatorBuilder: (context, index) {
                                  return Divider();
                                },
                                itemCount: state.orderMaterialDetails.length,
                                itemBuilder: (context, index) {
                                  return Container(
                                    height: 30,
                                    child: Expanded(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text((index + 1).toString() + ". ",
                                              style: TextStyle(fontSize: 12)),
                                          Text(
                                              state.orderMaterialDetails[index]
                                                  .rawMaterial.materialName,
                                              style: TextStyle(fontSize: 12)),
                                          Spacer(),
                                          _buildTrailing(
                                              state.orderMaterialDetails[index],
                                              index)
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              );
                            }
                            return Container();
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    CupertinoPageRoute(
                        builder: (context) => RawMaterialPicker(
                              getType: RawMaterialGetTypeEnum.GETAVAILABLE,
                              onItemSelected: (val) {
                                _orderMaterialBloc.add(OrderMaterialAddItem(
                                    orderMaterialDetail: OrderMaterialDetail(
                                        rawMaterial: val, qtyUsed: 1)));
                              },
                            )));
              },
              child: Card(
                  margin: EdgeInsets.only(top: 10),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.blue[700],
                        borderRadius: BorderRadius.circular(10)),
                    padding: EdgeInsets.all(15),
                    child: Row(
                      children: [
                        Icon(
                          Icons.add_box,
                          color: Colors.white,
                        ),
                        Text(
                          "  Add Item",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                        Spacer(),
                        Icon(Icons.chevron_right, color: Colors.white)
                      ],
                    ),
                  )),
            ),
            BlocBuilder<OrderMaterialBloc, OrderMaterialState>(
              cubit: _orderMaterialBloc,
              builder: (context, state) {
                if (state is OrderMaterialItemUpdated) {
                  if (state.orderMaterialDetails.length > 0) {
                    return MaterialButton(
                      onPressed: () {
                        _saveOrderMaterial(state.orderMaterialDetails);
                      },
                      child: (state is OrderMaterialSaveLoading
                          ? CircularProgressIndicator()
                          : Text("Save",
                              style: TextStyle(
                                  fontSize: 16, color: Colors.white))),
                      color: Colors.blue[800],
                      height: 40,
                    );
                  }
                }

                return Container();
              },
            )
          ],
        ),
      ),
    );
  }

  Widget _buildTrailing(OrderMaterialDetail orderMaterialDetail, int index) {
    return Row(
      children: [
        FlatButton(
            minWidth: 40,
            onPressed: () {
              _orderMaterialBloc.add(OrderMaterialDecreaseItemQuantity(
                  orderMaterialDetail.rawMaterial.rawMaterialId,
                  orderMaterialDetail.qtyUsed));
            },
            child: Icon(Icons.remove_circle)),
        Container(
          width: 40,
          height: 50,
          child: TextFormField(
            key: UniqueKey(),
            textAlign: TextAlign.center,
            initialValue: orderMaterialDetail.qtyUsed.toString(),
            onChanged: (val) {
              _debouncer.run(() {
                _onQtyTextFieldChange(
                    orderMaterialDetail.rawMaterial.rawMaterialId, val);
              });
            },
          ),
        ),
        FlatButton(
          minWidth: 40,
          onPressed: () {
            _orderMaterialBloc.add(OrderMaterialIncreaseItemQuantity(
                orderMaterialDetail.rawMaterial.rawMaterialId,
                orderMaterialDetail.qtyUsed));
          },
          child: Icon(Icons.add_circle),
        )
      ],
    );
  }

  void _saveOrderMaterial(List<OrderMaterialDetail> orderMaterialDetails) {
    _orderMaterialBloc.add(OrderMaterialSave(
        isEdit: widget.isEdit,
        order: widget.order,
        orderMaterialDetail: orderMaterialDetails));
  }

  void _onQtyTextFieldChange(String rawMaterialId, String val) {
    double qty = double.tryParse(val);
    if (qty == null) qty = 0;

    _orderMaterialBloc.add(OrderMaterialUpdateItemQuantity(rawMaterialId, qty));
  }
}
