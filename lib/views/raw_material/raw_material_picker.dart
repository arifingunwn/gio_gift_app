import 'package:flutter/material.dart';
import 'package:gio_gift_app/business_logic/repositories/implementations/raw_material_repository_impl.dart';
import 'package:gio_gift_app/business_logic/services/raw_material_service.dart';
import 'package:gio_gift_app/business_logic/models/raw_material.dart';
import 'package:gio_gift_app/locator.dart';

typedef OnItemSelectedCallback = Function(RawMaterial rawMaterial);

enum RawMaterialGetTypeEnum { GETALL, GETAVAILABLE }

class RawMaterialPicker extends StatefulWidget {
  final OnItemSelectedCallback onItemSelected;

  final RawMaterialGetTypeEnum getType;

  const RawMaterialPicker({Key key, this.onItemSelected, this.getType})
      : super(key: key);

  @override
  _RawMaterialPickerState createState() => _RawMaterialPickerState();
}

class _RawMaterialPickerState extends State<RawMaterialPicker> {
  bool _isSearch = false;

  final TextEditingController _searchTextController = TextEditingController();
  RawMaterialService _rawMaterialService;

  String filterString = "";

  @override
  void initState() {
    super.initState();
    _rawMaterialService = sl();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: _isSearch
              ? TextFormField(
                  textInputAction: TextInputAction.search,
                  controller: _searchTextController,
                  style: TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                    hintText: "Search",
                  ))
              : Text("Search Raw Material"),
          actions: [
            (_isSearch
                ? IconButton(
                    icon: Icon(Icons.close),
                    onPressed: () {
                      setState(() {
                        _isSearch = false;
                      });
                    },
                  )
                : IconButton(
                    icon: Icon(Icons.search),
                    onPressed: () {
                      setState(() {
                        _isSearch = true;
                      });
                    },
                  ))
          ],
        ),
        body: FutureBuilder<List<RawMaterial>>(
          future: _getStream(widget.getType, _searchTextController.text),
          builder:
              (BuildContext context, AsyncSnapshot<List<RawMaterial>> snap) {
            if (!snap.hasData) {
              return Center(
                child: Text("Raw Material is empty"),
              );
            }

            if (snap.hasData) {
              return ListView.builder(
                itemCount: snap.data.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    onTap: () {
                      widget.onItemSelected(snap.data[index]);
                      Navigator.pop(context);
                    },
                    title: Text(snap.data[index].materialName),
                    subtitle: Text(snap.data[index].qty.toString()),
                  );
                },
              );
            }

            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ));
  }

  // ignore: missing_return
  Future<List<RawMaterial>> _getStream(
      RawMaterialGetTypeEnum getType, String filterString) async {
    if (getType == RawMaterialGetTypeEnum.GETALL) {
      return await _rawMaterialService.getAll();
    }

    if (getType == RawMaterialGetTypeEnum.GETAVAILABLE) {
      return await _rawMaterialService.getAvailableRawMaterials(filterString);
    }
  }
}
