import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/raw_material/raw_material_bloc.dart';
import 'package:gio_gift_app/business_logic/blocs/raw_material/raw_material_event.dart';
import 'package:gio_gift_app/business_logic/blocs/raw_material/raw_material_state.dart';
import 'package:gio_gift_app/business_logic/repositories/implementations/raw_material_repository_impl.dart';
import 'package:gio_gift_app/business_logic/utils/debouncer.dart';
import 'package:gio_gift_app/views/custom_widgets/list_tile_popup_menu.dart';
import 'package:gio_gift_app/views/raw_material/raw_material_add_edit_screen.dart';

class RawMaterialScreen extends StatefulWidget {
  @override
  _RawMaterialScreenState createState() => _RawMaterialScreenState();
}

class _RawMaterialScreenState extends State<RawMaterialScreen> {
  RawMaterialBloc _rawMaterialBloc;
  TextEditingController _filterController = TextEditingController();
  final Debouncer _debouncer = Debouncer(1000);

  @override
  void initState() {
    super.initState();

    _rawMaterialBloc = new RawMaterialBloc(RawMaterialRepositoryImpl());
    _filterController.addListener(_onFilterChange);
  }

  @override
  void dispose() {
    _rawMaterialBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _rawMaterialBloc.add(RawMaterialFetch());

    return Scaffold(
      appBar: AppBar(
        title: TextFormField(
          controller: _filterController,
          decoration: InputDecoration(
              hintText: "Search", hintStyle: TextStyle(color: Colors.white)),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            color: Colors.white,
            onPressed: () {},
          )
        ],
        backgroundColor: Colors.purple,
      ),
      body: BlocProvider(
        create: (BuildContext context) {
          return _rawMaterialBloc;
        },
        child: Container(child: BlocBuilder<RawMaterialBloc, RawMaterialState>(
          builder: (context, state) {
            if (state is RawMaterialFetchLoading)
              return Center(child: CircularProgressIndicator());
            if (state is RawMaterialListUpdated) {
              if (state.rawMaterials == null) {
                return Center(child: Text("Empty"));
              }

              return Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                      child: ListView.separated(
                          itemCount: state.rawMaterials.length,
                          separatorBuilder: (context, index) {
                            return Divider();
                          },
                          itemBuilder: (context, index) {
                            return ListTilePopUpMenu(
                              id: state.rawMaterials[index].rawMaterialId,
                              title: state.rawMaterials[index].materialName,
                              subtitle:
                                  state.rawMaterials[index].qty.toString(),
                              onEdit: (id) {
                                Navigator.of(context).push(CupertinoPageRoute(
                                    builder: (context) =>
                                        RawMaterialAddEditScreen(
                                            rawMaterial: state.rawMaterials
                                                .firstWhere((r) =>
                                                    r.rawMaterialId == id),
                                            isEdit: true,
                                            onSave: (value) {
                                              _rawMaterialBloc.add(
                                                  RawMaterialUpdate(value));
                                            })));
                              },
                              onDelete: (value) {
                                _rawMaterialBloc.add(RawMaterialDeleted(
                                    state.rawMaterials[index].rawMaterialId));
                              },
                            );
                          }))
                ],
              );
            }

            return Center(child: CircularProgressIndicator());
          },
        )),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          backgroundColor: Colors.purple,
          onPressed: () => Navigator.of(context).push(CupertinoPageRoute(
              builder: (context) => RawMaterialAddEditScreen(
                    isEdit: false,
                    onSave: (value) {
                      _rawMaterialBloc
                          .add(RawMaterialAdded(value.materialName));
                    },
                  )))),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
    );
  }

  void _onFilterChange() {
    _debouncer.run(() {
      _rawMaterialBloc.add(RawMaterialFilter(_filterController.text));
    });
  }
}
