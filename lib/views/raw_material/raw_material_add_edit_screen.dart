import 'package:flutter/material.dart';
import 'package:gio_gift_app/business_logic/models/raw_material.dart';

typedef OnSaveCallback = Function(RawMaterial rawMaterial);

class RawMaterialAddEditScreen extends StatefulWidget {
  final bool isEdit;
  final RawMaterial rawMaterial;
  final OnSaveCallback onSave;

  const RawMaterialAddEditScreen(
      {Key key, @required this.isEdit, this.rawMaterial, @required this.onSave})
      : super(key: key);

  @override
  _RawMaterialAddEditScreenState createState() =>
      _RawMaterialAddEditScreenState();
}

class _RawMaterialAddEditScreenState extends State<RawMaterialAddEditScreen> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  RawMaterial _rawMaterial;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Add Raw Material"),
          backgroundColor: Colors.purple,
        ),
        body: Form(
          key: _formKey,
          child: Container(
              child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                TextFormField(
                  initialValue:
                      this.widget.isEdit ? widget.rawMaterial.materialName : '',
                  validator: (val) {
                    return val.trim().isEmpty
                        ? "Please Type Material Name"
                        : null;
                  },
                  onSaved: (value) {
                    if (this.widget.isEdit) {
                      _rawMaterial =
                          this.widget.rawMaterial.copyWith(materialName: value);
                    } else {
                      _rawMaterial = RawMaterial(materialName: value);
                    }
                  },
                  decoration: InputDecoration(labelText: "Material Name"),
                ),
              ],
            ),
          )),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.purple,
          onPressed: () {
            if (_formKey.currentState.validate()) {
              _formKey.currentState.save();
              widget.onSave(_rawMaterial);
              Navigator.pop(context);
            }
          },
          child: Icon(Icons.done),
        ));
  }
}
